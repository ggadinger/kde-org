---
aliases:
- ../../kde-frameworks-5.63.0
date: 2019-10-14
layout: framework
libCount: 70
---
### Icônes « Breeze »

- Améliorations pour l'icône de KFloppy (bogue 412404)
- Add format-text-underline-squiggle actions icons (bug 408283)
- Ajout d'une icône colorée pour « KCollapsibleGroupBox »
- Ajout d'une icône d'applications pour l'application de contrôle de drone Kirogi
- Added scripts to create a webfont out of all breeze action icons
- Add enablefont and disablefont icon for kfontinst KCM
- Fix large system-reboot icons rotating in an inconsistent direction (bug 411671)

### Modules additionnels « CMake »

- Nouveau module « ECMSourceVersionControl »
- Correction de « FindEGL » lors de l'utilisation de « Emscripten »
- ECMAddQch : ajout d'un argument « INCLUDE_DIRS »

### Intégration avec l'environnement de développement

- ensure winId() not called on non-native widgets (bug 412675)

### kcalendarcore

New module, previously known as kcalcore in kdepim

### KCMUtils

- Suppress mouse events in KCMs causing window moves
- adjust margins of KCMultiDialog (bug 411161)

### KCompletion

- [KComboBox] Properly disable Qt's builtin completer [regression fix]

### KConfig

- Fix generating properties that start with an uppercase letter

### KConfigWidgets

- Make KColorScheme compatible with QVariant

### kcontacts

Nouveau module, antérieurement appartenant à KDE PIM

### KCoreAddons

- Ajout de « KListOpenFilesJob »

### KDeclarative

- Delete QQmlObjectSharedEngine context in sync with QQmlObject
- [KDeclarative] Port from deprecated QWheelEvent::delta() to angleDelta()

### Prise en charge de « KDELibs 4 »

- Support NetworkManager 1.20 and do actually compile the NM backend

### KIconThemes

- Deprecate the global [Small|Desktop|Bar]Icon() methods

### KImageFormats

- Ajout de fichiers pour le test du bogue 411327
- xcf: Fix regression when reading files with "unsupported" properties
- xcf : lecture correcte de la résolution de l'image
- Port HDR (Radiance RGBE) image loader to Qt5

### KIO

- [Places panel] Revamp the Recently Saved section
- [DataProtocol] compile without implicit conversion from ascii
- Consider the usage of WebDAV methods sufficient for assuming WebDAV
- REPORT also supports the Depth header
- Make QSslError::SslError &lt;-&gt; KSslError::Error conversion reusable
- Deprecate the KSslError::Error ctor of KSslError
- [Windows] fix listing the parent dir of C:foo, that's C: and not C:
- Fix crash on exit in kio_file (bug 408797)
- Ajout des opérateurs « == » et « ! = » à « KIO::UDSEntry »
- Replace KSslError::errorString with QSslError::errorString
- Move/copy job: skip stat'ing sources if the destination dir isn't writable (bug 141564)
- Fixed interaction with DOS/Windows executables in KRun::runUrl
- [KUrlNavigatorPlacesSelector] Properly identify teardown action (bug 403454)
- KCoreDirLister: fix crash when creating new folders from kfilewidget (bug 401916)
- [kpropertiesdialog] add icons for the size section
- Add icons for "Open With" and "Actions" menus
- Avoid initializing an unnecessary variable
- Move more functionality from KRun::runCommand/runApplication to KProcessRunner
- [Advanced Permissions] Fix icon names (bug 411915)
- [KUrlNavigatorButton] Fix QString usage to not use [] out of bounds
- Make KSslError hold a QSslError internally
- Séparation de « KSslErrorUiData » de « KTcpSocket »
- Portage de « kpac » à partir de « QtScript »

### Kirigami

- Toujours masquer uniquement le dernier élément
- Plus de « z » (bogue 411832)
- Correction de la version d'importation dans « PagePoolAction »
- PagePool devient Kirigami 2.11.
- take into account dragging speed when a flick ends
- Correction de la copie d'« URL » vers le presse-papier
- check more if we are reparenting an actual Item
- prise en charge minimale pour les actions « ListItem »
- Introduction des pages de cache
- Correction de compatibilité avec Qt 5.11
- Insertion d'une action « PagePoolAction »
- new class: PagePool to manage recycling of pages after they're popped
- Meilleure apparence pour les barres de tâches
- Certaines marges sur la droite (bogue 409630)
- Revert "Compensate smaller icon sizes on mobile in the ActionButton"
- don't make list items look inactive (bug 408191)
- Revert "Remove scaling of iconsize unit for isMobile"
- Layout.fillWidth should be done by the client (bug 411188)
- Ajout d'un modèle pour le développement des applications sous « Kirigami »
- Ajour d'un mode pour les actions de centrage et omettre le titre lors de l'utilisation d'un style « ToolBar » (bogue 402948)
- Compensate smaller icon sizes on mobile in the ActionButton
- Fixed some undefined properties runtime errors
- Fix ListSectionHeader background color for some color schemes
- Remove custom content item from ActionMenu separator

### KItemViews

- [KItemViews] Port to non-deprecated QWheelEvent API

### KJobWidgets

- cleanup dbus related objects early enough to avoid hang on program exit

### KJS

- Added startsWith(), endsWith() and includes() JS String functions
- Fixed Date.prototype.toJSON() called on non-Date objects

### KNewStuff

- Bring KNewStuffQuick to feature parity with KNewStuff(Widgets)

### KPeople

- Confirmation d'Android comme plate-forme prise en compte
- Déploiement d'un avatar par défaut grâce à « qrc »
- Regroupement des fichiers de modules externes pour Android
- Désactivation des éléments « D-Bus » sous Android
- Fix crash when monitoring a contact that gets removed on PersonData (bug 410746)
- Utilisation de types totalement qualifiés pour les signaux

### KRunner

- Consider UNC paths as NetworkShare context

### KService

- Move Amusement to Games directory instead of Games &gt; Toys (bug 412553)
- [KService] Ajout du constructeur « copy »
- [KService] add workingDirectory(), deprecate path()

### KTextEditor

- Tentative d'évitement des artefacts dans l'aperçu de texte
- Variable expansion: Use std::function internally
- QRectF instead of QRect solves clipping issues, (bug 390451)
- next rendering artifact goes away if you adjust the clip rect a bit (bug 390451)
- avoid the font choosing magic and turn of anti aliasing (bug 390451)
- KadeModeMenuList: fix memory leaks and others
- try to scan for usable fonts, works reasonable well if you use no dumb scaling factor like 1.1
- Status bar mode menu: Reuse empty QIcon that is implicitly shared
- Expose KTextEditor::MainWindow::showPluginConfigPage()
- Remplacement de « QSignalMapper » avec « lambda »
- KateModeMenuList: use QString() for empty strings
- KateModeMenuList: add "Best Search Matches" section and fixes for Windows
- Expansion variable : prise en charge de « QTextEdits »
- Add keyboard shortcut for switching Input modes to edit menu (bug 400486)
- Variable expansion dialog: properly handle selection changes and item activation
- Variable expansion dialog: add filter line edit
- Backup on save: Support time and date string replacements (bug 403583)
- Variable expansion: Prefer return value over return argument
- Démarrage initial de la boîte de dialogue pour les variables
- Utilisation de l'« API » de nouveaux formats

### Environnement de développement « KWallet »

- Prise en charge « HiDPI »

### KWayland

- Sort files alphabetically in cmake list

### KWidgetsAddons

- Rendre le bouton « Ok » configurable dans « KMessageBox::sorry/detailedSorry »
- [KCollapsibleGroupBox] Alarme durant l'exécution « QTimeLine::start »
- Amélioration du nommage des méthodes pour icônes « KTitleWidget »
- Ajout de marqueurs « QIcon » pour les boîtes de dialogue de mots de passe
- [KWidgetAddons] Portage vers les API « Qt » non déconseillés

### KWindowSystem

- Définition de « XCB » comme nécessaire lors de la compilation du moteur X
- Limiter l'usage de l'alias d'énumération déconseillé « NET::StaysOnTop »

### KXMLGUI

- Déplacement de l'élément « Mode Plein écran » à partir du menu de paramètres vers le menu d'affichage (bogue 106807)

### NetworkManagerQt

- ActiveConnection : connexion du signal « stateChanged()  » pour corriger l'interface

### Environnement de développement de Plasma

- Exportation de la catégorie de journal pour la bibliothèque centrale de Plasma. Ajout d'une catégorie à « qWarning »
- [pluginloader] Utilisation d'une journalisation avec catégories
- Passage de « editMode » comme une propriété globale de « corona »
- Accord pour un facteur de vitesse pour l'animation globale
- properly install whole plasmacomponent3
- [Dialog] Apply window type after changing flags
- Modification de la logique d'affichage pour le bouton de mot clé
- Fix crash on teardown with Applet's ConfigLoader (bug 411221)

### QQC2StyleBridge

- Correction de plusieurs erreurs de compilation du système
- Utilisation des marges à partir de « qstyle »
- [Onglet] Correction du dimensionnement (bogue 409390)

### Coloration syntaxique

- Ajout de la coloration syntaxique dans « RenPy » (.rpy) (bogue 381547)
- WordDetect rule: detect delimiters at the inner edge of the string
- Highlight GeoJSON files as if they were plain JSON
- Ajout de la coloration syntaxique dans les sous-titres « SRT » (SubRip Text)
- Fix skipOffset with dynamic RegExpr (bug 399388)
- bitbake: handle embedded shell and python
- Jam : correction d'un identifieur dans une sous-règle
- Ajout de la définition de syntaxe dans « Perl6 » (bogue 392468)
- support .inl extension for C++, not used by other xml files at the moment (bug 411921)
- support *.rej for diff highlighting (bug 411857)

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
