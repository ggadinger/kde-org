---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE publie les applications KDE 16.04.0
layout: application
title: KDE publie les applications KDE 16.04.0
version: 16.04.0
---
Le 20 avril 2016. KDE présente aujourd'hui KDE Applications 16.04 comprenant un éventail impressionnant de mises à niveau concernant l'accessibilité. Il introduction de nouvelles fonctionnalités très utiles apporte des corrections de problèmes mineurs variés.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> et <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> ont maintenant été portés sur KDE Frameworks 5 et nous attendons avec impatience vos commentaires et votre vision des nouvelles fonctionnalités introduites avec cette version. Nous encourageons également vivement votre soutien à <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a> car nous l'accueillons dans nos applications KDE. N'hésitez pas à laisser vos commentaires sur ce que vous aimeriez voir d'amélioré dans l'application.

### Nouvelle addition de KDE

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

Une nouvelle application a été ajoutée à la suite KDE Éducation. <a href='https://minuet.kde.org'>Minuet</a> est un logiciel d'éducation musicale offrant un support MIDI complet avec contrôle du rythme, de la hauteur et du volume, ce qui le rend adapté aux musiciens novices et expérimentés.

Minuet comprend 44 exercices d'entraînement de l'écoute de gammes, sur les accords, sur les intervalles et sur le rythme, il permet la visualisation du contenu musical sur le clavier du piano et permet l'intégration de vos propres exercices.

### Plus d'aide pour vous

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

KHelpCenter, précédemment livré avec Plasma, est maintenant distribué comme faisant partie des applications de KDE.

Une campagne massive de correction des bogues menée par l'équipe du KHelpCenter a permis de résoudre 49 bogues, dont beaucoup étaient liés à l'amélioration et au rétablissement de la fonctionnalité de recherche auparavant non fonctionnelle.

La recherche interne de documents qui reposait sur le logiciel obsolète ht::/dig a été remplacée par un nouveau système d'indexation et de recherche basé sur Xapian qui permet de restaurer des fonctionnalités telles que la recherche dans les pages de manuel, les pages d'information et la documentation fournie par le logiciel KDE, avec la possibilité de créer des signets pour les pages de documentation.

Avec la suppression du support KDELibs4 et un nouveau nettoyage du code et de quelques autres bogues mineurs, la maintenance du code a également été minutieusement effectuée pour vous proposer un nouveau KHelpCenter encore plus performant.

### Contrôle agressif de nuisance

55 bogues ont été résolus dans la suite Kontact, dont certains étaient liés à des problèmes de réglage d'alarmes, d'importation de courriers électroniques Thunderbird, de réduction de la taille des icônes Skype et Google Talk dans le panneau des contacts, de solutions de contournement liées à KMail telles que l'importation de dossiers, l'importation de vCard, l'ouverture de pièces jointes ODF, l'insertion d'URL à partir de Chromium, les différences de menu d'outils lorsque l'application est lancée en tant que partie intégrante de Kontact par opposition à une utilisation autonome, l'élément de menu "Envoyer" manquant et quelques autres. Le support des flux RSS en portugais brésilien a été ajouté ainsi que la correction des adresses pour les flux en hongrois et en espagnol.

Les nouvelles fonctionnalités concernent une nouvelle conception pour l'éditeur de contacts de KAddressbook, un nouveau thème par défaut pour les en-têtes de KMail, des améliorations dans l'exportateur de paramètres et une correction pour la prise en charge des émoticônes dans Akregator. L'interface du composeur de KMail a été simplifiée, avec en même temps l'introduction d'un nouveau thème par défaut pour les en-têtes de KMail « Grantlee », utilisé pour la page « A propos », comme pour Kontact et Akregator. Akregator utilise maintenant « QtWebkit » - un des principaux moteurs pour réaliser le rendu de pages Internet et pour exécuter du code « javascript » comme moteur de rendu de pages Internet. Le processus pour la prise en charge de l'implémentation de « QtWebEngine » dans Akregator et les autres applications de la suite Kontact a déjà commencé. Bien que de nombreuses fonctionnalités ont été reportées comme des modules externes dans « kdepim-addons », les bibliothèques « pim » ont été découpées en de nombreux paquets.

### Archivage en profondeur

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, le gestionnaire d'archives, a vu deux bogues majeurs corrigés. Désormais, Ark avertit l'utilisateur si une extraction échoue faute d'espace suffisant dans le dossier de destination et il ne remplit pas la RAM lors de l'extraction de fichiers trop importants par glisser-déposer.

Ark comprend désormais également une boîte de dialogue de propriétés qui affiche des informations telles que le type d'archive, la taille compressée et non compressée, les hachages cryptographiques MD5/SHA-1/SHA-256 de l'archive actuellement ouverte.

Ark peut aussi maintenant ouvrir et extraire des archives RAR sans utiliser les utilitaires rar non libres et peut ouvrir, extraire et créer des archives TAR compressées avec les formats lzop/lzip/lrzip.

L'interface utilisateur d'Ark a été améliorée en réorganisant la barre de menus et la barre d'outils afin d'améliorer la convivialité et de supprimer les ambiguïtés, ainsi que pour gagner de l'espace vertical grâce à la barre d'état désormais masquée par défaut.

### Plus de précisions pour les modifications de vidéos

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, l'éditeur vidéo non linéaire voit de nombreuses nouvelles fonctionnalités implémentées. Le titreur dispose désormais d'une fonctionnalité de grille, de dégradés, de l'ajout d'une ombre de texte et de l'ajustement de l'espacement des lettres/lignes.

Le nouvel affichage intégré des niveaux audio permet de plus facilement contrôler la composante audio de votre projet. D'autres fonctionnalités ont été rajoutées: nouvelles superpositions de moniteurs vidéo affichent la vitesse de lecture, zones de sécurité et visualisation des formes d'onde audio, barre d'outils permettant de rechercher des marqueurs et moniteur de zoom.

Une nouvelle fonctionnalité de bibliothèque permet de copier/coller des séquences entre les projets. Une nouvelle vue divisée dans la timeline permet de visualiser les changements apportés.

La boîte de dialogue de rendu a été réécrite et une option ajoutée pour ajouter le choix d'encodage rapide.

Des courbes dans les images clés ont été introduites pour quelques effets et vous pouvez maintenant accéder rapidement à vos effets préférés via le widget des effets favoris. Lorsque vous utilisez l'outil de rasoir, la ligne verticale de la ligne de temps indique désormais l'image précise où la coupe sera effectuée et les générateurs de clips récemment ajoutés vous permettront de créer des clips et des compteurs à barres de couleur. En outre, le compte d'utilisation des clips a été réintroduit dans le Project Bin et les vignettes audio ont également été réécrites pour les rendre beaucoup plus rapides.

Les principales corrections de bogues incluent la résolution des crash lors de l'utilisation des titres (qui nécessite la dernière version de MLT), la correction des corruptions survenant lorsque le taux d'images par seconde du projet est différent de 25, les plantages lors de la suppression des pistes, le mode d'écrasement problématique dans le timeline et les corruptions/effets perdus lors de l'utilisation d'une locale avec une virgule comme séparateur. En dehors de ces problèmes, l'équipe a travaillé sans relâche pour apporter des améliorations majeures en matière de stabilité, de flux de travail et de petites fonctionnalités d'utilisation.

### Et encore plus !

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, le visualiseur de documents apporte de nouvelles fonctionnalités sous la forme d'une bordure personnalisable de la largeur des annotations en ligne, la possibilité d'ouvrir directement les fichiers intégrés au lieu de seulement les enregistrer et aussi l'affichage d'une table des marqueurs de contenu lorsque les liens enfants sont réduits.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> a introduit un support amélioré pour GnuPG 2.1 avec une correction des erreurs de selftest et des rafraîchissements de clés encombrants causés par la nouvelle disposition du répertoire GnuPG (GNU Privacy Guard). La génération de clés ECC a été ajoutée avec l'affichage des détails de la courbe pour les certificats ECC. Kleopatra est maintenant publié en tant que paquet séparé et le support des fichiers .pfx et .crt a été inclus. Afin de résoudre la différence de comportement entre les clés secrètes importées et les clés générées, Kleopatra vous permet désormais de définir la confiance du propriétaire comme ultime pour les clés secrètes importées. 
