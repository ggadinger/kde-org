---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE publie les applications de KDE en version 18.08.2
layout: application
title: KDE publie les applications de KDE en version 18.08.2
version: 18.08.2
---
11 Octobre 2018. Aujourd'hui, KDE a publié la seconde mise à jour de consolidation pour les <a href='../18.08.0'> applications 18.08 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus d'une douzaine de corrections de bogues apportent des améliorations à Kontact, Dolphin, Gwenview, KCalc, Umbrello et bien d'autres.

Les améliorations incluses sont :

- Le glisser d'un fichier dans Dolphin ne peut plus par accident déclencher un renommage intégré.
- KCalc permet de nouveau d'utiliser les touches de point et de virgule lors de la saisie de nombres décimaux.
- Un problème visuel dans le jeu de cartes de Paris pour les jeux de cartes de KDE a été corrigé.
