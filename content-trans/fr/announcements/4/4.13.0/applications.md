---
date: '2014-04-16'
description: KDE publie la version 4.13 des applications et de la plate-forme de développement.
hidden: true
title: Les applications KDE 4.13 tirent profit de la nouvelle recherche sémantique
  et présentent de nouvelles fonctionnalités
---
La communauté KDE est fière d'annoncer les dernières mises à jour majeures des applications KDE apportant corrections et nouvelles fonctionnalités. Kontact (le gestionnaire d'informations personnelles) a été l'objet d'une activité intense, tirant profit des améliorations apportées à la technologie de recherche sémantique de KDE et jouissant de nouvelles fonctionnalités. La visionneuse de documents Okular et l'éditeur de texte avancé Kate ont have gotten interface-related and feature improvements. Dans les domaines de l'éducation et du jeu, nous avons intégré le nouveau foreign speech trainer Artikulate; Marble (the desktop globe) gets support for Sun, Moon, planets, bicycle routing and nautical miles. Palapeli (the jigsaw puzzle application) has leaped to unprecedented new dimensions and capabilities.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## Kontact acquiert de nouvelles fonctionnalités et de la vitesse !

La suite Kontact de KDE introduit une série de fonctionnalités dans l'ensemble de ses diverses composantes. KMail introduit le stockage dans les nuages (cloud) et améliore la prise en charge de Sieve avec le filtrage côté serveur, KNotes peut maintenant générer des alarmes et voit apparaître des fonctionnalités de recherche, et il y a eu beaucoup d'améliorations dans la couche de cache de données de Kontact, améliorant sa vitesse dans presque toutes les opérations.

### Stockage dans les nuages

KMail introduit la prise en charge d'un service de stockage, de sorte que les grosses pièces jointes puissent être stockées dans le service de nuage (cloud) et incluent en tant que lien dans le message du courriel. Les services pris en charge comprennent Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic et il existe une option générique WebDav. Un <em>gestionnaire de stockage en ligne</em> aide dans la gestion des fichiers par ces services.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Beaucoup d'améliorations dans la prise en charge de Sieve

Les filtres Sieve, une technologie qui laisse à KMail la gestion des filtres sur le serveur, peut maintenant gérer la prise en charge de vacation pour de multiples serveurs. L'outil KSieveEditor permet aux utilisateurs de modifier les filtres Sieve sans avoir à ajouter le serveur à Kontact.

### Autres changements

La barre de filtre rapide a reçu une petite amélioration de l'interface utilisateur et bénéficie grandement des capacités de recherche améliorées introduites dans l'environnement de développement de KDE 4.13. Rechercher est devenu significativement plus rapide et plus fiable. Le compositeur apporter un raccourci pour « URL », augmentant la traduction existante et les outils pour fragments textuels.

Les tags et les annotations des données PIM sont maintenant stockés dans Akonadi. Dans les futures versions, ils seront également stockés sur des serveurs (sur IMAP ou Kolab), rendant possible le partage des tags entre plusieurs ordinateurs. Akonadi : ajout de la prise en charge de l'API de Google Drive. Il existe une prise en charge de la recherche avec des modules externes tierces (ce qui signifie que les résultats peuvent être retrouvés très rapidement) et de la recherche par serveur (rechercher des items non indexés par un service d'indexation local).

### KNotes, KAddressbook

Un travail significatif a été fait sur KNotes, corrigeant une série de bogues et de petits ennuis. La possibilité de fixer une alarme sur les notes fait son apparition, ainsi que la recherches dans les notes. Pour en savoir plus, vous pouvez lire <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>ceci</a> (en anglais). KAdressbook dispose maintenant d'une prise en charge de l'impression : plus de détails <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>ici</a> (en anglais).

### Améliorations des performances

Les performances de Kontact ont été notablement améliorées dans cette version. Certaines améliorations sont dues à l'intégration de la nouvelle version de l'infrastructure de <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>recherche sémantique</a> de KDE et la couche de cache de données et le chargement des données dans KMail ont également fait l'objet d'un travail significatif. Un travail particulier a été fait pour améliorer la prise en charge de la base de données  »« PostgreSQL. Plus d'informations et de détails sur les changements liés aux performances sont disponibles dans ces liens :

- Optimisation du stockage: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>rapport du sprint</a>
- accélération et réduction de la taille de la base de données : <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>liste de diffusion</a>
- Optimisation de l'accès aux dossiers : <a href='https://git.reviewboard.kde.org/r/113918/'>Groupe de revue</a>

### KNotes, KAddressbook

Un travail significatif a été fait sur KNotes, corrigeant une série de bogues et de petits ennuis. La possibilité de fixer une alarme sur les notes fait son apparition, ainsi que la recherches dans les notes. Pour en savoir plus, vous pouvez lire <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>ceci</a> (en anglais). KAdressbook dispose maintenant d'une prise en charge de l'impression : plus de détails <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>ici</a> (en anglais).

## Okular modernise son interface utilisateur

Cette nouvelle version du lecteur de document Okular apporte une grande quantité d'améliorations. Vous pouvez ouvrir plusieurs fichiers « PDF » dans une seule fenêtre d'Okular grâce à la mise en place d'onglets. Une nouvelle loupe est disponible et la haute résolution de l'écran est prise en compte pour le rendu, ce qui améliore l'aspect des documents. Un bouton « Lecture » fait son apparition dans le mode de présentation et la recherche. De même, les actions « Annuler » / « Refaire » ont été améliorées.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Kate introduit une barre de statut améliorée, la concordance des crochets en animation, une amélioration des modules externes

La dernière version de l'éditeur de texte avancée Kate introduit <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>la concordance animée des crochets</a>, modifications pour faire fonctionner <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>les raccourcis de clavier activés par « AltGr » en mode vim</a> et une série d'améliorations dans les modules externes de Kate, en particulier dans le domaine de la prise en charge de Python et dans le <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>module externe de compilation</a>. Une <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>une nouvelle barre de statut grandement améliorée</a> fait son apparition. Elle permet d'activer des actions directes telles que changer les paramètres d'indentation, d'encodage ou de coloration syntaxique. Par ailleurs, une nouvelle barre d'onglets dans chaque vue est proposée, ainsi que la prise en charge de la complétion de code pour le <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>langage D</a> et <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>beaucoup plus encore</a>. L'équipe est <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>intéressée par des retours sur ce qui devrait être amélioré dans Kate</a> et reporte une partie de son intérêt vers le portage vers les environnements de développement KDE 5.

## Diverses fonctionnalités en bref

Konsole apporte une souplesse supplémentaire en permettant le contrôle des barres d'onglets par des feuilles de style personnalisées. Les profils peuvent désormais stocker les tailles souhaitées des colonnes et des lignes. Plus d'informations <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>ici</a>.

Umbrello rend possible la duplication des diagrammes et introduit les menus contextuels intelligents ajustant leur contenu au widgets sélectionné. La fonction « Annuler » a été améliorée ainsi que les propriétés visuelles. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>est maintenant capable de prévisualiser les fichiers « RAW »</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

Le mixeur de sons KMix a intégré le contrôle à distance via le protocole de communication inter-process « D-Bus » (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>details</a>), des éléments additionnels au menu de sons et une nouvelle boîte de dialogue de configuration (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>details</a>), un ensemble de correction de bogues et de petites améliorations.

L'interface de recherche de Dolphin a été modifiée pour tirer partie de la nouvelle infrastructure de recherche et a reçu des améliorations plus profondes sur les performances. Pour les détails, lisez cette <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>vue d'ensemble du travail d'optimisation effectué l'année dernière (en anglais)</a>.

KHelpcenter ajoute le tri par ordre alphabétique dans les modules et la réorganisation des catégories pour rendre son utilisation plus facile.

## Jeux et logiciels éducatifs

Les applications éducationnelles et les jeux de KDE ont reçu plusieurs mises à jour avec cette sortie. L'application de puzzle de KDE, Palapeli, a gagné de <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>nouvelles fonctionnalités sympathiques</a> qui rendent la résolution de grands puzzles (jusqu'à 10 000 pièces) beaucoup plus facile pour ceux qui veulent relever le défi. KNavalBattle montre les positions des navires ennemis après la fin du jeu de sorte que vous puissiez voir où vous avez fait chou blanc.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

Les application éducationnelles de KDE ont reçu de nouvelles fonctionnalités. Une interface de script via « D-Bus » a été ajoutée à KStars. Par ailleurs il peut utiliser l'API des services web de astrometry.net pour optimiser l'usage de la mémoire. Cantor voit apparaître une surbrillance de la syntaxe dans son éditeur de script et ses backends Scilab et Python 2 sont maintenant pris en charge dans l'éditeur. L'outil de navigation et de cartographie éducative Marble inclut dorénavant les positions <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>du soleil, de la lune</a> et <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>des planètes</a> (en anglais) et active <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>la capture de vidéo durant le tour virtuel</a>. Le routage des vélos a été amélioré avec la prise en charge de cyclestreets.net. Les miles nautiques sont maintenant pris en charge et cliquer sur un <a href='https://en.wikipedia.org/wiki/Geo_URI'>URI géo (en anglais)</a> ouvrira Marble.

#### Installation des applications de KDE

Les logiciels de KDE, y compris toutes ses bibliothèques et ses applications, sont disponibles gratuitement sous des licences « Open Source ». Ils fonctionnent sur diverses configurations matérielles, sur des architectures processeurs comme « ARM » ou « x86 » et sur des différents systèmes d'exploitation. Ils utilisent tout type de gestionnaire de fenêtres ou environnements de bureaux. A côté des systèmes d'exploitation Linux ou reposant sur UNIX, vous pouvez trouver des versions « Microsoft Windows » de la plupart des applications de KDE sur le site <a href='http://windows.kde.org'>Logiciels KDE sous Windows</a> et des versions « Apple Mac OS X » sur le site <a href='http://mac.kde.org/'>Logiciels KDE sous Mac OS X</a>. Des versions expérimentales des applications de KDE peuvent être trouvées sur Internet pour diverses plate-formes mobiles comme « MeeGo », « MS Windows Mobile » et « Symbian » mais qui sont actuellement non prises en charge. <a href='http://plasma-active.org'>Plasma Active</a> est une interface utilisateur pour une large variété de périphériques comme des tablettes et d'autres matériels mobiles. <br />

Les logiciels KDE peuvent être obtenus sous forme de code source et de nombreux formats binaires à l'adresse <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a>. Ils peuvent être aussi obtenus sur <a href='/download'>CD-ROM</a> ou avec n'importe quelle distribution <a href='/distributions'>majeure de systèmes GNU / Linux et UNIX</a> publiée à ce jour.

##### Paquets

Quelques fournisseurs de systèmes Linux / Unix mettent à disposition gracieusement des paquets binaires de 4.13.0 pour certaines versions de leurs distributions. Dans les autres cas, des bénévoles de la communauté le font aussi.

##### Emplacements des paquets

Pour obtenir une liste courante des paquets binaires disponibles, connus par l'équipe de publication de KDE, veuillez visiter le <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Wiki de la communauté</a>.

Le code source complet pour 4.13.0 peut être <a href='/info/4/4.13.0'>librement téléchargé</a>. Les instructions pour la compilation et l'installation des logiciels KDE 4.13.0 sont disponibles à partir de la <a href='/info/4/4.13.0#binary'>Page d'informations 4.13.0</a>.

#### Configuration minimale du système

Pour bénéficier au maximum de ces nouvelles versions, l'utilisation d'une version récente de Qt est recommandée, comme la version 4.8.4. Elle est nécessaire pour garantir un fonctionnement stable et performant, puisque certaines améliorations apportées aux logiciels de KDE ont été réalisées dans les bibliothèques Qt sous-jacentes.

Pour bénéficier au maximum des capacités des logiciels KDE, l'utilisation des tout derniers pilotes graphiques pour votre système est recommandée, car ceux-ci peuvent grandement améliorer votre expérience d'utilisateur, à la fois dans les fonctionnalités optionnelles que dans les performances et la stabilité en général.
