---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE rilascia KDE Applications 15.08.3
layout: application
title: KDE rilascia KDE Applications 15.08.3
version: 15.08.3
---
10 novembre 2015. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../15.08.0'>KDE Applications 15.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 20 bug corretti includono miglioramenti ad Ark, Dolphin, Kdenlive, kdepim, Kig, Lokalize e Umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.14.
