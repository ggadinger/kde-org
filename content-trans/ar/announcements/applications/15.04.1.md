---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE Ships Applications 15.04.1.
layout: application
title: تطلق كدي تطبيقات كدي 15.04.1
version: 15.04.1
---
May 12, 2015. Today KDE released the first stability update for <a href='../15.04.0'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

أُصلحت أكثر من 50 علّة منها تحسينات لِـ kdelibs، و kdepim، و kdenlive، وأوكلار، و marble و umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.19, KDE Development Platform 4.14.8 and the Kontact Suite 4.14.8.
