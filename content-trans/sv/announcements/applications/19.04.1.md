---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE levererar Program 19.04.1.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE levererar KDE-program 19.04.1
version: 19.04.1
---
{{% i18n_date %}}

Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../19.04.0'>KDE-program 19.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring tjugo registrerade felrättningar omfattar förbättringar av bland annat Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle och Umbrello.

Förbättringar omfattar:

- Etiketterade filer på skrivbordet avkortar inte längre etikettnamnet
- En krasch i Kmails funktion för textdelning har rättats
- Flera regressioner i videoeditorn Kdenlive har rättats
