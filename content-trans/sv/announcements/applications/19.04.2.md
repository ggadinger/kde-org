---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE levererar Program 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE levererar KDE-program 19.04.2
version: 19.04.2
---
{{% i18n_date %}}

Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../19.04.0'>KDE-program 19.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring femtio registrerade felrättningar omfattar förbättringar av bland annat Kontact, ARk, Dolphin, JuK, Kdenlive, Kmplot, Okular och Spectacle.

Förbättringar omfattar:

- En krasch vid visning av vissa EPUB-dokument i Okular har rättats
- Hemliga nycklar kan åter exporteras från kryptografihanteraren Kleopatra
- Kalarm händelsepåminnelse misslyckas inte längre starta med de nyaste biblioteken för personlig informationshantering
