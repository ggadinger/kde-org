---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE levererar Program 15.04.1.
layout: application
title: KDE levererar KDE-program 15.04.1
version: 15.04.1
---
12:e maj, 2015. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../15.04.0'>KDE-program 15.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 50 registrerade felrättningar omfattar förbättringar av kdelibs, kdepim, kdenlive, okular, marble och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av Plasma arbetsrymder 4.11.19, KDE:s utvecklingsplattform 4.14.8 och Kontakt-sviten 4.14.8.
