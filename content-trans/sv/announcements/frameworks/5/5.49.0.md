---
aliases:
- ../../kde-frameworks-5.49.0
date: 2018-08-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Instansiera inte en QStringRef i en QString bara för att söka i en QStringList
- Definiera element när de deklareras

### Baloo

- [tags_kio] Rätta flera filnamnskopior
- Återställ "[tags_kio] Använd UDS_URL istället för UDS_TARGET_URL."
- [tags_kio] Använd UDS_URL istället för UDS_TARGET_URL
- [tags_kio] Fråga om målets filsökvägar istället för att lägga till sökvägar i filens UDS-post
- Stöd särskilda webbadresser för att hitta filer av en viss typ
- Undvik manipulation av listor med kvadratisk komplexitet
- Använd fastInsert som inte avråds från i Baloo

### Breeze-ikoner

- Lägg till ikonen <code>drive-optical</code> (fel 396432)

### Extra CMake-moduler

- Android: Hårdkoda inte en slumpmässig version av Android SDK
- ECMOptionalAddSubdirectory: Tillhandahåll några fler detaljer
- Rätta kontroll av variabeldefinition
- Ändra 'since' version
- Förbättra ECMAddAppIconMacro

### Integrering med ramverk

- Ta hänsyn till BUILD_TESTING

### KActivities

- Ta hänsyn till BUILD_TESTING

### KArchive

- Ta hänsyn till BUILD_TESTING

### KAuth

- Undvik varningar för PolkitQt5-1 deklarationsfiler
- Ta hänsyn till BUILD_TESTING

### KBookmarks

- Ta hänsyn till BUILD_TESTING

### KCodecs

- Ta hänsyn till BUILD_TESTING

### KCompletion

- Ta hänsyn till BUILD_TESTING

### KConfig

- Ta hänsyn till BUILD_TESTING

### KConfigWidgets

- Ta hänsyn till BUILD_TESTING

### KCoreAddons

- Rätta överflöde i avrundningskod (fel 397008)
- Dokumentation av programmeringsgränssnitt: Ta bort kolon som inte ska vara där efter "@note"
- Dokumentation av programmeringsgränssnitt: Tala om nullptr; inte 0
- KFormat: Ersätt unicode litteral med unicode kodpunkt för att rätta MSVC-bygge
- KFormat: Korrigera @since etikett för ny KFormat::formatValue
- KFormat: Tillåt användning av kvantiteter förutom byte och sekunder
- Korrigera exempel på KFormat::formatBytes
- Ta hänsyn till BUILD_TESTING

### KCrash

- Ta hänsyn till BUILD_TESTING

### KDBusAddons

- Blockera inte för alltid i ensureKdeinitRunning
- Ta hänsyn till BUILD_TESTING

### KDeclarative

- Säkerställ att vi alltid skriver i gränssnittets rotsammanhang
- Bättre läsbarhet
- Förbättra dokumentation av programmeringsgränssnitt lite grand
- Ta hänsyn till BUILD_TESTING

### Stöd för KDELibs 4

- Rätta qtplugins i KStandardDirs

### KDocTools

- Ta hänsyn till BUILD_TESTING

### KEmoticons

- Ta hänsyn till BUILD_TESTING

### KFileMetaData

- Dokumentation av programmeringsgränssnitt: Lägg bara till @file i deklaration av bara funktioner för att få doxygen att hantera dem

### KGlobalAccel

- Ta hänsyn till BUILD_TESTING

### KDE GUI Addons

- Ta hänsyn till BUILD_TESTING

### KHolidays

- Installera beräkningsdeklarationen för soluppgång och solnedgång
- Tillägg av skottårsdagen som (kulturell) helg i Norge
- Tillägg av posten 'name' för norska helgfiler
- Tillägg av beskrivningar för norska helgfiler
- Fler uppdateringar av japanska helger från phanect
- holiday_jp_ja, holiday_jp-en_us - Uppdaterade (fel 365241)

### KI18n

- Återanvänd funktion som redan gör samma sak
- Rätta hantering av katalog och detektering av landsinställning på Android
- Läsbarhet, hoppa över satser med ingen operation
- Rätta KCatalog::translate när översättningen är samma som originaltexten
- Namnet på en fil har ändrats
- Låt ki18n makrofilnamnet följa stilen hos andra filer relaterade till find_package
- Rätta konfigurationskontrollen för _nl_msg_cat_cntr
- Generera inte filer i källkodskatalogen
- libintl: Avgör om _nl_msg_cat_cntr finns innan användning (fel 365917)
- Rätta byggen av binary-factory

### KIconThemes

- Ta hänsyn till BUILD_TESTING

### KIO

- Installera kategorifil för kdebugsettings relaterad till kio
- Byt namn på privat deklarationsfil till _p.h
- Ta bort eget ikonval för papperskorg (fel 391200)
- Justera beteckningar längs överkant i egenskapsdialogruta
- Presentera feldialogruta när användaren försöker skapa katalog vid namn "." or ".." (fel 387449)
- Dokumentation av programmeringsgränssnitt: Tala om nullptr; inte 0
- kcoredirlister lstItems prestandamätning
- [KSambaShare] Kontrollera filen som ändras innan återinläsning
- [KDirOperator] Använd alternerande bakgrundsfärger för flerkolumners visning
- Undvik minnesläcka i slavjobb (fel 396651)
- SlaveInterface: Avråd från användning av setConnection/connection, ingen kan ändå använda dem
- Något snabbare UDS-konstuktor
- [KFilePlacesModel] Stöd snygga webbadresser för baloosearch
- Ta bort webbgenvägen för projects.kde.org
- Byt ut KIO::convertSize() mot KFormat::formatByteSize()
- Använd fastInsert som inte avråds från i file.cpp (första av många kommande)
- Ersätt Gitorious webbgenväg med GitLab
- Visa inte normalt bekräftelsedialogruta för åtgärden Flytta till papperskorg (fel 385492)
- Fråga inte efter lösenord i kfilewidgettest

### Kirigami

- Stöd att lägga till och ta bort rubrik dynamiskt (fel 396417)
- Introducera actionsVisible (fel 396413)
- Anpassa marginaler när rullningslist visas eller försvinner
- Bättre hantering av storleken (fel 396983)
- Optimera inställning av paletten
- AbstractApplciationItem ska inte ha sin egen storlek, bara implicit
- Nya signaler pagePushed/pageRemoved
- Rätta logik
- Lägg till elementet ScenePosition (fel 396877)
- Ingen anledning att skicka intermediär palett för varje tillstånd
- dölj-&gt;visa
- Ihopdragbart sidoradsläge
- kirigami_package_breeze_icons: Behandla inte listor som element (fel 396626)
- Rätta sök och ersätt reguljärt uttryck (fel 396294)
- Animering av en färg producerar en ganska obehaglig effekt (fel 389534)
- Färglägg fokuserat objekt för tangentbordsnavigering
- Ta bort genväg för avsluta
- Ta bort sedan länge avråd från Encoding=UTF-8 från skrivbordsformatfil
- Rätta verktygsradstorlek (fel 396521)
- Rätta greppstorlek
- Ta hänsyn till BUILD_TESTING
- Visa ikoner för åtgärder som har en ikonkälla istället för ett ikonnamn

### KItemViews

- Ta hänsyn till BUILD_TESTING

### KJobWidgets

- Ta hänsyn till BUILD_TESTING

### KJS

- Ta hänsyn till BUILD_TESTING

### KMediaPlayer

- Ta hänsyn till BUILD_TESTING

### KNewStuff

- Ta bort sedan länge avråd från Encoding=UTF-8 från skrivbordsformatfiler
- Ändra förvald sorteringsordning i nerladdningsdialogrutan till Betyg
- Rätta fönstermarginaler för nerladdningsdialogruta för att följa allmänna temamarginaler
- Återställ oavsiktligt borttagen qCDebug
- Använd rätt QSharedPointer programmeringsgränssnitt
- Hantera tomma förhandsgranskningslistor

### KNotification

- Ta hänsyn till BUILD_TESTING

### Ramverket KPackage

- Ta hänsyn till BUILD_TESTING

### KParts

- Dokumentation av programmeringsgränssnitt: Tala om nullptr; inte 0

### KPeople

- Ta hänsyn till BUILD_TESTING

### KPlotting

- Ta hänsyn till BUILD_TESTING

### KPty

- Ta hänsyn till BUILD_TESTING

### Kör program

- Ta hänsyn till BUILD_TESTING

### KService

- Dokumentation av programmeringsgränssnitt: Tala om nullptr; inte 0
- Kräv bygge utanför källkod
- Lägg till operatorn subseq för att matcha delsekvenser

### KTextEditor

- Riktig rättning av automatisk citering av obehandlad strängindentering
- Rätta indenterare för att klara av ny syntaxfil i ramverket syntaxhighlighting
- Justera test till nytt tillstånd i arkivet syntax-highlighting
- Visa meddelandet "Sökning omstartad" mitt i vyn för bättre synlighet
- Rätta varning, använd bara isNull()
- Utökat skript-programmeringsgränssnitt
- Rätta segmenteringsfel i sällsynta fall när en tom vektor uppstår för ordantal
- Tvinga att förhandsgranskning av rullningslist rensas när dokumentet rensas (fel 374630)

### KTextWidgets

- Dokumentation av programmeringsgränssnitt: Återställ delvis tidigare incheckning, fungerade egentligen inte
- KFindDialog: Ge radeditorn fokus när en oanvänd dialogruta visas
- KFind: Nollställ antal när mönstret ändras (t.ex. i sökdialogrutan)
- Ta hänsyn till BUILD_TESTING

### KUnitConversion

- Ta hänsyn till BUILD_TESTING

### Ramverket KWallet

- Ta hänsyn till BUILD_TESTING

### Kwayland

- Städa RemoteAccess buffertar vid aboutToBeUnbound istället för när objektet förstörs
- Stöd markörtips för låst pekare
- Reducera onödigt långa väntetider vid fallerande signalspioner
- Rätta markering och automatisk test av säte
- Ersätt återstående V5 compat globala inkluderingar
- Lägg till stöd för XDG WM Base till vårt XDGShell programmeringsgränssnitt
- Gör det möjligt att kompilera XDGShellV5 tillsammans med XDGWMBase

### KWidgetsAddons

- Rätta KTimeComboBox indatamask för FM/EM tider (fel 361764)
- Ta hänsyn till BUILD_TESTING

### KWindowSystem

- Ta hänsyn till BUILD_TESTING

### KXMLGUI

- Rätta att KMainWindow sparar felaktiga inställningar av grafiska komponenter (fel 395988)
- Ta hänsyn till BUILD_TESTING

### KXmlRpcClient

- Ta hänsyn till BUILD_TESTING

### Plasma ramverk

- Om ett miniprogram är ogiltigt har det omedelbart UiReadyConstraint
- [Plasma PluginLoader] Lagra insticksprogram i cache vid start
- Rätta tonande nod när en strukturerad är en atlas
- [Containment] Ladda inte omgivningsåtgärder med plasma/containment_actions KIOSK restriktion.
- Ta hänsyn till BUILD_TESTING

### Prison

- Rätta lägeslåsning för blandat till versaler i Aztec kodgenerering

### Syfte

- Säkerställ att felsökning för kf5.kio.core.copyjob är inaktiverad vid test
- Återställ "test: Ett mer "odelbart" sätt att kontrollera om signalen sker"
- test: Ett mer "odelbart" sätt att kontrollera om signalen sker
- Lägg till Blåtandsinsticksprogram
- [Telegram] Vänta inte på att Telegram ska stängas
- Förbered att använda Arc:s statusfärger i kombinationslistan med versioner
- Ta hänsyn till BUILD_TESTING

### QQC2StyleBridge

- Förbättra storleksändring av menyer (fel 396841)
- Ta bort dubbel jämförelse
- Konvertera från strängbaserade anslutningar
- Kontrollera om ikon är giltig

### Solid

- Ta hänsyn till BUILD_TESTING

### Sonnet

- Sonnet: setLanguage ska schemalägga en omfärgläggning om färgläggning är aktiverad
- Använd aktuellt hunspell programmeringsgränssnitt

### Syntaxfärgläggning

- CoffeeScript: Rätta mallar i inbäddad Javascript-kode och lägg till undantag
- Undanta this i Definition::includedDefinitions()
- Använd medlemsinitiering i klass om möjligt
- Lägg till funktioner i åtkomstnyckelord
- Lägg till Definition::::formats()
- Lägg till QVector&lt;Definition&gt; Definition::includedDefinitions() const
- Lägg till Theme::TextStyle Format::textStyle() const;
- C++: Rätta standard flyttalslitteraler (fel 389693)
- CSS: Uppdatera syntax och rätta några fel
- C++: Uppdatera för c++20 och rätta några syntaxfel
- CoffeeScript och JavaScript: Rätta medlemsobjekt. Lägg till filändelsen .ts i JS (fel 366797)
- Lua: Rätta flerraders sträng (fel 395515)
- RPM-specifikation: Lägg till Mime-typer
- Python: Rätta undantag i quoted-comments (fel 386685)
- haskell.xml: Färglägg inte Prelude datakonstruktorer annorlunda än andra
- haskell.xml: Ta bort typer från sektionen "prelude function"
- haskell.xml: Färglägg befordrade datakonstruktorer
- haskell.xml: Lägg till nyckelorden family, forall, pattern
- Ta hänsyn till BUILD_TESTING

### ThreadWeaver

- Ta hänsyn till BUILD_TESTING

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
