---
aliases:
- ../../kde-frameworks-5.56.0
date: 2019-03-09
layout: framework
libCount: 70
---
### Baloo

- Ersätt flera Q_ASSERT med riktiga kontroller
- Kontrollera stränglängd för att undvika krasch för webbadressen "tags:/"
- [tags_kio] Rätta lokal filtaggning genom att bara kontrollera tag: webbadresser för dubbla snedstreck
- Hårdkoda återstående tidsuppdateringsintervall
- Rätta regression för att matcha explicit inkluderade kataloger
- Städa idempotenta poster från Mime-typ avbildningstabell
- [baloo/KInotify] Underrätta om en katalog har flyttats från obevakad plats (fel 342224)
- Hantera kataloger som matchar delsträngar av inkluderade eller exkluderade kataloger korrekt
- [balooctl] Normalisera inkluderings- och exkluderingssökvägar innan de används för inställning
- Optimera Baloo::File kopieringstilldelningsoperator, rätta Baloo::File::load(url)
- Använd innehåll för att bestämma Mime-typ (fel 403902)
- [Extractor] Undanta GPG-krypterad data från att indexeras (fel 386791)
- [balooctl] Avbryt verkligen ett felformat kommando istället för att bara säga det
- [balooctl] Lägg till saknad hjälp för "config set", normalisera sträng
- Ersätt rekursiv isDirHidden med iterativ, tillåt const argument
- Säkerställ att bara kataloger läggs till i inotify övervakning

### Breeze-ikoner

- Lägg till ikonen code-oss
- [breeze-icons] Lägg till videokameraikoner
- [breeze-icons] Använd nya användarikoner för suspend, hibernate och switch user i Breeze ikontema
- Lägg till 16 bildpunkters och 22 bildpunkters versioner av gamepad ikonen i devices/
- Gör Breeze-tema verktygstipstexter konsekventa
- Lägg till batteri-ikoner
- Byt namn på ikonerna "visibility" och "hint" till "view-visible" och "view-hidden"
- [breeze-icons] Lägg till svartvita/mindre ikoner för SD-kort och minnessticka (fel 404231)
- Lägg till enhetsikoner för drönare
- Ändra C/C++ Mime-typikoner för deklaration/källkod till cirkel/linjestil
- Rätta saknade skuggor för C/C++ Mime-typikoner för deklaration (fel 401793)
- Ta bort svartvit ikon för teckensnittspreferenser
- Förbättra teckenurvalsikon
- Använd ny ikon med klockstil för alla användare av preferences-desktop-notification (fel 404094)
- [breeze-icons] Lägg till 16-bildpunkters versioner av gnumeric-font.svg och länka gnumeric-font.svg till font.svg
- Lägg till symboliska länkar för preferences-system-users som pekar på yast-users ikon
- Lägg till ikonen edit-none

### Extra CMake-moduler

- Rätta utcheckning av releaseme när den ingår i en underkatalog
- Ny sökmodul för Canberra
- Uppdatera Android verktygskedjans filer till verkligheten
- Lägg till kompileringskontroller för FindEGL

### KActivities

- Använd naturlig sortering i ActivityModel (fel 404149)

### KArchive

- Skydda KCompressionDevice::open från att anropas när inget gränssnitt är tillgängligt (fel 404240)

### KAuth

- Tala om för folk att de i huvudsak ska använda KF5::AuthCore
- Kompilera vår egen hjälpfunktion med AuthCore och inte Auth
- Introducera KF5AuthCore

### KBookmarks

- Ersätt beroende på KIconThemes med ekvivalent användning av QIcon

### KCMUtils

- Använd KCM namn i KCM sidhuvud
- Lägg till saknad ifndef KCONFIGWIDGETS_NO_KAUTH
- Anpassa till ändringar i kconfigwidgets
- Synkronisera QML-modulens vaddering så att den motsvarar systeminställningssidorna

### KCodecs

- Rättning av CVE-2013-0779
- QuotedPrintableDecoder::decode: returnera false vid fel istället för assert
- Markera att KCodecs::uuencode inte gör någonting
- nsEUCKRProber/nsGB18030Prober::HandleData kraschar inte om aLen är 0
- nsBig5Prober::HandleData: Krascha inte om aLen är 0
- KCodecs::Codec::encode: Ingen assert/krasch om makeEncoder returnerar null
- nsEUCJPProbe::HandleData: Krascha inte om aLen är 0

### KConfig

- Skriv giltiga UTF8-kataloger utan undantag (fel 403557)
- KConfig: Hantera symboliska kataloglänkar korrekt

### KConfigWidgets

- Hoppa över prestandamätning om inga scheme-filer kan hittas
- Lägg till en anmärkning att KF6 ska använda kärnversionen av KF5::Auth
- Lagra förvald KColorScheme inställning i cache

### KCoreAddons

- Skapa tel: länkar för telefonnummer

### KDeclarative

- använd KPackage::fileUrl för att stödja rcc inställningsmodulpaket
- [GridDelegate] Rätta långa beteckningar som blandas ihop med varandra (fel 404389)
- [GridViewKCM] förbättra kontrast och läsbarhet för delegaternas knappar som visas på plats när musen hålls över (fel 395510)
- Korrigera accept-flaggan för händelseobjekt vid DragMove (fel 396011)
- Använd annan "None" objektikon i inställningsmoduler med rutnätsvy

### KDESU

- kdesud: KAboutData::setupCommandLine() ställer redan in hjälp och version

### KDocTools

- Konvertera korskompileringsstöd till KF5_HOST_TOOLING
- Rapportera bara att DocBookXML hittas om det verkligen hittas
- Uppdatera spanska poster

### KFileMetaData

- [Extractor] Lägg till metadata för extrahering (fel 404171)
- Lägg till extrahering av AppImage-filer
- Städa ffmpeg-extrahering
- [ExternalExtractor] Tillhandahåll hjälpsammare utmatning när extrahering misslyckas
- Formatera EXIF-fotoblixtdata (fel 343273)
- Undvik sidoeffekter på grund av gammalt errno-värde
- Använd Kformat för bit- och samplingshastighet
- Lägg till enheter för bildhastighet och gps-data
- Lägg till funktion för strängformatering i egenskapsinformation
- Undvik att läcka QObject in ExternalExtractor
- Hantera &lt;a&gt; som behållarelement i SVG
- Kontrollera Exiv2::ValueType::typeId innan det konverteras till rational

### KImageFormats

- ras: Rätta krasch för felaktiga filer
- ras: skydda också palettens QVector
- ras: finjustera kontroll av maximala filer
- xcf: Rätta användning av oinitierat minne för felaktiga dokument
- lägg till const, hjälper till att förstå funktionen bättre
- ras: finjustera maximal storlek som "får plats" i en QVector
- ras: ingen assert eftersom vi försöker skapa en jättestor vektor
- ras: Skydda mot dividera med noll
- xcf: Dividera inte med 0
- tga: misslyckas på ett snyggt sätt för fel i readRawData
- ras: misslyckas på ett snyggt sätt för height*width*bpp &gt; length

### KIO

- kioexec: KAboutData::setupCommandLine() ställer redan in hjälp och version
- Rätta krasch i Dolphin när en fil från papperskorgen släpps i papperskorgen (fel 378051)
- Avkorta mycket långa filnamn i felsträngar mitt på (fel 404232)
- Lägg till stöd för portaler i KRun
- [KPropertiesDialog] Rätta gruppkombinationsruta (fel 403074)
- Försök att hitta kioslave binärfil på ett riktigt sätt i $libexec OCH $libexec/kf5
- Använd AuthCore istället för Auth
- Lägg till ikonnamn för tjänstleverantörer i .desktop-fil
- Läs ikon för IKWS sökleverantör från skrivbordsfil
- [PreviewJob] Skicka också med att vi är miniatyrbildsgeneratorn när stat görs för en fil (fel 234754)

### Kirigami

- ta bort det felaktiga bråket med contentY i refreshabeScrollView
- lägg till OverlayDrawer i grejerna som är dokumenterbara av doxygen
- avbilda currentItem på vyn
- Riktig färg på nedåtpilikonen
- SwipeListItem: gör utrymme för åtgärderna när !supportsMouseEvents (fel 404755)
- Kolumnvy och partiell C++ omstrukturering av PageRow
- vi kan använda som mest kontroller 2.3 som Qt 5.10
- rätta höjd på horisontella lådor
- Förbättra verktygstips i komponenten ActionTextField
- Lägg till en ActionTextField-komponent
- Rätta mellanrum för knappar (fel 404716)
- Rätta knappstorlek (fel 404715)
- GlobalDrawerActionItem: referera till ikon på rätt sätt genom att använda gruppegenskap
- visa avskiljare om huvudverktygsrad är osynlig
- lägg till en standardbakgrund för sida
- DelegateRecycler: Rätta att översättning använder fel domän
- Rätta varningar när QQuickAction används
- Ta bort några onödiga konstruktioner med QString
- Visa inte verktygstipset när kombinationsmenyn visas (fel 404371)
- Dölj skuggor om stängd
- Lägg till nödvändiga egenskaper för alternativfärg
- återställ det mesta av heuristisk ändring av ikonfärgläggning
- hantera grupperade egenskaper riktigt
- [PassiveNotification] Starta inte tidtagning innan fönster har fokus (fel 403809)
- [SwipeListItem] Använd en riktig verktygsknapp för att förbättra användbarhet (fel 403641)
- stöd för valfria alternerande bakgrunder (fel 395607)
- visa bara grepp när det finns synliga åtgärder
- stöd färgade ikoner i åtgärdsknappar
- visa alltid bakåtknappen på lager
- Uppdatera SwipeListItem dokument till QQC2
- rätta logik för updateVisiblePAges
- exponera synliga sidor i pagerow
- dölj länkstig när den aktuella sidan har en verktygsrad
- stöd överskridning av sidor med verktygsradsstil
- ny egenskap på sida: titleDelegate för att överskrida titeln i verktygsrader

### KItemModels

- KRearrangeColumnsProxyModel: gör tvåkolumners avbildningsmetoder öppna

### KNewStuff

- Filtrera ut ogiltigt innehåll i listor
- Rätta minnesläcka hittad av asan

### KNotification

- konvertera till findcanberra från ECM
- Lista Android som officiellt stödd

### Ramverket KPackage

- ta bort avrådningsvarning för kpackage_install_package

### KParts

- mallar: KAboutData::setupCommandLine() ställer redan in hjälp och version

### Kross

- Installera Kross-moduler i ${KDE_INSTALL_QTPLUGINDIR}

### KService

- kbuildsycoca5: inget behov av att upprepa arbetet i KAboutData::setupCommandLine()

### KTextEditor

- försök förbättra målarhöjd för textrader - fel 403868 undvik cut _ och andra delar som fortfarande är sönder: ge saker som blandad engelska/arabiska dubbel höjd, se fel 404713
- Använd QTextFormat::TextUnderlineStyle istället för QTextFormat::FontUnderline (fel 399278)
- Gör det möjligt att visa alla mellanslag i dokumentet (fel 342811)
- Skriv inte ut indenterade rader
- KateSearchBar: Visa också tips om att sökningen har gått runt i nextMatchForSelection(), samma som Ctrl-H
- katetextbuffer: omstrukturera TextBuffer::save() för att bättre skilja kodvägar åt
- Använd AuthCore istället för Auth
- Omstrukturera KateViewInternal::mouseDoubleClickEvent(QMouseEvent *e)
- Förbättringar av komplettering
- Ställ in färgschemat till Utskrift för Förhandsgranskning av utskrift (fel 391678)

### Kwayland

- Checka bara in XdgOutput::done om ändrad (fel 400987)
- FakeInput: lägg till stöd för pekarförflyttning med absolut koordinat
- Lägg till saknad XdgShellPopup::ackConfigure
- [server] Lägg till proxymekanism för ytdata
- [server] Lägg till signalen selectionChanged

### KWidgetsAddons

- Använd riktig KStandardGuiItem "nej" ikon

### Plasma ramverk

- [Ikonobjekt] Block next animation also based on window visibility
- Visa en varning om ett insticksprogram kräver en nyare version
- Öka temaversionerna eftersom ikonerna ändrades, för att invalidera gamla cachar.
- [breeze-icons] Gör om system.svgz
- Gör Breeze-tema verktygstipstexter konsekventa
- Ändra glowbar.svgz till jämnare stil (fel 391343)
- Använd reserv för bakgrundskontrast vid körtid (fel 401142)
- [breeze desktop theme/dialogs] Lägg till rundade hörn på dialogrutor

### Syfte

- pastebin: visa inte förloppsinformation (fel 404253)
- sharetool: Visa delad webbadress överst
- Rätta delade filer med mellanslag eller citationstecken i namn via Telegram
- Låt ShareFileItemAction tillhandahålla utmatning eller ett fel om det tillhandahålls (fel 397567)
- Aktivera att dela webbadresser via e-post

### QQC2StyleBridge

- Använd PointingHand när musen hålls över länkar i beteckning
- Respektera knappars visningsegenskap
- att klicka på tomma områden beter sig som pgup/pgdown (fel 402578)
- Stöd ikon för ComboBox
- stöd programmeringsgränssnitt för textpositionering
- Stöd ikoner från lokala filer i knappar
- Använd korrekt markör när musen hålls över den redigerbara delen av en nummerruta

### Solid

- Bringa FindUDev.cmake upp till ECM-standarder

### Sonnet

- Hantera fallet då createSpeller skickas ett otillgängligt språk

### Syntaxfärgläggning

- Rätta varning om arkivborttagning
- MustacheJS: färglägg också mallfiler, rätta syntax och förbättra stöd för Handlebars
- gör oanvänt sammanhang till fatalt för indexering
- Uppdatera example.rmd.fold och test.markdown.fold med nya nummer
- Installera DefinitionDownloader deklarationsfil
- Uppdatera octave.xml till Octave 4.2.0
- Förbättra färgläggning av TypeScript (och React) och lägg till fler tester för PHP
- Lägg till mer färgläggning för nästlade språk i markdown
- Returnera sorterade definitioner för filnamn och Mime-typer
- lägg till saknad ref uppdatering
- BrightScript: Unära och hexadecimala nummer, @attribute
- Undvik duplicerade *-php.xml filer i "data/CMakeLists.txt"
- Lägg till funktioner som returnerar alla definitioner för en Mime-typ eller filnamn
- uppdatera literate haskell Mime-typ
- förhindra assert i laddning av reguljärt uttryck
- cmake.xml: Uppdatera för version 3.14
- CubeScript: rättar radfortsättning med undantag i strängar
- lägg till viss minimal handledning för tillägg av test
- R Markdown: förbättra vikning av block
- HTML: färglägg JSX, TypeScript och MustacheJS kod i taggen &lt;script&gt; (fel 369562)
- AsciiDoc: Lägg till vikning för sektioner
- FlatBuffers schema syntaxfärgläggning
- Lägg till några konstanter och funktioner för Maxima

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
