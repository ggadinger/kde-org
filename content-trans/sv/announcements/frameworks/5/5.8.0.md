---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nya ramverk:

- KPeople, ger tillgång till alla kontakter och personerna som de gäller
- KXmlRpcClient, interaktion med XMLRPC-tjänster

### Allmänt

- Ett antal byggrättningar för att kompilera med det kommande Qt 5.5

### KActivities

- Tjänsten för resursbetygsättning är nu färdigställd

### KArchive

- Sluta misslyckas för ZIP-filer med redundanta datadeskriptorer

### KCMUtils

- Återställ KCModule::setAuthAction

### KCoreAddons

- KPluginMetadata: Lägg till stöd för nyckeln Hidden

### KDeclarative

- Föredra att exponera listor för QML med QJsonArray
- Hantera annat än standardvärden för devicePixelRatios i bilder
- Exponera hasUrls i DeclarativeMimeData
- Tillåt användare att ställa in hur många horisontella linjer som ritas

### KDocTools

- Rätta byggning på MacOSX när Homebrew används
- Bättre stil för mediaobjekt (bilder, ...) i dokumentation
- Koda ogiltiga tecken i sökvägar som används av XML DTD:er, för att undvika fel

### KGlobalAccel

- Tidsstämpel för aktivering ställs in som dynamisk egenskap för utlöst QAction.

### KIconThemes

- Rätta QIcon::fromTheme(xxx, någotreservvärde) som inte returnerade reservvärdet

### KImageFormats

- Gör PSD-bildläsaren oberoende av endianess.

### KIO

- Avråd från användning av UDSEntry::listFields och lägg till metoden UDSEntry::fields som returnerar en QVector utan kostsam konvertering.
- Synkronisera bara bookmarkmanager om ändringarna gjordes av den här processen (fel 343735)
- Rätta start av DBus-tjänst kssld5
- Implementera quota-used-bytes och quota-available-bytes från RFC 4331 för att ge tillgång till information om ledigt utrymme i I/O-slaven http.

### KNotifications

- Fördröj initiering av ljud tills det verkligen behövs
- Rätta att inställning av underrättelser inte verkställs omedelbart
- Rätta att ljudunderrättelser slutar efter första filen som spelats

### KNotifyConfig

- Lägg till valfritt beroende på QtSpeech för att aktivera uppläsningsunderrättelser igen.

### KService

- KPluginInfo: Stöd stränglistor som egenskaper

### KTextEditor

- Lägg till statistik över antal ord i statusraden
- vimode: Rätta krasch när sista raden tas bort i visuellt radläge

### KWidgetsAddons

- Låt KRatingWidget hanterar devicePixelRatio

### KWindowSystem

- KSelectionWatcher och KSelectionOwner kan användas utan att bero på QX11Info.
- KXMessages kan användas utan beroende på QX11Info

### NetworkManagerQt

- Tillägg av nya egenskaper och metoder från NetworkManager 1.0.0

#### Plasma ramverk

- Rätta plasmapkg2 för översatta system
- Förbättra layout av verktygstips
- Gör det möjligt att låta Plasmoider ladda skript utanför Plasma-paketet...

### Ändringar av byggsystem (extra-cmake-modules)

- Utöka makrot ecm_generate_headers för att också stödja deklarationsfiler såsom CamelCase.h

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
