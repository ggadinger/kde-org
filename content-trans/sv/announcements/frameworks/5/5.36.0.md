---
aliases:
- ../../kde-frameworks-5.36.0
date: 2017-07-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Alla ramverk: Alternativ för att bygga och installera QCH-fil med dox för det öppna programmeringsgränssnittet

### Baloo

- Använd FindInotify.cmake för att bestämma om inotify är tillgänglig

### Breeze-ikoner

- Bero inte på bash i onödan, och validera inte ikoner standardmässigt

### Extra CMake-moduler

- FindQHelpGenerator: Undvik att hämta Qt4-version
- ECMAddQch: Misslyckas fatalt om nödvändiga verktyg inte finns, för att undvika överraskningar
- Sluta använda perl som beroende för ecm_add_qch, behövs och används inte
- Sök igenom hela installationskatalogen för QML-beroenden
- Ny: ECMAddQch, för att skapa qch- och doxygen-taggfiler
- Rätta KDEInstallDirsTest.relative_or_absolute_usr, för att undvika att Qt-sökvägar används

### KAuth

- Kontrollera felstatus efter varje användning av PolKitAuthority

### KBookmarks

- Skicka ut fel när keditbookmarks saknas (fel 303830)

### KConfig

- Rättning för CMake 3.9

### KCoreAddons

- Använd FindInotify.cmake för att bestämma om inotify är tillgänglig

### KDeclarative

- KKeySequenceItem: Gör det möjligt att spela in Ctrl+Num+1 som genväg
- Starta drag med tryck ner och håll för pekhändelser (fel 368698)
- Lita inte på att QQuickWindow levererar QEvent::Ungrab som mouseUngrabEvent (eftersom det inte gör det längre i Qt 5.8+) (fel 380354)

### Stöd för KDELibs 4

- Sök efter KEmoticons, som är ett beroende enligt CMake config.cmake.in (fel 381839)

### KFileMetaData

- Lägg till extrahering som använder qtmultimedia

### KI18n

- Säkerställ att målet tsfiles skapas

### KIconThemes

- Mer information om att distribuera ikonteman på Mac och MSWin
- Ändra panelens standardikonstorlek till 48

### KIO

- [KNewFileMenu] Dölj menyn "Länk till enhet" om den skulle vara tom (fel 381479)
- Använd KIO::rename istället för KIO::moveAs i setData (fel 380898)
- Rätta öppnad menyposition på Wayland
- KUrlRequester: Ställ in NOTIFY signal till textChanged() för textegenskap
- [KOpenWithDialog] HTML-escape filnamn
- KCoreDirLister::cachedItemForUrl: Skapa inte cachen om den inte fanns
- Använd "data" som filnamn när datawebbadresser kopieras (fel 379093)

### KNewStuff

- Rätta felaktig feldetektering för saknade knsrc-filer
- Exponera och använd gränssnittets variabel för sidstorlek
- Gör det möjligt att använda QXmlStreamReader för att läsa en KNS-registerfil

### Ramverket KPackage

- Använd kpackage-genericqml.desktop

### KTextEditor

- Rätta topp i processoranvändning efter att vi-kommandoraden visas (fel 376504)
- Rätta ryckig dragning av rullningslist när miniavbildning är aktiverad
- Hoppa till den klickade rullningslistens position när miniavbildningen är aktiverad (fel 368589)

### KWidgetsAddons

- Uppdatera kcharselect-data till Unicode 10.0

### KXMLGUI

- KKeySequenceWidget: Gör det möjligt att lagra Ctrl+Num+1 som en genväg (fel 183458)
- Återställ "När menyhierarkier byggs, överliggande menyer till sina omgivningar"
- Återställ "använd transient överliggande objekt direkt"

### NetworkManagerQt

- WiredSetting: Vakna på LAN-egenskaper konverterades tillbaka till NM 1.0.6
- WiredSetting: Uppmätta egenskaper konverterades tillbaka till NM 1.0.6
- Lägg till nya egenskaper i många inställningsklasser
- Enheter: Lägg till enhetsstatistik
- Lägg till IpTunnel-enhet
- WiredDevice: Lägg till information om nödvändig NM version för egenskapen s390SubChannels
- TeamDevice: Lägg till ny konfigurationsegenskap (sedan NM 1.4.0)
- WiredDevice: Lägg till egenskapen s390SubChannels
- Uppdatera introspektioner (NM 1.8.0)

### Plasma ramverk

- Säkerställ att storleken är slutlig efter showEvent
- Rätta marginaler och färgschema för vlc-ikon i systembrickan
- Ställ in omgivning för att ha fokus inom vyn (fel 381124)
- Generera den gamla nyckeln innan uppdatering av enabledborders (fel 378508)
- Visa lösenordsknapp också för tom text (fel 378277)
- Skicka ut usedPrefixChanged när prefix är tomt

### Solid

- CMake: Bygg udisks2-gränssnitt på FreeBSD bara om aktiverat

### Syntaxfärgläggning

- Färglägg .julius-filer som Javascript
- Haskell: Lägg till alla språkets pragma som nyckelord
- CMake: OR/AND färgläggs inte efter uttryck i () (fel 360656)
- Makefile: Ta bort ogiltiga nyckelordsposter i makefile.xml
- Indexering: Förbättra felrapportering
- Versionsuppdatering av HTML-syntaxfil
- Vinkelmodifierare i HTML-egenskaper tillagda
- Uppdatera testreferensdata i enlighet med ändringarna i föregående incheckning
- Fel 376979 - vinkelparenteser i doxygen-kommentarer förstör syntaxfärgläggning

### ThreadWeaver

- Undvik MSVC2017-kompilatorfel

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
