---
aliases:
- ../announce-applications-14.12-rc
custom_spread_install: true
date: '2014-11-27'
description: KDE Ships Applications 14.12 Release Candidate.
layout: application
title: KDE выпускает первую версию-кандидат к выпуску KDE Applications 14.12
---
27 ноября 2014 года. Сегодня KDE выпустило версию-кандидат к выпуску KDE Applications. Теперь, когда программные зависимости и функциональность «заморожены», команда KDE сконцентрируется на исправлении ошибок и наведении красоты.

With various applications being based on KDE Frameworks 5, the KDE Applications 14.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

#### Установка двоичных пакетов KDE Applications 14.12 RC

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 14.12 Release Candidate (internally 14.11.97) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Компиляция KDE Applications 14.12 RC

The complete source code for KDE Applications 14.12 Release Candidate may be <a href='http://download.kde.org/unstable/applications/14.11.97/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-14.11.97'>KDE Applications Release Candidate Info Page</a>.
