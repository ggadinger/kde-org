---
aliases:
- ../../kde-frameworks-5.23.0
date: 2016-06-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Mogelijk maken de leveranciers bekend te maken aan de hand van de ontvangen url
- QDebug helpers leveren voor enige Attica classes
- Doorverwijzing van absolute Url's repareren (bug 354748)

### Baloo

- Gebruik van spaties in de tags van kioslave repareren (bug 349118)

### Breeze pictogrammen

- Een CMake optie toevoegen om binaire Qt resource te bouwen vanuit map met pictogrammen
- Veel nieuwe en bijgewerkte pictogrammen
- pictogram voor netwerkverbinding verbreken voor een groter verschil met die voor verbonden (bug 353369)
- pictogram voor aan- en afkoppelen bijwerken (bug 358925)
- enige avatars toevoegen uit plasma-desktop/kcms/useraccount/pics/sources
- chromium pictogram verwijderen omdat het standaard chromium pictogram goed past (bug 363595)
- de konsole pictogrammen lichter maken (bug 355697)
- pictogram voor e-mail voor Thunderbird toegevoegd (bug 357334)
- pictogram voor publieke sleutel toegevoegd (bug 361366)
- process-working-kde verwijderen omdat de konqueror pictogrammen gebruikt zouden moeten worden (bug 360304)
- pictogrammen voor krusader bijwerken volgens (bug 359863)
- de mic pictogrammen hernoemen volgens D1291 (bug D1291)
- pictogrammen voor MIME-typen van scripts toevoegen (bug 363040)
- virtueel toetsenbord en touchpad aan/uit functionaliteit toevoegen voor OSD

### Frameworkintegratie

- Ongebruikte afhankelijkheden en behandeling van vertaling verwijderen

### KActivities

- Eigenschap runningActivities toevoegen aan de Consumer

### KDE Doxygen hulpmiddelen

- Belangrijk bewerken van de generatie van API docs

### KCMUtils

- QQuickWidget gebruiken voor QML KCM's (bug 359124)

### KConfig

- Overslaan van KAuthorized controle vermijden

### KConfigWidgets

- Gebruik van nieuwe stijl verbindingssyntaxis toestaan met KStandardAction::create()

### KCoreAddons

- De niet werkende plug-in tonen bij melden van een cast-waarschuwing
- [kshareddatacache] ongeldig gebruik van &amp; repareren om niet uitgelijnd lezen te vermijden
- Kdelibs4ConfigMigrator: opnieuw ontleden overslaan als er niets was gemigreerd
- krandom: testcase toevoegen om bug 362161 te vangen (mislukking van auto-seed)

### KCrash

- Groote van "unix domain socket path" controleren alvorens het te kopiëren

### KDeclarative

- Geselecteerde status ondersteunen
- KCMShell import kan nu gebruikt worden om af te vragen of het openen van een KCM is werkelijk is toegestaan

### Ondersteuning van KDELibs 4

- Waarschuwing over KDateTimeParser::parseDateUnicode die niet is geïmplementeerd
- K4TimeZoneWidget: corrigeer pad naar vlagafbeeldingen

### KDocTools

- Algemeen gebruikte entities voor sleutels toevoegen aan en/user.entities
- man-docbook sjablonen bijwerken
- Sjablonen voor boeken, manpagina's en artikelen bijwerken
- kdoctools_create_handbook alleen aanroepen voor index.docbook (bug 357428)

### KEmoticons

- emojis ondersteuning toevoegen aan KEmoticon + Emoji One pictogrammen
- Ondersteuning toegevoegd voor eigen afmetingen van emoticons

### KHTML

- Potentieel geheugenlek repareren gerapporteerd door Coverity en de code vereenvoudigen
- Het aantal lagen is bepaald door het aantal kommagescheiden waarden in de eogenschap ‘background-image’
- Ontleden van background-position in verkleinde declaratie repareren
- Maak geen nieuw fontFace als er geen geldige bron is

### KIconThemes

- Maak geen KIconThemes hangt af van Oxygen (bug 360664)
- Geselecteerd statusconcept voor pictogrammen
- Systeemkleuren gebruiken voor monochrome pictogrammen

### KInit

- Race repareren waarin het bestand met de X11-cookie gedurende een korte tijd de verkeerde rechten heeft
- Rechten van /tmp/xauth-xxx-_y repareren

### KIO

- Meer heldere foutmelding geven wanneer KRun(URL) een URL krijgt zonder schema (bug 363337)
- KProtocolInfo::archiveMimetypes() toevoegen
- geselecteerde pictogrammodus gebruiken in dialoog in zijbalk voor openen bestand
- kshorturifilter: regressie repareren met mailto: niet voorgevoegd wanneer er geen mailer is geïnstalleerd

### KJobWidgets

- Juiste "dialoog"vlag voor de dialoog van voortgangswidget instellen

### KNewStuff

- KNS3::DownloadManager niet initialiseren met de verkeerde categorieën
- KNS3::Entry publieke API uitbreiden

### KNotification

- QUrl::fromUserInput gebruiken om gezonde url te maken (bug 337276)

### KNotifyConfig

- QUrl::fromUserInput gebruiken om gezonde url te maken (bug 337276)

### KService

- Geassocieerde toepassingen voor mime-types met hoofdletters repareren
- Maak de zoeksleutel voor mimetypes in kleine letters, om het ongevoelig te maken voor hoofd- of kleine letters
- ksycoca meldingen repareren wanneer de DB nog niet bestaat

### KTextEditor

- Standaard codering naar UTF-8 repareren (bug 362604)
- Instelbaarheid van kleur van standaard stijl "Fout" repareren
- Zoeken &amp; vervangen: vervangen van achtergrondkleur repareren (regressie geïntroduceerd in v5.22) (bug 363441)
- Nieuw kleurschema "Breeze Dark", zie https://kate-editor.org/?post=3745
- KateUndoManager::setUndoRedoCursorOfLastGroup(): geeft Cursor door als const referentie
- sql-postgresql.xml verbeter accentuering van syntaxis door negeren van functiebodies uit meerdere regels
- Syntaxisaccentuering voor Elixir en Kotlin toevoegen
- accentuering van syntaxis van VHDL in ktexteditor: ondersteuning voor functies binnen architectuurstatements
- vimode: niet crashen wanneer een reeks is gegeven voor een niet bestaand commando (bug 360418)
- Samengestelde tekens op de juiste manier verwijderen bij gebruik van Indic locales

### KUnitConversion

- Downloaden van wisselkoersen repareren (bug 345750)

### KWallet Framework

- Migratie van KWalletd: foutbehandeling repareren, stopt de migratie te gebeuren bij elk opstarten van het systeem.

### KWayland

- [client] Hulpbronversie niet controleren voor PlasmaWindow
- Een initieel statusgebeurtenis introduceren in Plasma vensterprotocol
- [server] Start een fout indien een overgangsverzoek om ouder van zichzelf te worden
- [server] Behandel het geval dat de verbinding met een Plasma-venster is verbroken voordat een client er zich mee verbindt
- [server] Behandel vernietiger in SlideInterface op de juiste manier
- Ondersteuning toevoegen voor aanrakingen in fakeinput-protocol en interface
- [server] Het afhandelen van een vernietigingsverzoek voor hulpbronnen standaardiseren
- wl_text_input en zwp_text_input_v2 interfaces implementeren
- [server] Het dubbel verwijderen van hulpmiddelen voor callback in SurfaceInterface voorkomen
- [server] Controle op nullptr naar hulpbron ShellSurfaceInterface toevoegen
- [server] ClientConnection vergelijken in plaats van wl_client in SeatInterface
- [server] De behandeling verbeteren wanneer clients verbinding verbreken
- server/plasmawindowmanagement_interface.cpp - waarschuwing -Wreorder repareren
- [client] Contextpointer naar verbindingen in PlasmaWindowModel toevoegen
- Veel reparties gerelateerd aan vernietiging

### KWidgetsAddons

- Geselecteerd pictogrameffect gebruiken voor huidige KPageView pagina

### KWindowSystem

- [platform xcb] Gevraagde pictogramgrootte respecteren (bug 362324)

### KXMLGUI

- Rechtsklikken op de menubalk van een toepassing zal nu langer voorbijgaan toestaan

### NetworkManagerQt

- WiMAX ondersteuning voor NM 1.2.0+ laten vallen omgedraaid omdat het ABI breekt

### Oxygen-pictogrammen

- Weer pictogrammen met breeze synchroniseren
- Pictogrammen voor bijwerken toevoegen

### Plasma Framework

- ondersteuning voor cantata systeemvak toevoegen (bug 363784)
- Geselecteerde status voor Plasma::Svg en IconItem
- DaysModel: m_agendaNeedsUpdate resetten wanneer plug-in nieuw gebeurtenissen verzend
- Audio- en netwerkpictogram bijwerken om een beter contrast te krijgen (bug 356082)
- Maak downloadPath(const QString &amp;file) verouderd ten faveure van downloadPath()
- [icon thumbnail] Om de voorkeurgrootte van een pictogram verzoeken (bug 362324)
- Plasmoids kunnen nu vertellen of widgets vergrendeld zijn door de gebruiker of door restricties van de systeembeheerder
- [ContainmentInterface] Geen leeg QMenu laten zien
- SAX gebruiken voor vervanging van Plasma::Svg stijlsheet
- [DialogShadows] Toegang tot QX11Info::display() in de cache
- Het pictogramthema air plasma herstellen uit KDE4
- Geselecteerde kleurenschema herladen bij gewijzigde kleuren

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
