---
aliases:
- ../../kde-frameworks-5.21.0
date: 2016-04-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nieuw framework: KActivitiesStats, een bibliotheek voor toegang tot de gegevens van gebruiksstatistieken verzamelt door de KDE activiteitbeheerder.

### Alle frameworks

Qt &gt;= 5.4 is nu vereist, d.w.z. Qt 5.3 wordt niet langer ondersteund.

### Attica

- Const. variant toevoegen voor een betere methode

### Baloo

- Batchgrootte in configuratie centraliseren
- Code die indexering van text/plain bestanden zonder .txt extensie blokkeert verwijderen (bug 358098)
- Controleer zowel bestandsnaam als bestandsinhoud om het MIME-type te bepalen (bug 353512)

### BluezQt

- ObexManager: splits foutbericht voor ontbrekende objecten

### Breeze pictogrammen

- Breeze pictogrammen voor lokalisatie toevoegen
- app-pictogrammen synchroniseren tussen breeze en breeze dark
- Thema-pictogrammen bijwerken en reparatie van pictogrammen voor toepassingensysteem in kickergroepen verwijderen
- xpi ondersteuning voor firefox add-ons toevoegen (bug 359913)
- pictogram van okular bijwerken met de juiste
- ondersteuning voor ktnef-app-pictogram toegevoegd
- Pictogram voor kmenueditor, kmouse en knotes toevoegen
- het te gebruiken pictogram voor gedempt geluidsvolume - Voor gedempt in plaats van alleen de rode kleur (bug 360953)
- ondersteuning voor djvu MIME-type toevoegen (bug 360136)
- koppeling toevoegen in plaats van dubbel item
- ms-sneltoetspictogram voor gnucash toevoegen (bug 360776)
- achtergrondafbeelding wijzigen naar de algemene
- pictogrammen bijwerken om een algemene achtergrond te gebruiken
- pictogram voor konqueror toegevoegd (bug 360304)
- pictogram voor proces-werkt toevoegen voor animatie van voortgang in KDE (bug 360304)
- pictogram voor software installeren en bijwerken toevoegen met de juiste kleur
- embleempictogram voor toevoegen en verwijderen toevoegen voor selecteren in dolphin, pictogram voor aankoppelen toevoegen
- stylesheet uit analogclock en kickerdash applet-pictogrammen verwijderen
- breeze en breeze dark synchroniseren (bug 360294)

### Extra CMake-modules

- _ecm_update_iconcache repareren om alleen de installatielocatie bij te werken
- "ECMQtDeclareLoggingCategory: &lt;QDebug&gt; met het gegenereerde bestand invoegen" terugzetten

### Frameworkintegratie

- Terugvallen naar QCommonStyle implementatie van standardIcon
- Stel een standaard timeout in voor sluiten van menu

### KActivities

- Controle op compiler verwijderen nu alle frameworks c++11 vereisen
- QML ResourceModel verwijderd omdat het door KAStats::ResultModel er bovenuit gaat
- Invoegen in een lege QFlatSet gaf een ongeldige iterator

### KCodecs

- Code vereenvoudigen (qCount -&gt; std::count, homegrown isprint -&gt; QChar::isPrint)
- detectie van codering: crash repareren bij verkeerd gebruik van isprint (bug 357341)
- Crash vanwege niet geïnitialiseerde waarde repareren (bug 357341)

### KCompletion

- KCompletionBox: venster zonder frame afdwingen en geen focus instellen
- KCompletionBox zou *geen* tekstballon mogen zijn

### KConfig

- Ondersteuning voor QStandardPaths locaties verkrijgen in desktop-bestanden toevoegen

### KCoreAddons

- kcoreaddons_desktop_to_json() op Windows repareren
- src/lib/CMakeLists.txt - linking naar een Threads-bibliotheek repareren
- Stubs toevoegen om compilatie op Android toe te staan

### KDBusAddons

- Introspectie van een DBus interface vermijden wanneer we het niet gebruiken

### KDeclarative

- uniform gebruik van std::numeric_limits
- [DeclarativeDragArea] "text" van MIME-gegevens niet overschrijven

### Ondersteuning van KDELibs 4

- Verouderde koppeling in docbook van kdebugdialog5 repareren
- Qt5::Network niet als vereiste lib lekken voor de rest van de ConfigureChecks

### KDESU

- Feature macro's instellen om bouwen op musl libc in te schakelen

### KEmoticons

- KEmoticons: crash wanneer loadProvider om de een of andere reden crasht repareren

### KGlobalAccel

- Zorg ervoor dat kglobalaccel5 op de juiste manier is af te breken, waarmee zeer langzaam afsluiten wordt gerepareerd

### KI18n

- Qt systeem-locale talen gebruiken als terugval op niet UNIX

### KInit

- De xcb overzetting van klauncher opschonen en herprogrammeren

### KIO

- FavIconsCache: synchroniseer na schrijven, zodat andere toepassingen het zien en om te vermijden op vernietiging te crashen
- Veel problemen met threading in KUrlCompletion repareren
- Crash in dialoog voor hernoemen repareren (bug 360488)
- KOpenWithDialog: venstertitel en tekst van beschrijving verbeteren (bug 359233)
- Zorg voor betere cross-platform gebruik van io-slaves door protocolinformatie te bundelen in metagegevens van plug-in

### KItemModels

- KSelectionProxyModel: behandeling van verwijdering van een rij vereenvoudigen, vereenvoudiging van selectielogica ongedaan maken
- KSelectionProxyModel: mapping opnieuw maken bij verwijderen alleen indien nodig (bug 352369)
- KSelectionProxyModel: wis firstChild mappings alleen voor topniveau
- KSelectionProxyModel: verzeker juiste signalering bij verwijderen van de laatst geselecteerde
- DynamicTreeModel doorzoekbaar maken op rol bij tonen

### KNewStuff

- Niet crashen als .desktop bestanden ontbreken of gebroken zijn

### KNotification

- Behandel links klikken op oude pictogrammen in het systeemvak (bug 358589)
- X11BypassWindowManagerHint-vlag allen op platform X11 gebruiken

### Pakket Framework

- Na installeren van een pakket, laad het
- Als het pakket bestaat en is bijgewerkt geef dan geen mislukt aan
- Pakket toevoegen::cryptographicHash(QCryptographicHash::Algorithm)

### KPeople

- Stel de contact-uri in als persoon-uri in PersonData wanneer geen persoon bestaat
- Stel een naam in voor de databaseverbinding

### KRunner

- Sjabloon voor runner importeren uit KAppTemplate

### KService

- Nieuwe waarschuwing van kbuildsycoca repareren, wanneer een MIME-type erft uit een alias
- Behandeling van x-scheme-handler/* in mimeapps.list repareren
- Behandeling van x-scheme-handler/* bij ontleden in mimeapps.list repareren (bug 358159)

### KTextEditor

- Draai "Open/opslaan configuratiepagina: gebruik de term "Folder" in plaats van "Directory"" om
- dwing UTF-8 af
- Open/opslaan configuratiepagina: gebruik de term "Folder" in plaats van "Directory"
- kateschemaconfig.cpp: juiste filters gebruiken met open/opslaan dialogen (bug 343327)
- c.xml: standaard stijl gebruiken voor sleutelwoorden voor flow-control
- isocpp.xml: standaard stijl "dsControlFlow" gebruiken voor sleutelwoorden voor flow-control
- c/isocpp: meer C standaard typen toevoegen
- KateRenderer::lineHeight() geef een int terug
- afdrukken: lettergrootte gebruiken uit geselecteerd afdrukschema (bug 356110)
- cmake.xml versnelling: WordDetect gebruiken in plaats van RegExpr
- Tabbreedte naar 4 in plaats van 8 wijzigen
- Bijwerken van de kleur van het huidige regelnummer repareren
- Selectie van item voor aanvullen met de muis repareren (bug 307052)
- Syntaxisaccentuering voor gcode toevoegen
- De MiniMap selectie van achtergrondschildering repareren
- Codering voor gap.xml repareren (UTF-8 gebruiken)
- Geneste commentaarblokken repareren (bug 350612)

### KWidgetsAddons

- Marges van inhoud meenemen bij berekening van hints voor grootte

### KXMLGUI

- Bij bewerken van werkbalken gingen geplugde acties verloren repareren

### NetworkManagerQt

- ConnectionSettings: initialiseer gateway ping timeout
- Nieuwe TunSetting en Tun verbindingstype
- Apparaten voor alle bekende typen maken

### Oxygen-pictogrammen

- index.theme installeren in dezelfde map waarin het altijd was
- In oxygen/base/ installeren zodat pictogrammen verplaatst uit apps niet in conflict komen met de versie geïnstalleerd door die apps
- Symbolische koppelingen repliceren uit breeze-pictogrammen
- Nieuw pictogrammen voor embleem-toegevoegd en embleem-verwijderd toevoegen voor synchronisatie met breeze

### Plasma Framework

- [calendar] Agenda-applet die selectie niet wist bij verbergen repareren (bug 360683)
- audiopictogram bijwerken om stijlsheet te gebruiken
- pictogram voor geluid dempen bijwerken (bug 360953)
- Het maken-afdwingen van applets wanneer plasma niet is te wijzigen repareren
- [Fading Node] dekking niet separaat mixen (bug 355894)
- [Svg] De configuratie niet opnieuw ontleden in antwoord op Theme::applicationPaletteChanged
- Dialog: Stel SkipTaskbar/Pager statussen in voor het tonen van het venster (bug 332024)
- Introduceer opnieuw de eigenschap bezet in een Applet
- Nagaan dat exportbestand van PlasmaQuick juist wordt gevonden
- Een niet bestaande layout niet importeren
- Het mogelijk maken voor een applet om een testobject aan te bieden
- QMenu::exec vervangen door QMenu::popup
- FrameSvg: loshangende pointers repareren in sharedFrames bij wijziging van thema
- IconItem: plan bijwerken van pixmap bij wijzigingen in venster
- IconItem: actieve en ingeschakelde wijziging animeren zelfs met animaties uitgeschakeld
- DaysModel: Maak bijwerken een slot
- [Icon Item] Niet uit de vorige pixmap animeren wanneer het onzichtbaar was
- [Icon Item] Roep loadPixmap in setColorGroup niet aan
- [Applet] Overschrijf niet de "Persistent" vlag van melding Ongedaan maken
- Overschrijven toestaan van instelling voor mogelijkheid plasma te wijzigen bij aanmaken van container
- Toevoegen van icon/titleChanged
- Afhankelijkheid van QtScript verwijderen
- Header van plasmaquick_export.h is in plasmaquick map
- Enige plasmaquick-headers installeren

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
