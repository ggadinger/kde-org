---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: KDE stelt KDE Applicaties 17.12.0 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.12.0 beschikbaar
version: 17.12.0
---
14 december 2017. KDE Applications 17.12.0 is uitgebracht.

We werken voortdurend aan verbetering van de software in onze KDE Application series en we hopen dat u alle nieuwe verbeteringen en reparaties van bugs nuttig vindt!

### Wat is er nieuw in KDE Applicaties 17.12

#### Systeem

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, onze bestandsbeheerder, kan nu zoekopdrachten opslaan en het zoeken beperken tot alleen mappen. Hernoemen van bestanden is nu gemakkelijker; eenvoudig dubbelklikken op de bestandsnaam. Meer bestandsinformatie is nu voorhanden, omdat de wijzigingsdatum en oorspronkelijke URL van gedownloadde bestanden nu getoond worden in het informatiepaneel. Bovendien zijn nieuwe kolommen voor genre, bitsnelheid en uitgavejaar geïntroduceerd.

#### Grafische zaken

Onze krachtige documentviewer <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> heeft ondersteuning gekregen voor HiDPI schermen en de Markdown-taal, en het renderen van documenten die langzaam zijn te laden worden nu progressief getoond. Er is nu een optie beschikbaar om een document te delen via e-mail.

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a> de afbeeldingsviewer kan nu afbeeldingen openen en accentueren in de bestandsbeheerder, zoomen is soepeler, navigatie met toetsenbord is verbeterd en het ondersteunt de FITS en Truevision TGA formaten. Afbeeldingen worden nu beschermd tegen per ongeluk verwijderen door de toets Delete wanneer ze niet zijn geselecteerd.

#### Multimedia

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> gebruikt nu minder geheugen bij behandeling van videoprojecten die veel afbeeldingen bevatten, standaard proxy-profielen zijn verfijnd en een vervelende bug gerelateerd aan één second vooruit bij achterwaarts afspelen is gerepareerd.

#### Hulpmiddelen

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

De ondersteuning van zip in <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> in de backend libzip is verbeterd. <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> heeft een nieuwe <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>Voorbeeldplug-in</a> die u in staat stelt om een live-voorbeeld van het tekstdocument in het uiteindelijke formaat te zien, toepassen van elke beschikbare KParts-plug-in (bijv. voor Markdown, SVG, Dot-graph, Qt UI of patches). Deze plug-in werkt ook in <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop.</a>

#### Ontwikkeling

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

<a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> biedt nu ook een contextmenu in het diff-gebied, voor snellere toegang tot acties voor navigatie of wijzigingen. Als u een ontwikkelaar bent, zou u de nieuwe in-pane-voorbeeld van KUIViewers van een UI-object beschreven door Qt UI bestanden (widgets, dialogen, etc) nuttig kunnen vinden. Het ondersteunt nu ook de streaming API van KParts.

#### Kantoor

Het team van <a href='https://www.kde.org/applications/office/kontact'>Kontact</a> heeft hard gewerkt aan verbeteringen en verfijningen. Veel van het werk was het moderniseren van de code, maar gebruikers zullen merken dat tonen van versleutelde berichten is verbeterd en ondersteuning is toegevoegd voor tekst/pgp en <a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a>. Er is nu een optie om een IMAP-map tijdens configuratie voor vakantie, een nieuwe waarschuwing in KMail wanneer een e-mail opnieuw geopend wordt en identiteit/mailtransport niet hetzelfde is, nieuwe <a href='https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/'>ondersteuning voor Microsoft® Exchange™</a>, ondersteuning voor Nylas Mail en verbeterde import van Geary in de akonadi-import-assistent, samen met verschillende andere reparaties van bugs en algemene verbeteringen.

#### Spellen

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

<a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> kan nu een breder publiek bereiken, omdat het <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>overgebracht is naar Android</a>. <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a> en <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> hebben het overzetten van KDE games naar Frameworks 5 volledig gemaakt.

### Meer overgezet naar KDE Framework 5

Nog meer toepassingen die gebaseerd waren op kdelibs4 zijn nu overgezet naar KDE Frameworks 5. Dit omvat de muziekspeler <a href='https://www.kde.org/applications/multimedia/juk/'>JuK</a>, de downloadbeheerder <a href='https://www.kde.org/applications/internet/kget/'>KGet</a>, <a href='https://www.kde.org/applications/multimedia/kmix/'>KMix</a>, hulpmiddelen zoals <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> en <a href='https://www.kde.org/applications/utilities/kmouth/'>KMouth</a>, en <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> en Zeroconf-ioslave. Veel dank aan de hardwerkende ontwikkelaars die vrijwillig hun tijd beschikbaar stelden en het hebben laten werken!

### Toepassingen gaan naar hun eigen uitgaveplanning

<a href='https://www.kde.org/applications/education/kstars/'>KStars</a> heeft nu zijn eigen uitgaveplanning; bekijk deze <a href='https://knro.blogspot.de'>ontwikkelaarsblog</a> voor aankondigingen. Het is waard op te merken dat verschillende toepassingen zoals Kopete en Blogilo <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>niet langer geleverd worden</a> met de series toepassingen, omdat ze nog niet zijn overgezet naar KDE Frameworks 5 of op dit moment niet actief worden onderhouden.

### Op bugs jagen

Meer dan 110 bugs zijn opgelost in toepassingen inclusief de Kontact Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello en meer!

### Volledige log met wijzigingen

Als u meer wilt lezen over de wijzigingen in deze uitgave, <a href='/announcements/changelogs/applications/17.12.0'>ga dan naar de volledige log met wijzigingen</a>. Hoewel een beetje intimiderend vanwege zijn uitgebreidheid, kan de log met wijzigingen een excellente manier zijn om te weten te komen over de interne werking van KDE en toepassingen en functies te ontdekken waarover u nooit eerder weet had.
