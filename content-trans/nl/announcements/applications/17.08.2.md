---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE stelt KDE Applicaties 17.08.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.08.2 beschikbaar
version: 17.08.2
---
12 oktober 2017. Vandaag heeft KDE de tweede update voor stabiliteit vrijgegeven voor <a href='../17.08.0'>KDE Applicaties 17.08</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 25 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Dolphin, Gwenview, Kdenlive, Marble, Okular, naast andere.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.37.

Verbeteringen bevatten:

- Een geheugenlek en crash in plug-in voor configuratie van gebeurtenissen voor Plasma is gerepareerd
- Gelezen meldingen worden niet langer onmiddellijk verwijderd uit het ongelezenfilter in Akregator
- Gwenview-importprogramma gebruikt nu de EXIF datum/tijd
