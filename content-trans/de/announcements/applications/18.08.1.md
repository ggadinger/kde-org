---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE veröffentlicht die KDE-Anwendungen 18.08.1
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 18.08.1
version: 18.08.1
---
06. September 2018. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../18.08.0'>KDE-Anwendungen 18.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als ein Dutzend aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Cantor, Gwenview, Okular und Umbrello.

Verbesserungen umfassen:

- The KIO-MTP component no longer crashes when the device is already accessed by a different application
- Sending mails in KMail now uses the password when specified via password prompt
- Okular now remembers the sidebar mode after saving PDF documents
