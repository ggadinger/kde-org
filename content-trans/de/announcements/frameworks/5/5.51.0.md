---
aliases:
- ../../kde-frameworks-5.51.0
date: 2018-10-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Add calls to KIO::UDSEntry::reserve in timeline/tags ioslaves
- [balooctl] Flush buffered "Indexing &lt;file&gt;" line when indexing starts
- [FileContentIndexer] Connect finished signal from extractor process
- [PositionCodec] Avoid crash in case of corrupt data (bug 367480)
- Fix invalid char constant
- [Balooctl] remove directory parent check (bug 396535)
- Allow removing non-existent folders from include and exclude lists (bug 375370)
- Use String to store UDS_USER and UDS_GROUP of String type (bug 398867)
- [tags_kio] Fix parenthesis. Somehow this got by my code checker
- Exclude genome files from indexing

### BluezQt

- Implement Media and MediaEndpoint API

### Breeze Icons

- Fix "stack-use-after-scope" detected by ASAN in CI
- Fix monochrome icons missing stylesheets
- Change drive-harddisk to more adaptable style
- Add firewall-config and firewall-applet icons
- Make lock on plasmavault icon visible with breeze-dark
- Add plus symbol to document-new.svg (bug 398850)
- Provide icons for 2x scaling

### Extra CMake-Module

- Compile python bindings with the same sip flags used by PyQt
- Android: Allow passing a relative path as the apk dir
- Android: Properly offer a fallback to applications that don't have a manifest
- Android: Make sure Qm translations get loaded
- Fix Android builds using cmake 3.12.1
- l10n: Fix matching digits in the repository name
- Add QT_NO_NARROWING_CONVERSIONS_IN_CONNECT as default compile flags
- Bindings: Correct handling of sources containing utf-8
- Actually iterate over CF_GENERATED, rather than checking item 0 all the time

### KActivities

- Fix dangling reference with "auto" becoming "QStringBuilder"

### KCMUtils

- manage return events
- Manually resize KCMUtilDialog to sizeHint() (bug 389585)

### KConfig

- Fix issue when reading path lists
- kcfg_compiler now documents valid inputs for its 'Color' type

### KFileMetaData

- remove own implementation of QString to TString conversion for taglibwriter
- increase test coverage of taglibwriter
- implement more basic tags for taglibwriter
- remove usage of own TString to QString conversion function
- bump required taglib version to 1.11.1
- implement reading of the replaygain tags

### KHolidays

- add Ivory Coast holidays (French) (bug 398161)
- holiday*hk** - fix date for Tuen Ng Festival in 2019 (bug 398670)

### KI18n

- Properly scope CMAKE_REQUIRED_LIBRARIES change
- Android: Make sure we're looking for .mo files in the right path

### KIconThemes

- Start drawing emblems in the bottom-right corner

### KImageFormats

- kimg_rgb: optimize away QRegExp and QString::fromLocal8Bit
- [EPS] Fix crash at app shutdown (being tried to persist clipboard image) (bug 397040)

### KInit

- Lessen log spam by not checking for existence of file with empty name (bug 388611)

### KIO

- allow non-local file:// redirect to a Windows WebDav URL
- [KFilePlacesView] Change icon for the 'Edit' context menu entry in Places panel
- [Places panel] use more appropriate network icon
- [KPropertiesDialog] Show mount information for folders in / (root)
- Fix deletion of files from DAV (bug 355441)
- Avoid QByteArray::remove in AccessManagerReply::readData (bug 375765)
- Don't try to restore invalid user places
- Make it possible to change directory up even with trailing slashes in the url
- KIO slave crashes are now handled by KCrash instead of subpar custom code
- Fixed a file being created from pasted clipboard contents showing up only after a delay
-  
- Improve "insufficient disk space" error message
- IKWS: use non-deprecated "X-KDE-ServiceTypes" in desktop file generation
- Fix WebDAV destination header on COPY and MOVE operations
- Warn user before copy/move operation if available space is not enough (bug 243160)
- Move SMB KCM to Network Settings category
- trash: Fix directorysizes cache parsing
- kioexecd: watch for creations or modifications of the temporary files (bug 397742)
- Don't draw frames and shadows around images with transparency (bug 258514)
- Fixed file type icon in file properties dialog rendered blurry on high dpi screens

### Kirigami

- properly open the drawer when dragged by handle
- extra margin when the pagerow globaltoolbar is ToolBar
- support also Layout.preferredWidth for sheet size
- get rid of last controls1 remains
- Allow creation of separator Actions
- consent an arbitrary # of columns in CardsGridview
- Don't actively destroy menu items (bug 397863)
- icons in actionButton are monochrome
- don't make icons monochrome when they shouldn't
- restore the arbitrary *1.5 sizing of icons on mobile
- delegate recycler: Do not request the context object twice
- use the internal material ripple implementation
- control header width by sourcesize if horizontal
- expose all properties of BannerImage in Cards
- use DesktopIcon even on plasma
- correctly load file:// paths
- Revert "Start looking for the context from the delegate itself"
- Add test case that outlines scoping issue in DelegateRecycler
- explicitly set an height for overlayDrawers (bug 398163)
- Start looking for the context from the delegate itself

### KItemModels

- Use reference in for loop for type with non-trivial copy constructor

### KNewStuff

- Add support for Attica tags support (bug 398412)
- [KMoreTools] give the "Configure..." menu item an appropriate icon (bug 398390)
- [KMoreTools] Reduce menu hierarchy
- Fix 'Impossible to use knsrc file for uploads from non standard location' (bug 397958)
- Make test tools link on Windows
- Unbreak build with Qt 5.9
- Add support for Attica tags support

### KNotification

- Fixed a crash caused by bad lifetime management of canberra-based audio notification (bug 398695)

### KNotifyConfig

- Fix UI file hint: KUrlRequester now has QWidget as base class

### KPackage-Framework

- Use reference in for loop for type with non-trivial copy constructor
- Move Qt5::DBus to the 'PRIVATE' link targets
- Emit signals when a package is installed/uninstalled

### KPeople

- Fix signals not being emitted when merging two persons
- Don't crash if person gets removed
- Define PersonActionsPrivate as class, as declared before
- Make PersonPluginManager API public

### Kross

- core: handle better comments for actions

### KTextEditor

- Paint code folding marker only for multiline code folding regions
- Intialize m_lastPosition
- Scripting: isCode() returns false for dsAlert text (bug 398393)
- use R Script hl for R indent tests
- Update of the R indent script
- Fix Solarized Light and Dark color schemes (bug 382075)
- Don't require Qt5::XmlPatterns

### KTextWidgets

- ktextedit: lazy load the QTextToSpeech object

### KWallet Framework

- Log wallet open failure errors

### KWayland

- Don't silently error if damage is sent before buffer (bug 397834)
- [server] Do not return early on fail in touchDown fall back code
- [server] Fix remote access buffer handling when output not bound
- [server] Do not try to create data offers without source
- [server] Abort drag start on correct conditions and without posting error

### KWidgetsAddons

- [KCollapsibleGroupBox] Respect style's widget animation duration (bug 397103)
- Remove obsolete Qt version check
- Compile

### KWindowSystem

- Use _NET_WM_WINDOW_TYPE_COMBO instead of _NET_WM_WINDOW_TYPE_COMBOBOX

### KXMLGUI

- Fix OCS provider URL in about dialog

### NetworkManagerQt

- Use matching enum value AuthEapMethodUnknown to compare a AuthEapMethod

### Plasma Framework

- Bump theme version strings because there are new icons in 5.51
- Also raise configuration window when reusing it
- Add missing component: RoundButton
- Combine display OSD icon files and move to plasma icon theme (bug 395714)
- [Plasma Components 3 Slider] Fix implicit size of handle
- [Plasma Components 3 ComboBox] Switch entries with mouse wheel
- Support button icons when present
- Fixed week names not showing properly in calendar when week starts with a day other than Monday or Sunday (bug 390330)
- [DialogShadows] Use 0 offset for disabled borders on Wayland

### Prison

- Fix rendering Aztec codes with an aspect ratio != 1
- Remove assumption about the barcode aspect ratio from the QML integration
- Fix rendering glitches caused by rounding errors in Code 128
- Add support for Code 128 barcodes

### Purpose

- Make cmake 3.0 the minimum cmake version

### QQC2StyleBridge

- Small default padding when there is a background

### Solid

- Don't show an emblem for mounted disks, only unmounted disks
- [Fstab] Remove AIX support
- [Fstab] Remove Tru64 (**osf**) support
- [Fstab] Show non-empty share name in case root fs is exported (bug 395562)
- Prefer provided drive label for loop devices as well

### Sonnet

- Fix breakage of language guessing
- Prevent highlighter from erasing selected text (bug 398661)

### Syntax Highlighting

- i18n: fix extraction of theme names
- Fortran: Highlight alerts in comments (bug 349014)
- avoid that the main context can be #poped
- Endless state transition guard
- YAML: add literal &amp; folded block styles (bug 398314)
- Logcat &amp; SELinux: improvements for the new Solarized schemes
- AppArmor: fix crashes in open rules (in KF5.50) and improvements for the new Solarized schemes
- Merge git://anongit.kde.org/syntax-highlighting
- Update git ignore stuff
- Use reference in for loop for type with non-trivial copy constructor
- Fix: Email highlighting for unclosed parenthesis in Subject header (bug 398717)
- Perl: fix brackets, variables, string references and others (bug 391577)
- Bash: fix parameter &amp; brace expansion (bug 387915)
- Add Solarized Light and Dark themes

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
