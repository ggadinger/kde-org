---
aliases:
- ../../kde-frameworks-5.64.0
date: 2019-11-10
layout: framework
libCount: 70
---
### Attica

- Se han añadido algunos «std:move» en funciones «setter».

### Baloo

- Permitir que se pueda compilar con Qt 5.15.
- Usar «propertymap» para guardar propiedades en «Baloo::Result».
- Add standalone conversion functions for PropertyMap to Json and vice versa
- [Base de datos] Se ha trabajado en el manejo de indicadores del entorno.
- Se ha sustituido la recursividad de «FilteredDirIterator» con una iteración de bucle.

### Iconos Brisa

- Center-align the non-square 64px audio mimetype icons (bug 393550)
- Se han eliminado los remanentes del icono «nepomuk».
- Move colorful 32px help-about icon into actions (bug 396626)
- Se han mejorado los iconos de dibujo (fallo 399665).
- Se ha borrado el icono de nepomuk
- Rellenar el área del botón central del ratón.
- Add folder-recent, extend hand of clock in folder-temp
- Use a more correct and appropriate visual metaphor for "get hot new stuff" icon (bug 400500)
- Se ha actualizado el icono «elisa».
- Usar CSS en lugar de SCSS como formato de salida.
- Se ha corregido la visualización incorrecta del icono «edit-opacity» de 22 píxeles.
- Se han añadido iconos de «edit-opacity» (error 408283).
- Iconos para vientos meteorológicos (error 412718).
- Se han corregido los márgenes de los iconos multimedia de 16 y 22 píxeles.
- Use the text rather than highlight color for rating/star emblem
- Se han añadido los iconos «draw-arrow» (fallo 408283).
- Se han añadido los iconos de las acciones «draw-highlight» (fallo 408283).
- Se ha añadido «PATH/LD_LIBRARY_PATH» a la invocación de «qrcAlias».
- Add applications-network icon for renaming Internet category to Network
- Se han añadido iconos de «edit-line-width» (error 408283).

### Módulos CMake adicionales

- No definir los estándares de C/C++ si ya se han definido.
- Usar el modo moderno para definir el estándar de C/CXX.
- Elevar los requisitos de CMake a 3.5.
- ECMAddQch: Permitir el uso de «PREDEFINED_MACROS/BLANK_MACROS» con espacios en blanco y comillas.

### Integración con Frameworks

- Add standard icons to support to all entries in QDialogButtonBox (bug 398973)
- Se ha asegurado que winId() no se llame en los widgets no nativos (error 412675).

### KActivitiesStats

- Pruebas: Se ha corregido un fallo de compilación en macOS.
- Corregir compilación en MSVC de Windows.
- Add a utility accessor to get a QUrl from a ResultSet::Result

### KArchive

- Se ha corregido una fuga de memoria en KXzFilter::init.
- Se ha corregido una referencia a puntero nulo cuando falla la extracción.
- decodeBCJ2: se ha corregido una aserción con archivos dañados.
- KXzFilter::Private: Se han eliminado propiedades no usadas.
- K7Zip: Se ha corregido el uso de memoria en «readAndDecodePackedStreams».

### KCalendarCore

- Añadir también la versión de «libical».
- Definir explícitamente el constructor de copia de «Journal».

### KCMUtils

- Conditionally show navigation buttons in the header for multi-page KCMs
- don't use a custom header height (bug 404396)
- Se ha añadido un «include» adicional.
- Se ha corregido una fuga de memoria en los objetos «KQuickAddons::ConfigModule» (error 412998).
- [KCModuleLoader] Mostrar un error cuando falla la carga de QML.

### KConfig

- kconfig_compiler: Mover el «KSharedConfig::Ptr» cuando se usa.
- Posibilitar la compilación con qt5.15 sin un método obsoleto.
- Exponer «isImmutable» para introspección (por ejemplo, QML).
- Add convenience for defaults/dirty states to KCoreConfigSkeleton
- Make kconfig_compiler generate ctors with the optional parent arg
- Hacer que la función «preferences()» sea pública.

### KConfigWidgets

- Evitar la sobrecarga de «KCModule::changed».

### KContacts

- Instalación de traducciones

### KCoreAddons

- KProcessInfoList: añadir el motor «proclist» para FreeBSD.

### KDeclarative

- Use compile time checked connect
- Hacer que el slot «settingChanged()» sea protegido.
- Get KQuickAddons::ConfigModule to expose if we're in the defaults state
- Grab the keyboard when KeySequenceItem is recording
- Añadir «ManagedConfigModule».
- Remove outdated comment about [$e] expansion
- [ConfigModule] Expose mainUi component status and error string

### Soporte de KDELibs 4

- KLocale api docs: make it easier to find how to port code away from it

### KDocTools

- man: usar &lt;arg&gt; en lugar de &lt;group&gt;.

### KFileMetaData

- Se ha corregido un fallo de la aplicación al escribir y borrar la colección.

### KHTML

- Extend KHtmlView::print() to use a predefined QPrinter instance (bug 405011)

### KI18n

- Añadir «KLocalizedString::untranslatedText».
- Replace all qWarning and related calls with categorised logging

### KIconThemes

- Fix usage of the new deprecation macros for assignIconsToContextMenu
- Marcar como obsoleto «KIconTheme::assignIconsToContextMenu».

### KIO

- Const &amp; signature of new introduced SlaveBase::configValue
- Port to the QSslError variant of KSslInfoDialog
- Port KSSLD internals from KSslError to QSslError
- Make non-ignorable SSL errors explicit
- auto-enable KIO_ASSERT_SLAVE_STATES also for from-git builds
- Port (most of) KSslInfoDialog from KSslError to QSslError
- kio_http: avoid double Content-Type and Depth when used by KDAV
- Port the KSSLD D-Bus interface from KSslError to QSslError
- Replace usage of SlaveBase::config()-&gt;readEntry by SlaveBase::configValue
- Remove two unused member variables using KSslError
- Avoid sending KDirNotify::emitFilesAdded when the emptytrashjob finishes
- Deprecate the KTcpSocket-based variant of SslUi::askIgnoreSslErrors
- Treat "application/x-ms-dos-executable" as executable on all platforms (bug 412694)
- Sustituir el uso de «SlaveBase::config()» por «SlaveBase::mapConfig()».
- ftptest: replace logger-colors with logger
- [SlaveBase] Use QMap instead of KConfig to store ioslave config
- Adaptar «KSslErrorUiData» a «QSslError».
- exclude ioslaves directory from api docs
- ftptest: mark overwrite without overwrite flag no longer failing
- ftptest: refactor the daemon startup into its own helper function
- [SslUi] Add api docs for askIgnoreSslErrors()
- consider ftpd not required for the time being
- port ftp slave to new error reporting system
- fix proxy setting loading
- Implement KSslCertificateRule with QSslError instead of KSslError
- Port (most of) the interface of KSslCertificateRule to QSslError
- Adaptar «KSslCertificateManager» a «QSslError».
- Update test kfileplacesviewtest following D7446

### Kirigami

- Asegurarse de que el «topContent» del «GlobalDrawer» siempre permanece encima (error 389533).
- highlight on mouseover only when mode than one page (bug 410673)
- Cambiar el nombre de «Okular Active» a «Okular Mobile».
- items have active focus on tab when they aren't in a view (bug 407524)
- Allow contextualActions to flow into the header toolbar
- Fix incorrect Credits model for Kirigami.AboutPage
- No mostrar el cajón de contexto si todas las acciones son invisibles.
- Fix Kirigami template image
- keep containers devoid of deleted items
- Limitar el tamaño de los márgenes de arrastre.
- Se ha corregido la visualización del botón de herramienta del menú cuando no hay ningún cajón disponible.
- Se ha desactivado el arrastre del cajón global en el modo de menú.
- Show menu items tooltip text
- No mostrar advertencia sobre «LayoutDirection» en «SearchField».
- Properly check enabled state of Action for ActionToolBar buttons
- Use MenuItem's action property directly in ActionMenuItem
- Permitir que el cajón global se pueda convertir en un menú, si así se quiere.
- Be more explicit about action property types

### KItemViews

- [RFC] Se ha unificado el estilo de los nuevos «Kirigami.ListSectionHeader» y «CategoryDrawer».

### KJS

- Better message for String.prototype.repeat(count) range errors
- Simplificar el análisis de literales numéricos.
- Analizar literales binarios JS.
- Detectar literales hexadecimales y octales truncados.
- Permitir el uso del nuevo estándar para especificar literales octales.
- Collection of regression tests taken from khtmltests repository

### KNewStuff

- Ensure that the changedEntries property is correctly propagated
- Fix KNSCore::Cache fetching when initialising Engine (bug 408716)

### KNotification

- [KStatusNotifierItem] Allow left click when menu is null (bug 365105)
- Se ha eliminado el soporte de «Growl».
- Añadir y activar el uso del centro de notificaciones en macOS.

### KPeople

- Unbreak build: limit DISABLE_DEPRECATED for KService to &lt; 5.0

### KService

- Posibilitar la compilación con qt5.15 sin un método obsoleto.

### KTextEditor

- KateModeMenuList: mejorar el ajuste de palabras.
- Se ha corregido un cuelgue (error 413474).
- more ok's arrived, more v2+
- more ok's arrived, more v2+
- Se ha añadido un consejo a la cabecera de copyright.
- no non-trivial code lines remain here beside for people that agreed to v2+ =&gt; v2+
- no non-trivial code lines remain here of authors no longer responding, v2+
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- «katedocument.h» ya es v2+.
- ok'd by loh.tar, sars, lgplv2+
- Se ha cambiado la licencia a LGPL v2+, tras la aprobación de Sven y de Michal.
- Se ha cambiado la licencia a LGPL v2+, tras la aprobación de Sven y de Michal.
- all files with SPDX-License-Identifier are lgplv2+
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- Se ha actualizado la licencia, «dh» está en la sección LGPL v2+ de «relicensecheck.pl».
- Se ha actualizado la licencia, «dh» está en la sección LGPL v2+ de «relicensecheck.pl».
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- lgplv2.1+ =&gt; lgplv2+
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- this header contains no gpl v2 only logic since long
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, 'michalhumpula' =&gt; ['gplv23', 'lgplv23', 'gplv2+', 'lgplv2+', '+eV' ]
- Se ha añadido una «s» que faltaba (error 413158).
- KateModeMenuList: force the vertical position above the button
- better: self-contained headers
- group includes for semantics
- Se han ordenados los archivos de cabecera.

### KTextWidgets

- Se ha eliminado una llamada que ya no es necesaria a «KIconTheme::assignIconsToContextMenu».

### KWayland

- FakeInput: add support for keyboard key press and release
- Fix non-integer scale copy on creation of OutputChangeSet

### KXMLGUI

- fix default shortcut detection

### NetworkManagerQt

- Se ha añadido la implementación de la autenticación SAE usada por WPA3.

### Framework de Plasma

- map disabledTextColor to ColorScope
- add DisabledTextColor to Theme
- [PC3/button] Elide text always
- Improve panel options menu entries
- [icons/media.svg] Add 16 &amp; 32px icons, update style
- [PlasmaComponents3] Se ha corregido el fondo de los botones de herramientas que se pueden marcar.

### Prison

- Se ha corregido la gestión de memoria en «datamatrix».

### Purpose

- i18n: se ha añadido una elipsis en los elementos de acción (X-Purpose-ActionDisplay).

### QQC2StyleBridge

- Do not assign combobox currentIndex as it breaks binding
- Listen to the application style changing

### Solid

- Don't build static library when BUILD_TESTING=OFF

### Resaltado de sintaxis

- VHDL: all keywords are insensitive (bug 413409)
- Add string escape characters to PowerShell syntax
- Modelines: se ha corregido el fin de comentario.
- Meson: more built-in functions and add built-in member functions
- debchangelog: se ha añadido Focal Fossa.
- Actualizaciones de CMake 3.16.
- Meson: Add a comment section for comment/uncomment with Kate
- TypeScript: update grammar and fixes

### ThreadWeaver

- Permitir que se pueda compilar con Qt 5.15.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
