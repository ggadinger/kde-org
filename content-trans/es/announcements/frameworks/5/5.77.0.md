---
aliases:
- ../../kde-frameworks-5.77.0
date: 2020-12-12
layout: framework
libCount: 83
qtversion: 5.13
---
### Attica

* Se ha corregido un fallo en la carga del proveedor comprobando el puntero de respuesta antes de derreferenciarlo (error 427974).

### Baloo

* [DocumentUrlDB] Borrar entrada de la lista hija de la base de datos si está vacía.
* Se ha añadido el tipo de documento de presentación para las presentaciones y plantillas de «Office OpenXML».
* [MetaDataMover] Se ha corregido la búsqueda del identificador del documento padre.
* [DocumentUrlDB] Añadir un método para cambios de nombre y movimientos triviales.
* [MetaDataMover] Hacer que los cambios de nombre solo sean una operación de bases de datos.
* [Documento] Añadir el ID del documento padre y poblarlo.
* Sustituir la llamada directa a «syslog» con un mensaje de registro categorizado.

### Iconos Brisa

* Añadir las variantes de «text-field»: «-frameless» (-> text-field), «-framed».
* Se han añadido enlaces simbólicos para los iconos «input-*».
* Se ha añadido un icono para tipo de letra TrueType en XML.
* Se ha añadido «add-subtitle».
* Se ha cambiado el icono de MathML para que use una fórmula y el tipo MIME específico.
* Se ha añadido un icono para la imágenes de disco de QEMU y de SquashFS.
* Se ha añadido el icono para la acción «edit-move».
* Se ha añadido el icono para volcados del núcleo.
* Se han añadido varios tipos MIME para subtítulos.
* Se ha eliminado el aspecto borroso innecesario en el icono de «kontrast».

### Módulos CMake adicionales

* Se ha corregido la extracción de la categoría de los archivos de escritorio.
* Definir la variable del directorio de instalación para las plantillas de archivos.
* Se ha añadido la generación de metadatos «fastlane» para las compilaciones de Android.
* (Qt)WaylandScanner: Marcar correctamente los archivos como «SKIP_AUTOMOC».

### KActivitiesStats

* ResultModel: Exponer el recurso «MimeType».
* Se ha añadido filtrado de eventos para los archivos y los directorios (error 428085).

### KCalendarCore

* Se ha corregido el nombre del encargado, que se supone que es Allen, no yo :)
* Se ha añadido el uso de la propiedad «CONFERENCE».
* Se ha añadido el método de conveniencia «alarmsTo» al calendario.
* Comprobar que las repeticiones por día no preceden a «dtStart».

### KCMUtils

* Eliminar el truco que ha hecho que dejasen de funcionar los KCM multinivel en el modo de iconos.

### KConfig

* Se ha corregido «KConfigGroup::copyTo» con «KConfigBase::Notify» (error 428771).

### KCoreAddons

* Evitar un fallo cuando la factoría está vacía (al devolver un error).
* KFormat: Añadir más casos de horas y fechas relativas.
* Activar que «KPluginFactory» pase opcionalmente «KPluginMetaData» a los complementos.

### KDAV

* Se ha eliminado debido a la cantidad de errores que producía.

### KDeclarative

* Sincronizar los márgenes de «AbstractKCM» en «SimpleKCM».
* Se ha eliminado el texto de licencia obsoleto.
* Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.
* Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.
* Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.
* Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.
* Se ha reescrito «KeySequenceItem» (y auxiliares) para que usen «KeySequenceRecorder» (error 427730).

### KDESU

* Analizar correctamente las comillas dobles en secuencias de escape.
* Permitir el uso de «doas(1)» de «OpenBSD».

### KFileMetaData

* Se han corregido varias fugas de memoria en los extractores de «OpenDocument» y de «Office OpenXML».
* Añadir varios subtipos para los documentos «OpenDocument» y «OpenXML».

### KGlobalAccel

* Cargar los complementos de la interfaz «kglobalacceld» enlazados de forma estática.

### Complementos KDE GUI

* Hacer que la inhibición de accesos rápidos funcione desde el principio (error 407395).
* Se ha corregido un fallo potencial en la destrucción del inhibidor de Wayland (error 429267).
* CMake: Encontrar «Qt5::GuiPrivate» cuando está activado el soporte de Wayland.
* Añadir «KeySequenceRecorder» como base para «KKeySequenceWidget» y «KeySequenceItem» (error 407395).

### KHolidays

* Se ha corregido el redondeo de los eventos de la posición del Sol menores de 30 segundos antes de la siguiente hora.
* Evitar que se analicen los archivos de festividades dos veces en «defaultRegionCode()».
* Calcular las estaciones astronómicas solo una vez por ocurrencia.
* Se ha corregido la búsqueda de la región de festividades para los códigos ISO 3166-2.
* Hacer que «HolidayRegion» se puede copiar y mover.
* Se ha añadido soporte para calcular las horas de crepúsculo civiles.

### KIdleTime

* Cargar los complementos enlazados estáticamente del «poller» del sistema.

### KImageFormats

* No volver a disminuir la profundidad de color a 8 bits en los archivos PSD sin comprimir de 16 bits.

### KIO

* Diálogo de nuevo archivo: Permitir que se pueda aceptar la creación del directorio antes de haber ejecutado «stat» (error 429838).
* No permitir fugas de memoria en el hilo «DeleteJob».
* KUrlNavBtn: Hacer que funcione la apertura de subdirectorios del menú desplegable con el ratón (error 428226).
* DropMenu: Usar accesos rápidos traducidos.
* [ExecutableFileOpenDialog] Ceder el foco al botón «Cancelar».
* Vista de Lugares: Resaltar el lugar solo cuando se muestra (error 156678).
* Se ha añadido una propiedad para mostrar acciones de complementos en el submenú «Acciones».
* Eliminar un método recientemente introducido.
* KIO::iconNameForUrl: Resolver los iconos de los archivos remotos basándose en su nombre (error 429530).
* [kfilewidget] Usar el nuevo acceso rápido de teclado estándar para «Crear carpeta».
* Se ha refactorizado la carga del menú de contexto y se ha hecho más escalable.
* RenameDialog: Permitir sobrescritura cuando los archivos son más antiguos (fallo 236884).
* DropJob: Usar el nuevo icono «edit-move» para «Mover aquí».
* Se ha corregido la generación de «moc_predef,h» cuando está activada la «ccache»  mediante «-DCMAKE_CXX_COMPILER=ccache CMAKE_CXX_COMPILER_ARG1=g++».
* Ahora es necesario Qt 5.13, por lo que se eliminan los «ifdef».
* Se ha portado «KComboBox» a «QComboBox».
* Corrección de documentación solicitada por Méven Car @meven.
* Refactorizar algunos bucles usando C++ moderno.
* Limpiar el código muerto.
* Se ha eliminado una comprobación redundante si existe la clave.
* Simplificar el código de «RequiredNumberOfUrls».
* Eliminar algunas líneas en blanco.
* Simplificar el código y hacerlo más consistente.
* ioslaves: Se han corregido los permisos de la raíz de «remote:/».
* KFileItem: «isWritable» usa «KProtocolManager» para archivos remotos.
* Se ha añadido una sobrecarga a las acciones «prepend» del menú «Acciones».
* Usar estilo de código moderno.
* No añadir separadores no necesarios (error 427830).
* Usar un inicializador de lista con llaves en lugar del operador <<.
* MkPathJob: Se ha reescrito el código compilado condicionalmente para mejorar la legibilidad.

### Kirigami

* Hacer que la cabecera de «GlobalDrawer» defina la posición de «ToolBars/TabBars/DialogButtonBoxes».
* Propiedad adjunta «inViewport».
* Se ha corregido el deslizamiento de cabeceras en las pantallas táctiles.
* Se ha refactorizado «AbstractapplicationHeader» con un nuevo concepto de «ScrollIntention».
* En el escritorio, rellenar siempre los anclajes con el padre.
* [controles/BasicListItem]: No anclar elementos no existentes.
* No mostrar el texto del avatar en tamaños pequeños.
* Eliminar «#» y «@» del proceso de extracción de la inicial del avatar.
* Inicializar la propiedad de «sizeGroup».
* Usar la interacción del ratón en «isMobile» para realizar pruebas más fácilmente.
* Parche rápido para «leading/trailing» usando los valores de «trailing» para el margen de separación de «leading».
* [controles/BasicListItem]: Se han añadido las propiedades «leading/trailing».
* Se ha corregido la posición de la hora al cambiar el tamaño del contenido.
* Aplicar el antiguo comportamiento en el modo amplio y no añadir «topMargin» en «FormLayout».
* Corregir el diseño de formularios en pantalla pequeña.
* No se puede usar un «AbstractListItem» en un «SwipeListItem».
* Se ha corregido «No se puede asignar [no-definido] a int» en «OverlaySheet».
* [overlaysheet]: No realizar una transición brusca cuando cambia la altura del contenido.
* [overlaysheet]: Animar los cambios de altura.
* Corregir el posicionamiento de la hoja superpuesta.
* Definir siempre el índice al hacer clic en una página.
* Se ha corregido el arrastre de FAB en el modo RTL.
* Refinar el aspecto del separador de listas (error 428739).
* Se han corregido las asas de los cajones en el modo RTL.
* Se ha corregido la renderización de bordes del tamaño correcto con el software al que recurrir (fallo 427556).
* No situar el elemento de software al que recurrir fuera de los límites del elemento rectangular.
* Usar «fwidth()» para suavizar en el modo de bajo consumo (error 427553).
* Renderizar también un color de fondo en el modo de bajo consumo.
* Activar el renderizado transparente de «Shadowed(Border)Texture» en el modo de bajo consumo.
* No cancelar los componentes de alfa para rectángulos sombreados en el modo de bajo consumo.
* No usar un valor de suavizado más bajo cuando se renderiza la textura de «ShadowedBorderTexture».
* Eliminar los pasos de «recorte» del rectángulo sombreado y de los sombreadores relacionados.
* Usar «icon.name» en lugar de «iconName» en la documentación.
* [Avatar] Hacer que los iconos usen un tamaño de icono parecido al tamaño del texto.
* [Avatar] Hacer que las iniciales usen más espacio y mejore la alineación vertical.
* [Avatar] Exponer las propiedades «sourceSize» y «smooth» para cualquiera que desee animar el tamaño.
* [Avatar] Definir el tamaño de la fuente para evitar que las imágenes se vean borrosas.
* [Avatar] Definir el color del primer plano una sola vez.
* [Avatar] Cambiar el degradado del fondo.
* [Avatar] Cambiar el ancho del borde a 1 píxel para que coincida con otros anchos.
* [Avatar] Hacer que siempre funcionen el relleno, «verticalPadding» y «horizontalPadding».
* [avatar]: Añadir degradado a los colores.

### KItemModels

* KRearrangeColumnsProxyModel: Solo la columna 0 tiene hijos.

### KMediaPlayer

* Instalar el reproductor y los archivos «def» del tipo de servicio del motor por coincidencia de tipo de nombre de archivo.

### KNewStuff

* Corregir la desinstalación cuando la entrada no está en caché.
* Cuando se llama a la comprobación de actualizaciones, se esperan actualizaciones (fallo 418082).
* Reutilizar el diálogo de «QWidgets» (error 429302).
* Rodear el bloque de compatibilidad en «KNEWSTUFFCORE_BUILD_DEPRECATED_SINCE».
* No escribir la caché para estados intermedios.
* Usar «enum» para la descompresión en lugar de valores de cadenas de texto.
* Se han corregido las entradas que desaparecían demasiado pronto de la página de actualizables (fallo 427801).
* Añadir el enumerador «DetailsLoadedEvent» a la nueva señal.
* Se ha reelaborado la API de adopción (fallo 417983).
* Se han corregido un par URL remanentes del antiguo proveedor.
* Eliminar la entrada de la caché antes de insertar un nueva entrada (error 424919).

### KNotification

* No pasar una sugerencia transitoria (fallo 422042).
* Se ha corregido un error de distinción de mayúsculas en la cabecera «AppKit» en macOS.
* No llamar acciones de notificación no válidas.
* Se ha corregido la gestión de memoria para «notifybysnore».

### Framework KPackage

* Dejar de usar «X-KDE-PluginInfo-Depends».

### KParts

* Se ha marcado como obsoleto el método «embed()» por falta de uso.
* Hacer que «KParts» use «KPluginMetaData» en lugar de «KAboutData».

### KQuickCharts

* Se ha revisado el algoritmo de suavizado de líneas.
* Mover la aplicación de la interpolación a paso de pulido.
* Centrar correctamente los delegados de puntos en los gráficos de líneas y cambiar su tamaño cuando cambia la anchura de la línea.
* Añadir una casilla de verificación «suave» al ejemplo de gráficos de líneas.
* Asegurar que los delegados de puntos del gráfico de líneas se limpien adecuadamente.
* Mostrar también el nombre en la ayuda emergente del ejemplo de página de gráfico de líneas.
* Documentar «LineChartAttached» y corregir un error sintáctico en la documentación de «LineChart».
* Se han añadido las propiedades «name» y «shortName» a «LineChartAttached».
* Documentar más a fondo la propiedad «pointDelegate».
* Eliminar el miembro «previousValues» y corregir los gráficos de líneas apilados.
* Usar «pointDelegate» el ejemplo de gráficos de líneas para mostrar valores al situar el cursor encima.
* Se ha añadido el uso de «delegado de punto» a los gráficos de líneas.
* LineChart: Mover el cálculo del punto de «updatePaintNode» al pulido.

### KRunner

* Desaconsejar el uso de los remanentes de paquetes de KDE4.
* Hacer uso del nuevo constructor de complemento «KPluginMetaData» de «KPluginLoader».

### KService

* [kapplicationtrader] Se ha corregido la documentación de la API.
* KSycoca: Volver a crear la base de datos cuando la versión sea menor que la esperada.
* KSycoca: Seguir el rastro de los archivos de recursos de «KMimeAssociation»,

### KTextEditor

* Se ha portado «KComboBox» a «QComboBox».
* Usar «themeForPalette» de «KSyntaxHighlighting».
* Se ha corregido una llamada «i18n» en la que faltaba un argumento (error 429096).
* Mejorar la selección automática del tema.

### KWidgetsAddons

* No emitir la señal «passwordChanged» dos veces.
* Se ha añadido «KMessageDialog», una variante asíncrona de «KMessageBox».
* Restaurar el antiguo modo emergente por omisión de «KActionMenu».
* Portar «KActionMenu» a «QToolButton::ToolButtonPopupMode».

### KWindowSystem

* Cargar los complementos de integración enlazados estáticamente.
* Usar «processId()» en lugar de «pid()».

### KXMLGUI

* Introducir «HideLibraries» y desaconsejar el uso de «HideKdeVersion».
* Se ha reescrito «KKeySequenceWidget» para que use «KeySequenceRecorder» (fallo 407395).

### Framework de Plasma

* [Representación] Eliminar el relleno superior/inferior solo cuando la cabecera o el pie son visibles.
* [PlasmoidHeading] Usar la técnica de «Representation» para «inset/margins».
* Se ha añadido un componente de representación.
* [Tema del escritorio] Se ha cambiado el nombre de «hint-inset-side-margin» a «hint-side-inset».
* [FrameSvg] Se ha cambiado el nombre de «insetMargin» a «inset».
* [PC3] Usar la barra de desplazamiento de PC3 en «ScrollView».
* [Brisa] Notificar la sugerencia de «inset».
* [FrameSvg*] Se ha cambiado el nombre de «shadowMargins» a «inset».
* [FrameSvg] Mantener en caché los márgenes de las sombras y respetar los prefijos.
* Terminar la animación antes de cambiar la longitud del resaltado de la barra de avance (error 428955).
* [campo de texto] Se ha corregido que el botón de borrado se solapara con el texto (fallo 429187).
* Mostrar el menú emergente en la posición global correcta.
* Usar «gzip -n» para impedir horas de construcción empotradas.
* Usar «KPluginMetaData» para listar «containmentActions».
* Adaptar la carga de «packageStructure» de «KPluginTrader».
* Usar «KPluginMetaData» para listar «DataEngines».
* [TabBar] Añadir resaltado al foco del teclado.
* [FrameSvg*] Exponer márgenes de sombras.
* Hacer más claro el valor de «MarginAreasSeparator».
* [TabButton] Centrar el icono y el texto cuando el texto está junto al icono.
* [SpinBox] Se ha corregido un error de lógica en el desplazamiento direccional.
* Se ha corregido la barra de desplazamiento de la plataforma móvil en modo RTL.
* [PC3 ToolBar] No desactivar los bordes.
* [PC3 ToolBar] Usar propiedades correctas de márgenes SVG para relleno.
* [pc3/scrollview] Eliminar «pixelAligned».
* Añadir áreas de margen.

### Purpose

* [bluetooth] Se ha corregido un fallo al compartir múltiples archivos (fallo 429620).
* Leer la etiqueta traducida de la acción del complemento (fallo 429510).

### QQC2StyleBridge

* Botón: Confiar en «abajo» en lugar de en «pulsado» para el estilo.
* Reducir el tamaño de los botones redondeados en la plataforma móvil.
* Se ha corregido la barra de desplazamiento de la plataforma móvil en modo RTL.
* Corregir la barra de avance en el modo RTL.
* Corregir la visualización RTL para «RangeSlider».

### Solid

* Incluir «errno.h» para «EBUSY/EPERM».
* FstabBackend: Devolver «DeviceBusy» cuando falla el desmontaje en «EBUSY» (error 411772).
* Se ha corregido la detección de «libplist» y «libimobiledevice» modernos.

### Resaltado de sintaxis

* Se han corregido las dependencias de los archivos generados.
* Indexador: Se han corregido algunos problemas y se han desactivado dos comprobaciones (capturar grupo y palabra calve con delimitador).
* indexador: cargar todos los archivos XML en memoria para una comprobación más cómoda.
* Resaltado de C++: actualizar a Qt 5.15.
* Volver a lanzar los generadores de sintaxis cuando se modifica el archivo fuente.
* Unidad de systemd: actualizar a systemd v247.
* ILERPG: simplificar y probar.
* Zsh, Bash, Fish, Tcsh: Se han añadido «truncate» y «tsort» como palabras clave de órdenes de UNIX.
* Latex: algunos entornos matemáticos se pueden anidar (fallo 428947).
* Bash: muchas correcciones y mejoras.
* Se ha añadido «--syntax-trace=stackSize».
* php.xml: Corregir la coincidencia de «endforeach».
* Mover «bestThemeForApplicationPalette» de «KTextEditor» hasta aquí.
* debchangelog: añadir «Trixie».
* alert.xml: Añadir `NOQA`, que es otra alerta popular en el código fuente.
* cmake.xml: Upstream ha decidido posponer `cmake_path` para el próximo lanzamiento.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
