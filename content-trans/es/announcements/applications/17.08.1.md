---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE lanza las Aplicaciones de KDE 17.08.1
layout: application
title: KDE lanza las Aplicaciones de KDE 17.08.1
version: 17.08.1
---
Hoy, 7 de septiembre de 2017, KDE ha lanzado la primera actualización de estabilización para las <a href='../17.08.0'>Aplicaciones de KDE 17.08</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas se incluyen mejoras en Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello, y juegos de KDE, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.36 que contará con asistencia a largo plazo.
