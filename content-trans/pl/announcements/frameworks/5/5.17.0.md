---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Naprawiono filtr daty wykorzystywany przez timeline://
- BalooCtl: Powrót po poleceniach
- Porządki i uodpornienie Baloo::Database::open(), radzenie sobie z większą liczbą usterek
- Dodano sprawdzenie w Database::open(OpenDatabase), tak aby zwracało błąd, jeśli baza danych nie istnieje

### Ikony Bryzy

- Dodano i ulepszono wiele ikon
- Użyto arkuszy stylów dla ikon bryzy (błąd 126166)
- BŁĄD: 355902 naprawiono i zmieniono ekran blokowania systemu (błąd 355902 naprawiono i zmieniono ekran blokowania systemu)
- Dodano informacje w oknach 24 pikselowych dla aplikacji GTK (błąd 355204)

### Dodatkowe moduły CMake

- Usunięto ostrzeżenia gdy ikony SVG(Z) są dostarczane w wielu rozmiarach/poziomach szczegółowości
- Zapewniono, że tłumaczenia są wczytywane w głównym wątku. (błąd 346188)
- Uprzątnięto system budowania ECM.
- Umożliwiono uruchamianie Clazy na dowolnym projekcie KDE
- Biblioteki XINPUT z XCB nie będzie znajdywana domyślnie.
- Katalog eksportowania będzie czyszczony przed ponownym utworzeniem APK
- Użyto quickgit dla adresu URL repozytorium Git.

### Integracja Szkieletów

- Usunięto błąd przy dodawaniu plazmoidu przy wykonywaniu plasma_workspace.notifyrc

### KAktywności

- Usunięto blokadę przy pierwszym uruchomieniu usługi
- Przeniesiono tworzenie QAction do głównego wątku. (błąd 351485)
- Czasami clang-format podejmowało złe decyzje (błąd 355495)
- Usunięto potencjalny problem z synchronizacją
- Użyto org.qtproject zamiast com.trolltech
- Usunięto użycie libkactivities z wtyczek
- Usunięto ustawienia KAStats z API
- Dodano linkowanie i odlinkowywanie do ResultModel

### Narzędzia KDE Doxygen

- Przyspieszono kgenframeworksapidox.

### KArchiwum

- Poprawiono Fix KCompressionDevice::seek(), przy tworzeniu KTar na podstawie KCompressionDevice.

### KCoreAddons

- KAboutData: Zezwalaj na https:// i inne schematy URL w stronach domowych. (błąd 355508)
- Naprawiono właściwość MimeType przy używaniu kcoreaddons_desktop_to_json()

### KDeclarative

- Przeniesiono KDeclarative, aby można było go używać bezpośrednio przez KI18n
- DragArea delegateImage może od teraz być ciągiem znaków, z którego ikona tworzona jest samoczynnie
- Dodano nową bibliotekę CalendarEvents

### KDED

- Zmienna środowiskowa SESSION_MANAGER jest teraz raczej usuwana niż ustawiana na pustą

### Obsługa KDELibs 4

- Naprawiono parę wywołań i18n.

### KFileMetaData

- Oznaczono m4a jako odczytywalny przez taglib

### KIO

- Okno dialogowe ciasteczek: spraw aby działało tak jak ma działać
- Poprawiono sugestię nazw plików, które zmieniają się na coś losowego przy zmianie typu mime podczas czynności zapisz-jako.
- Zarejestrowano nazwę DBus dla kioexec (błąd 353037)
- KProtocolManager uaktualnia się po zmianie ustawień.

### KItemModels

- Naprawiono użycie KSelectionProxyModel w QTableView (błąd 352369)
- Naprawiono zerowanie lub zmianę modelu źródłowego dla  KRecursiveFilterProxyModel.

### KNewStuff

- registerServicesByGroupingNames może domyślnie podawać kilka elementów
- Uczyniono KMoreToolsMenuFactory::createMenuFromGroupingNames leniwym

### KTextEditor

- Dodano podświetlanie składni dl TaskJuggler oraz PL/I
- Dano możliwość wyłączenia podświetlania uzupełnienia słowa kluczowego poprzez interfejs ustawień.
- Drzewo zmienia swój rozmiar, gdy model uzupełniania jest zerowany.

### Szkielet Portfela

- Przypadek, gdy użytkownik nas dezaktywował jest teraz poprawnie obsługiwany

### KWidgetsAddons

- Naprawiono mały artefakt KRatingWidget na hi-dpi.
- Napisano ponownie i poprawiono funkcję wprowadzoną przez błąd 171343 (błąd 171343)

### KXMLGUI

- Nie wywołuj QCoreApplication::setQuitLockEnabled(true) przy inicjalizacji.

### Szkielety Plazmy

- Do poradnika programisty dodano podstawowy plazmoid jako przykład
- Dodano kilka szablonów plazmoidów do kapptemplate/kdevelop
- [kalendarz] Opóźniono zerowanie modelu do czasu gotowości widoku (błąd 355943)
- Nie zmieniaj położenia przy ukrywaniu. (błąd 354352)
- [IconItem] Nie wywołuj usterki dla zerowego wystroju KIconLoader (błąd 355577)
- Upuszczanie plików obrazu na panel nie zapyta już o ustawienie tego obrazu jako tapety dla panelu
- Upuszczenie pliku .plasmoid na panel lub pulpit zapyta o jego instalację i doda go
- usunięto już nieużywany moduł kded platformstatus (błąd 348840)
- zezwolono na wklejanie w polach hasła
- naprawiono umieszczanie menu edycji, dodano przycisk do wyboru
- [kalendarz] Użyto języka ui do uzyskania nazwy miesiąca (błąd 353715)
- [kalendarz] Wydarzenia mogą być uszeregowane wg ich rodzaju
- [kalendarz] Przeniesiono bibliotekę wtyczek do KDeclarative
- [kalendarz] qmlRegisterUncreatableType wymaga trochę więcej argumentów
- Zezwól na dynamiczne dodawanie kategorii ustawień
- [kalendarz] Przenieś obsługę wtyczek do osobnej klasy
- Zezwól wtyczką na podawanie danych wydarzenia w aplecie kalendarza (błąd 349676)
- przed połączeniem lub rozłączeniem sprawdzane jest istnienie slotu (błąd 354751)
- [plasmaquick] Nie dowiązuj jednoznacznie OpenGL
- [plasmaquick] Porzuć XCB::COMPOSITE oraz zależność DAMAGE

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
