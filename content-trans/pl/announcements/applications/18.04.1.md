---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE wydało Aplikacje KDE 18.04.1
layout: application
title: KDE wydało Aplikacje KDE 18.04.1
version: 18.04.1
---
10 maja 2018. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../18.04.0'>Aplikacji KDE 18.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

About 20 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, Gwenview, JuK, Okular, Umbrello, among others.

Wśród ulepszeń znajdują się:

- Duplicate entries in Dolphin's places panel no longer cause crashes
- An old bug with reloading SVG files in Gwenview was fixed
- Umbrello's C++ import now understands the 'explicit' keyword
