---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE toob välja KDE rakendused 18.08.0
layout: application
title: KDE toob välja KDE rakendused 18.08.0
version: 18.08.0
---
16. august 2018. KDE laskis välja rakendused 18.08.0.

Me jätkame visalt tööd meie KDE rakendustesse kaasatud tarkvara parandamisel ja täiustamisel ning loodame, et kõik uued omadused ja veaparandused tulevad kasutajatele kasuks!

### What's new in KDE Applications 18.08

#### Süsteem

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, has received various quality-of-life improvements:

- Seadistusdialoogi kaasajastati meie kujundusjuhiste täpsema järgimise huvides ja see on nüüd intuitiivsem.
- Kõrvaldati rohkelt mälulekkeid, mis aeglustasid arvuti tööd.
- Menüükirjed "Loo uus" all ei ole enam saadaval, kui parajasti vaadatakse prügikasti.
- Rakendus kohandub nüüd paremini kõrglahutusega ekraanidega.
- Kontekstimenüüs on nüüd rohkem kasulikke valikuid, mis lubavad vahetult sortida ja vaadet muuta.
- Sorting by modification time is now 12 times faster. Also, you can now launch Dolphin again when logged in using the root user account. Support for modifying root-owned files when running Dolphin as a normal user is still work in progress.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Multiple enhancements for <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, are available:

- Otsimisvidin paikneb nüüd akna ülaoas ega seda muud tegevust.
- Lisatud on enamate paojadade (DECSCUSR ja XTermi alternatiivne kerimisrežiim) toetus.
- Samuti saab nüüd millist tahes sümbolit kasutada kiirklahvina.

#### Graafika

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 is a major release for <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer. Over the last months contributors worked on a plethora of improvements. Highlights include:

- Gwenview olekuribal on nüüd pildiarvesti, mis näitab piltide koguarvu.
- Müüd saab sortida hinnangu järgi ja alanevas järjekorras. Sortimisel kuupäeva järgi eristatakse katalooge ja arhiive ning selle juures on parandatud mitmeid vigu.
- Lohistamise toetust on täiustatud: nüüd saab faile ja katalooge vaatamiseks lohistada vaaterežiimi, samuti nähtud elemente välistesse rakendustesse.
- Gwenview'st lopeeritud piltide asetamine toimib nüüd ka rakenduste puhul, mis tunnistavad ainult toorpildiandmeid, aga mitte failiasukohta. Samuti on nüüd toetatud muudetud piltide kopeerimine.
- Pildi suuruse muutmise dialoogi on korralikult ümber töötatud tõhusama kasutamise huvides, samuti on lisatud võimalus muuta piltide suurust protsendi järgi.
- Punasilmsuse vähendamise tööriista suuruseliugur ja ristik parandati ära.
- Läbipaistva tausta valimisel on nüüd ka valik "Puudub" ning seadistada saab ka SVG-faile.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

Piltide suurendamine on varasemast mugavam:

- Suurendamine on lubatud kerimisega, klõpsamisega kui ka vaadet lohistades, kui kasutada kärpimise või punasilmsuse vähendamise tööriista.
- Paremkõpsuga saab taas lülitada sobitava suurenduse ja 100% suurenduse vahel.
- Täitmissuurenduse lülitamiseks lisati kiirklahvid Tõstuklahv+keskmise nupu kõps ja Tõstuklahv+ F.
- Ctrl+klõps suurendab nüüd kiiremini ja usaldusväärsemalt.
- Gwenview suurendab nüüd suurendamise-vähendamise, täitmise ja 100% suurenduse toimingute korral hiirt ja kiirklahve kasutades nii, et võtab keskmeks hiirekursori asukoha.

Mitmeid täiustusi sai piltide võrdlemise režiim:

- Parandati valiku esiletõstmise suurust ja joondust.
- Parandati SVG-de kattumine valiku esiletõstmisega.
- Väiksemate SVG-piltide puhul vastab valiku esiletõstmine pildi suurusele.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

Mõned täiustused aitavad muuta töötamise märksa nauditavamaks:

- Erineva suuruse ja läbipaistvusega piltide üleminekute hääbumisega täiustamine.
- Parandati ikoonide nähtavust mõningates hõljuvates nuppudes heleda värviskeemi kasutamisel.
- Piltide salvestamisel teise nimega ei hüppa näitaja seejärel enam täiesti seosetult mõnele muule pildile.
- Kui kasutaja klõpsab jagamisnuppu, aga KIPI pluginaid pole paigaldatud, soovitab Gwenview need paigaldada. Paigaldamise järel näidatakse neid viivitamata.
- Külgriba juhuslik peitmine suuruse muutmisel on nüüd välistatud ning see jätab määratud laiuse meelde.

#### Kontoritöö

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client, features some improvements in the travel data extraction engine. It now supports UIC 918.3 and SNCF train ticket barcodes and Wikidata-powered train station location lookup. Support for multi-traveler itineraries was added, and KMail now has integration with the KDE Itinerary app.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, the personal information management framework, is now faster thanks to notification payloads and features XOAUTH support for SMTP, allowing for native authentication with Gmail.

#### Õpirakendused

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE's frontend to mathematical software, now saves the status of panels (\"Variables\", \"Help\", etc.) for each session separately. Julia sessions have become much faster to create.

User experience in <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, our graphing calculator, has been significantly improved for touch devices.

#### Tööriistad

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Contributors to <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, focused on improving the Rectangular Region mode:

- Ristkülikukujulise ala režiimis on nüüd suurendaja, mis lubab ristküliku paika panna piksli täpsusega.
- Valikuristkülikut saab nüüd liigutada ja selle suurust muuta klaviatuuri abil.
- Kasutajaliides järgib kasutaja värviskeemi ning abiteksti esitamist on täiustatud.

Ekraanipiltide jagamise hõlbustamiseks kopeeritakse jagatud piltide lingid nüüd automaatselt lõikepuhvrisse. Ekraanipilte saab nüüd lasta automaatselt salvestada kasutaja määratud alamkataloogidesse.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, our webcam recorder, was updated to avoid crashes with newer GStreamer versions.

### Vigadest vabaks

Üle 120 vea parandati sellistes rakendustes, nagu Kontacti komplekt, Ark, Cantor, Dolphin, Gwenview, Kate, Okular, Spectacle, Umbrello jt!

### Täielik muudatuste logi
