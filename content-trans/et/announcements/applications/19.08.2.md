---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE Ships Applications 19.08.2.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE Ships Applications 19.08.2
version: 19.08.2
---
{{% i18n_date %}}

Today KDE released the second stability update for <a href='../19.08.0'>KDE Applications 19.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 20 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Dolphini, Gwenview, Kate, Kdenlive'i, Konsooli, Lokalize ja Spectacle'i täiustused.

Täiustused sisaldavad muu hulgas:

- Kõrglahutusega DPI toetuse täiustamine Konsoolis ja teistes rakendustes
- Lülitumisel eri otsingute vahel uuendab Dolphin nüüd korrektselt otsinguparameetreid
- KMail suudab taas salvestada kirju otse võrgukataloogi
