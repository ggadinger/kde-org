---
date: 2013-08-14
hidden: true
title: KDE Plasma töötsoonid 4.11 jätkab kasutamiskogemuse lihvimisega
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`KDE Plasma töötsoonid 4.11` width="600px" >}}

In the 4.11 release of Plasma Workspaces, the taskbar – one of the most used Plasma widgets – <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>has been ported to QtQuick</a>. The new taskbar, while retaining the look and functionality of its old counterpart, shows more consistent and fluent behavior. The port also resolved a number of long standing bugs. The battery widget (which previously could adjust the brightness of the screen) now also supports keyboard brightness, and can deal with multiple batteries in peripheral devices, such as your wireless mouse and keyboard. It shows the battery charge for each device and warns when one is running low. The Kickoff menu now shows recently installed applications for a few days. Last but not least, notification popups now sport a configure button where one can easily change the settings for that particular type of notification.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Täiustatud märguannete käitlemine` width="600px" >}}

KMix, KDE's sound mixer, received significant performance and stability work as well as <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>full media player control support</a> based on the MPRIS2 standard.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`Ümber kujundatud akuaplett` width="600px" >}}

## Aknahaldur KWin

Meie aknahaldur KWin sai taas olulisi uuendusi, jättes selja taha pärandtehnoloogia ja võttes omaks XCB suhtlemisprotokolli. See võimaldab akende sujuvamat ja kiiremat haldamist. Samuti on nüüd toetatud OpenGL 3.1 ja OpenGL ES 3.0. Ühtlasi pakub see väljalase esmast X11 järglase Waylandi eksperimentaalset toetust. See lubab kasutada KWini X11-ga Waylandi raamistiku peal. Täpsemalt kõneleb selle eksperimentaalse režiimi kasutamisest <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>see postitus</a>. KWini skriptimise liidest on tunduvalt täiustatud ja see toetab seadistuste kasutajaliidest, uusi animatsioone ja graafilisi efekte, rääkimata muudest väiksematest täiustustest. Väljalase toob samuti kaasa parema mitme monitori tunnetuse (sealhulgas serva helendamise võimalus "kuumades nurkades"), parema kiire paanimise (paanitavate alade seadistamise võimalusega) ja, nagu ikka, rohkelt veaparandusi ja optimeerimisi. Üksikasju saab lugeda <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>siit</a> ja <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>siit</a>.

## Monitori käitlemine ja veebikiirklahvid

Monitori seadistamine Süsteemi seadistustes asendati <a href='http://www.afiestas.org/kscreen-1-0-released/'>uue tööriistaga KScreen</a>. See tagab Plasma töötsoonidele märksa nutikama mitme monitori toetuse, võimaldab rakendada uutele monitoridele seadistusi automaatselt ning jätab meelde käsitsi seadistatud monitoride seadistused. Liides on lihtne ja arusaadav, orienteeritud visuaalsele väljanägemisele, ning monitore saab ümber korraldada lihtsalt lohistades.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`Uus KScreen monitore käitlemas` width="600px" >}}

Veebikiirkorraldusi ehk lihtsaimat võimalust leida veebist kiiresti, mida tarvis, on oluliselt puhastatud ja täiustatud. Enamikku on uuendatud kasutama turvaliselt krüptitud (TSL/SSL) ühendusi, lisatud on uusi veebikiirkorraldusi ja mõned iganenud eemaldatud. Täiustatud on ka kasutaja enda veebikiirkorralduse lisamise võimalust. Üksikasju saab lugeda <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>siit</a>.

See väljalase märgib ühtlasi KDE tarkvarakomplekti 4 koosseisu kuulunud Plasma töötsoonide 1 lõppu. Lihtsustamaks üleminekut järgmisele põlvkonnale, toetatakse seda väljalaset vähemalt kaks aastat. Uute omaduste arendajate tähelepanu koondub nüüd Plasma töötsoonide 2 peale, 4.11 seeria aga saab edasi jõudluse täiustusi ja veaparandusi.

#### Plasma paigaldamine

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Tarkvarapaketid

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Pakettide asukohad

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Nõuded süsteemile

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Täna ilmusid veel:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

See väljalase toob kaasa hulganisi täiustusi KDE PIM-i rakendustes, tagades tunduvalt parema jõudluse ja mitmeid uusi võimalusi. Kate täiustused lubavad neil, kellele meeldib kasutada Pythonit ja JavaScripti, uute pluginate abil produktiivsust suurendada. Dolphin on kiirem ning õpirakendused pakuvad mimesuguseid uusi võimalusi.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

KDE arendusplatvormi versioon 4.11 keskendub endiselt stabiilsusele. Tulevase KDE Frameworks 5.0 puhuks oleme näinu vaeva ka uute omadustega, aga stabiilses väljalaskes on meie tähelepanu keskmes olnud eelkõige Nepomuki raamistiku optimeerimine.
