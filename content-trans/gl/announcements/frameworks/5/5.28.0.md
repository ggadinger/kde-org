---
aliases:
- ../../kde-frameworks-5.28.0
date: 2016-11-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nova infraestrutura: syntax-highlighting

Motor de salientado de sintaxe para definicións de sintaxe de Kate

Esta é unha codificación independente do motor de realce de sintaxe de Kate. Está pensado como bloque de construción para editores de texto e para renderización simple de texto realzado (p. ex. HTML), permitindo tanto integración cun editor personalizado como unha subclase de QSyntaxHighlighter lista pasa usar.

### Iconas de Breeze

- actualizar as iconas de accións de KStars (fallo 364981)
- Breeze Escuro lístase como Breeze na configuración do sistema, ficheiro .themes incorrecto (fallo 370213)

### Módulos adicionais de CMake

- Facer que KDECMakeSettings funcione con KDE_INSTALL_DIRS_NO_DEPRECATED
- Non requirir as dependencias da API de Python para ECM
- Engadir o módulo PythonModuleGeneration

### KActivitiesStats

- Ignorar o estado de ligazón ao ordenar os modelos UsedResources e LinkedResources

### Ferramentas de Doxygen de KDE

- [CSS] reverter os cambios feitos por doxygen 1.8.12
- Engadir un ficheiro de doxygenlayout
- Actualizar a forma de definir nomes de grupos

### KAuth

- Asegurarse de que se pode facer máis dunha solicitude
- Asegurarse de que nos informamos do progreso lendo a saída do programa

### KConfig

- Asegurarse de que non rompemos a compilación con unidades rotas pasadas
- Continuar aínda que non se analice correctamente o campo File (ficheiro)

### KCoreAddons

- Mostrar un URL incorrecto
- Cargar os avatares de usuario de AccountsServicePath se existe (fallo 370362)

### KDeclarative

- [QtQuickRendererSettings] Corrixir o predeterminado para estar baleiro en vez de ser «false» 

### Compatibilidade coa versión 4 de KDELibs

- Facer que a bandeira de Francia use o mapa de píxeles completo

### KDocTools

- Corrixir «checkXML5 xera ficheiros HTML no directorio de traballo para docbooks válidos» (fallo 371987)

### KIconThemes

- Permitir factores de cambio de dimensións non enteiros en kiconengine (fallo 366451)

### KIdleTime

- Desactivouse cubrir toda a saída de consola de mensaxes de «agardando»

### KImageFormats

- imageformats/kra.h - sobreposicións para overrides capabilities() e create() de KraPlugin

### KIO

- Corrixir o formato de data de HTTP que envía kio_http para que sempre use a configuración rexional C (fallo 372005)
- KACL: corrixir fugas de memoria detectadas por ASAN
- Corrixir fugas de memoria en KIO::Scheduler, detectado por ASAN
- Retirar un botón duplicado de baleirar (fallo 369377)
- Corrixir a edición das entradas de inicio automático cando /usr/local/share/applications non existe (fallo 371194)
- [KOpenWithDialog] Agochar a cabeceira de TreeView
- Desinfectar o tamaño de búfer do nome da ligazón simbólica (fallo 369275)
- Rematar de maneira axeitada os DropJob cando non se emite triggered (fallo 363936)
- ClipboardUpdater: corrixir outra quebra en Wayland (fallo 359883)
- ClipboardUpdater: corrixir unha quebra en Wayland (fallo 370520)
- Permitir factores de cambio de dimensións non enteiros en KFileDelegate (fallo 366451)
- kntlm: Distinguir entre NULL e dominio baleiro
- Non mostrar o diálogo de sobrescribir se o nome de ficheiro está baleiro
- kioexec: usar nomes de ficheiro intuitivos
- Corrixir quen ten o foco cando se cambia o URL antes de mostrar o trebello
- Gran mellora de rendemento ao desactivar as vistas previas no diálogo de ficheiros (fallo 346403)

### KItemModels

- Engadir API para Python

### KJS

- Exportar FunctionObjectImp para que o depurador de khtml poida usalo

### KNewStuff

- Ordenar por separado roles e filtros
- Permitir consultar as entradas instaladas

### KNotification

- Non resolver un obxecto ao que non fixemos referencia cando a notificación non ten acción
- KNotification deixará de quebrar ao usalo nun QGuiApplication se non hai ningún servizo de notificacións en funcionamento (fallo 370667)
- Corrixir quebras en NotifyByAudio

### Infraestrutura KPackage

- Asegurarse de que buscamos metadatos tanto de JSON como de escritorio
- Impedir que Q_GLOBAL_STATIC se destrúa ao apagarse a aplicación
- Corrixir un punteiro solto en KPackageJob (fallo 369935)
- Retirar o descubrimento asociado a unha clave ao retirar unha definición
- Xerar a icona no ficheiro de AppStream

### KPty

- Usar ulog-helper en FreeBSD en vez de utempter
- buscar utempter mellor usando tamén un prefixo básico de CMake
- apañar os fallos de find_program(utempter…)
- usar a tira de ECM para atopar o binario utempter, máis fiábel que un simple prefixo de CMake

### KRunner

- i18n: xestionar cadeas en ficheiros kdevtemplate

### KTextEditor

- Breeze Escuro: escurecer a cor de fondo da liña actual para mellorar a lexibilidade (fallo 371042)
- Ordenáronse as instrucións do Dockerfile
- Breeze (Escuro): Facer os comentarios máis claros para que sexan máis fáciles de ler (fallo 371042)
- Corrixir os sangradores de CStyle e C++ ou boost cando se activan os corchetes automáticos (fallo 370715)
- Engadir parénteses automáticos no modo de liña
- Corrixir a inserción de texto tras o final do ficheiro (caso estraño)
- Corrixir ficheiros XML de salientado incorrectos
- Maxima: Retirar cores definidas a man, corrixir a etiqueta de itemData
- Engadir definicións de sintaxe de OBJ, PLY e STL
- Engadir salientado de sintaxe para Praat

### KUnitConversion

- Novas unidades termais e eléctricas e función de comodidade de unidades

### Infraestrutura de KWallet

- Se non se atopa Gpgmepp, intentar usar KF5Gpgmepp
- Usar Gpgmepp de GpgME-1.7.0

### KWayland

- Mellorouse a posibilidade de cambio de lugar de exportación de CMake
- [tools] Corrixir a xeración de wayland_pointer_p.h
- [ferramentas] Xerar métodos eventQueue só para clases globais
- [servidor] Corrixir unha quebra ao actualizar unha superficie de teclado co foco
- [servidor] Corrixir unha quebra posíbel na creación de DataDevice
- [servidor] Asegurarse de que temos un DataSource no DataDevice de setSelection
- [tools/generator] Mellorar a destrución de recursos no lado do servidor
- Engadir unha solicitude para ter o foco nunha PlasmaShellSurface de panel de rol
- Engadir a posibilidade de paneis que se agochan automaticamente á interface PlasmaShellSurface
- Permitir pasar un QIcon xenérico a través da interface PlasmaWindow
- [servidor] Crear a propiedade xenérica «window» en QtSurfaceExtension
- [cliente] Engadir métodos para obter ShellSurface dunha QWindow
- [servidor] Enviar os eventos de punteiro aos recursos de wl_pointer dun cliente
- [servidor] Non chamar a wl_data_source_send_send se DataSource non está asociado
- [servidor] Usar deleteLater cando se destrúe unha ClientConnection (fallo 370232)
- Engadir compatibilidade co protocolo de punteiro relativo
- [servidor] Cancelar a selección anterior desde SeatInterface::setSelection
- [servidor] Enviar eventos de tecla a todos os recursos de wl_keyboard dun cliente

### KWidgetsAddons

- mover kcharselect-generate-datafile.py ao subdirectorio src
- Importar o script kcharselect-generate-datafile.py script con historial
- Retirar unha sección sen actualizar
- Engadir unha nota de dereitos de autor e permisos sobre Unicode
- Corrixir un aviso: Falta unha sobreposta
- Engadir bloques SMP de símbolos
- Corrixir as referencias de «Ver tamén»
- Engadir bloques de Unicode que faltaban; mellorar o ordenamento (fallo 298010)
- engadir as categorías dos caracteres ao ficheiro de datos
- actualizar as categorías de Unicode no script de xeración do ficheiro de datos
- axustar o ficheiro de xeración de ficheiro de datos para poder analizar os ficheiros de datos de Unicode 5.2.0
- migrar cara adiante unha corrección da xeración das traducións
- deixar que o script para xerar o ficheiro de datos para KCharSelect tamén escriba modelos de tradución
- Engadir o script para xerar o ficheiro de datos para KCharSelect
- nova aplicación KCharSelect (agora usa o trebello using kcharselect de kdelibs)

### KWindowSystem

- Mellorouse a posibilidade de cambio de lugar de exportación de CMake
- Engadir compatibilidade con desktopFileName a NETWinInfo

### KXMLGUI

- Permitir usar o novo estilo de connect en KActionCollection::add<a href="">Action</a>

### ModemManagerQt

- Corrixir o directorio de inclusión no ficheiro pri

### NetworkManagerQt

- Corrixir o directorio de inclusión no ficheiro pri
- Corrixir un erro de moc por mor de usar Q_ENUMS nun espazo de nomes, coa rama 5.8 de Qt

### Infraestrutura de Plasma

- asegurarse de que OSD non ten marca de Dialog (fallo 370433)
- definir as propiedades de contexto antes de cargar de novo o qml (fallo 371763)
- Non analizar de novo o ficheiro de metadatos se xa se cargou
- Corrixir unha quebra en qmlplugindump cando non hai ningún QApplication dispoñíbel
- Non mostrar o menú de «Alternativas» de maneira predeterminada
- Novo booleano para usar o sinal activado como conmutador do expandido (fallo 367685)
- Correccións para construír plasma-framework con Qt 5.5
- [PluginLoader] Usar o operador &lt;&lt; para finalArgs en vez de unha lista de inicialización
- usar KWayland para sombras e para a colocación de diálogos
- Iconas restantes e melloras de rede
- Ascender availableScreenRect e Region a AppletInterface
- Non cargar as accións de contedor para contedores incrustados (área de notificacións)
- Actualizar a visibilidade da entrada de menú de alternativas de miniaplicativo baixo demanda

### Solid

- Corrixir unha vez máis a orde inestábel dos resultados da consulta
- Engadir unha opción de CMake para cambiar entre os xestores HAL e UDisks en FreeBSD
- Facer que a infraestrutura de UDisks2 compile en FreeBSD (e, posiblemente, noutros UNIX)
- Windows: non mostrar os diálogos de erro (fallo 371012)

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
