---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE Ships Applications 14.12.3.
layout: application
title: KDE publica a versión 14.12.3 das aplicacións de KDE
version: 14.12.3
---
March 3, 2015. Today KDE released the third stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Entre as 19 correccións de erros rexistradas están melloras no xogo de anagramas Kanagram, o modelador de UML Umbrello, o visor de documentos Okular e a aplicación xeométrica Kig.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.17, KDE Development Platform 4.14.6 and the Kontact Suite 4.14.6.
