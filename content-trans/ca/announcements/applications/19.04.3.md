---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: Es distribueixen les aplicacions 19.04.3 del KDE.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE distribueix les aplicacions 19.04.3 del KDE
version: 19.04.3
---
{{% i18n_date %}}

Avui, KDE distribueix la tercera actualització d'estabilització per a les <a href='../19.04.0'>Aplicacions 19.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, i proporciona una actualització segura i millor per a tothom.

Hi ha prop de 60 esmenes registrades d'errors que inclouen millores al Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular i Umbrello entre d'altres.

Les millores inclouen:

- El Konqueror i el Kontact ja no fallen en sortir amb el QtWebEngine 5.13
- En retallar grups amb composicions ja no fa fallar l'editor de vídeo Kdenlive
- L'importador de Python al dissenyador d'UML Umbrello ara gestiona els paràmetres amb arguments predeterminats
