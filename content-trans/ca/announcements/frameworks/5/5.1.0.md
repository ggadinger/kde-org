---
aliases:
- ../../kde-frameworks-5.1
- ./5.1
customIntro: true
date: '2014-08-08'
description: KDE distribueix el segon llançament dels Frameworks 5.
layout: framework
qtversion: 5.2
title: Segon llançament dels Frameworks 5 de KDE
---
7 d'agost de 2014. El KDE ha anunciat avui la segona versió dels Frameworks 5 de KDE. En línia amb la política de llançaments prevista per als Frameworks del KDE. Aquesta versió esdevé un mes després de la versió inicial i conté correccions d'errors i noves característiques.

{{% i18n "annc-frameworks-intro" "60" "/announcements/frameworks/5/5.0" %}}

## {{< i18n "annc-frameworks-new" >}}

This release, versioned 5.1, comes with a number of bugfixes and new features including:

- KTextEditor: Major refactorings and improvements of the vi-mode
- KAuth: Now based on PolkitQt5-1
- New migration agent for KWallet
- Windows compilation fixes
- Translation fixes
- New install dir for KXmlGui files and for AppStream metainfo
- <a href='http://www.proli.net/2014/08/04/taking-advantage-of-opengl-from-plasma/'>Plasma Taking advantage of OpenGL</a>
