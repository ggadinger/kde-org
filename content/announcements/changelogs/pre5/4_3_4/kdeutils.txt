------------------------------------------------------------------------
r1044563 | rkcosta | 2009-11-04 03:22:09 +0000 (Wed, 04 Nov 2009) | 4 lines

Backport r1044561.

Include header for correctness

------------------------------------------------------------------------
r1044564 | rkcosta | 2009-11-04 03:22:49 +0000 (Wed, 04 Nov 2009) | 4 lines

Backport r1044562.

Use a const QString& in the parameter.

------------------------------------------------------------------------
r1044566 | rkcosta | 2009-11-04 03:26:04 +0000 (Wed, 04 Nov 2009) | 6 lines

Backport r1044565.

Set status to 'reading first line' after a closing header line.

CCBUG: 212027

------------------------------------------------------------------------
r1045773 | dakon | 2009-11-06 18:54:38 +0000 (Fri, 06 Nov 2009) | 6 lines

fix passphrases with non-ascii characters

CCBUG:155790

backport of r1043371

------------------------------------------------------------------------
r1046532 | rkcosta | 2009-11-09 01:14:09 +0000 (Mon, 09 Nov 2009) | 14 lines

Backport r1046438.

Explicitly close the part when closing ArkViewer.

Calling ReadOnlyPart::closeUrl() is necessary, otherwise
the part may be destroyed before performing some proper
clean-up.

A progress bar dialog is shown if the part takes too long
to close.

CCBUG: 211880
CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
r1046919 | kossebau | 2009-11-09 23:14:39 +0000 (Mon, 09 Nov 2009) | 4 lines

backport of 1046918: fixed: doing some grouped change (e.g. editing values, which is one behind the scenes) if unmodified does not emit the modifiedChanged signal

CCBUG: 213868

------------------------------------------------------------------------
r1046929 | kossebau | 2009-11-09 23:33:30 +0000 (Mon, 09 Nov 2009) | 4 lines

backport of 1046927: fixed: logic was inverted, canClose should be true if a modified document was successfully synced

BUG: 213868

------------------------------------------------------------------------
r1046967 | rkcosta | 2009-11-10 03:02:14 +0000 (Tue, 10 Nov 2009) | 4 lines

Backport r1046966.

Use a const-reference in the signal.

------------------------------------------------------------------------
r1047100 | kossebau | 2009-11-10 13:44:00 +0000 (Tue, 10 Nov 2009) | 6 lines

fixed: the signal in the 4.3 branch is still modificationChanged, not modifiedChanged
Oh failing joy of svnbackport...

Thanks for the hint, André!
CCMAIL:Woebbeking@kde.org

------------------------------------------------------------------------
r1047105 | kossebau | 2009-11-10 14:00:20 +0000 (Tue, 10 Nov 2009) | 1 line

changed: next version is 0.3.4
------------------------------------------------------------------------
r1047318 | kossebau | 2009-11-11 00:07:16 +0000 (Wed, 11 Nov 2009) | 1 line

backport of 1047317: fixed: if checking chars do not extra handle spaces
------------------------------------------------------------------------
r1049194 | rkcosta | 2009-11-14 17:36:01 +0000 (Sat, 14 Nov 2009) | 7 lines

Backport r1049192.

Protect m_part with a QPointer.

Let's not forget bug 206814: the part may have deleted itself
when we call m_part->closeUrl().

------------------------------------------------------------------------
r1049727 | dakon | 2009-11-15 18:21:48 +0000 (Sun, 15 Nov 2009) | 6 lines

properly handle self signature references

Those references must not search for the root node when a key was updated as those would usually happen when their parent is going to be deleted. Traversing the keychain upwards across that parent-on-deletion will cause crashes. At the end we don't need to do anything then: if the parent is going to be deleted this signature will follow shortly after so selfsigs never need to be refreshed.

BUG:208659

------------------------------------------------------------------------
r1051242 | scripty | 2009-11-19 04:15:53 +0000 (Thu, 19 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1052147 | dakon | 2009-11-20 21:40:36 +0000 (Fri, 20 Nov 2009) | 1 line

fix crash after deleting photo id
------------------------------------------------------------------------
r1052436 | dakon | 2009-11-21 17:37:28 +0000 (Sat, 21 Nov 2009) | 4 lines

fix some actions not having descriptions until items were selected

backport of r1049222

------------------------------------------------------------------------
