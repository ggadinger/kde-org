2005-03-03 23:31 +0000 [r394743]  carewolf

	* kfile-plugins/flac/kfile_flac.cpp: Backport

2005-03-13 23:03 +0000 [r397415]  carewolf

	* akode/arts_plugin/akodePlayObject_impl.cpp: Backport of pitch

2005-03-14 21:57 +0000 [r397646]  mattr

	* kioslave/audiocd/plugins/vorbis/audiocd_vorbis_encoder.kcfg: The
	  quality setting is a double and not integer. Backported from HEAD
	  CCMAIL: nhasan@kde.org

2005-03-16 10:12 +0000 [r398044]  larkang

	* libkcddb/cddb.h, libkcddb/cdinfo.cpp, libkcddb/cdinfo.h:
	  Backport: Split lines when longer than 256 chars

2005-03-16 12:53 +0000 [r398073]  larkang

	* libkcddb/cdinfo.cpp: Backport: Make sure we don't split inside an
	  UTF-8 character

2005-03-17 22:57 +0000 [r398551-398550]  wheeler

	* juk/googlefetcher.h, juk/googlefetcherdialog.h,
	  juk/googlefetcher.cpp, juk/googlefetcherdialog.cpp: Backporting:
	  Don't show the image selection dialog when no matching images
	  were found.

	* juk/trackpickerdialog.cpp: Backporting: Make double clicking on
	  the MusicBrainz track selection dialog "accept" the dialog.

2005-03-18 15:42 +0000 [r398741]  larkang

	* libkcddb/cdinfo.cpp: Backport: Split at 256 unicode characters
	  instead of 256 bytes Allow multiline genres

2005-03-20 20:44 +0000 [r399330]  binner

	* kaboodle/kaboodle.desktop: X-KDE-More;

2005-03-24 13:13 +0000 [r400224]  larkang

	* kaudiocreator/tracksimp.cpp: Backport: Fix logic

2005-03-25 13:03 +0000 [r400498]  esken

	* kmix/mixer_alsa.h, kmix/mixer_alsa9.cpp: Backport of a bugfix
	  (invalid usage of snd_mixer_selem_id_get_index()). This also
	  possibly fixes bug 100561. CCBUGS:100561

2005-04-03 12:02 +0000 [r402833]  charles

	* arts/modules/effects/mcopclass/Synth_FREEVERB.mcopclass,
	  arts/modules/effects/mcopclass/Synth_VOICE_REMOVAL.mcopclass:
	  backports of that...

2005-04-03 12:07 +0000 [r402837]  charles

	* noatun/library/effects.cpp,
	  noatun/modules/artseffects/ExtraStereo.mcopclass,
	  noatun/modules/artseffects/VoiceRemoval.mcopclass: Use=directly
	  backport to KDE 3.4.n

2005-04-03 16:49 +0000 [r402900]  antlarr

	* kmid/kdisptext.cpp: Backport: Fix the viewport not being
	  repainted after restarting a song (the lyrics remained
	  highlighted)

2005-04-06 20:12 +0000 [r403603]  larkang

	* libkcddb/httplookup.cpp: Backport: Return when result is
	  ServerError or NoRecordFound

2005-04-09 04:39 +0000 [r404229]  bmeyer

	* kioslave/audiocd/plugins/lame/encoderlame.h,
	  kioslave/audiocd/Makefile.am, kioslave/audiocd/audiocd.cpp,
	  kioslave/audiocd/plugins/vorbis/encodervorbis.cpp,
	  kioslave/audiocd/configure.in.in,
	  kioslave/audiocd/plugins/wav/encodercda.h,
	  kioslave/audiocd/plugins/flac/encoderflac.cpp,
	  kioslave/audiocd/plugins/lame/encoderlame.cpp,
	  kioslave/audiocd/plugins/audiocdencoder.h,
	  kioslave/audiocd/plugins/vorbis/encodervorbis.h,
	  kioslave/audiocd/plugins/flac/encoderflac.h,
	  kioslave/audiocd/plugins/wav/encoderwav.h: Backport bugfixes:
	  93988, 101982

2005-04-10 09:44 +0000 [r404523]  larkang

	* kaudiocreator/tracksimp.cpp: Backport: Make sure the disc-id is
	  never shorter than 8 characters

2005-04-20 18:58 +0000 [r406782]  wheeler

	* juk/playlistitem.cpp, juk/musicbrainzquery.cpp: Backporting: Fix
	  memory leak.

2005-04-25 12:08 +0000 [r407748]  lukas

	* krec/krecconfig_files.cpp: show the UI translated

2005-04-27 19:02 +0000 [r408227]  bmeyer

	* kaudiocreator/kaudiocreator.kcfg: Backport bug fix (use track
	  number)

2005-04-27 19:06 +0000 [r408229]  bmeyer

	* kaudiocreator/jobqueimp.cpp: BUG:103574 Fix for 3.4 branch.

2005-04-28 02:53 +0000 [r408302]  bmeyer

	* libkcddb/cdinfodialogbase.ui.h, libkcddb/cdinfodialogbase.ui:
	  Backport fix for the libkcddb dialog which was used in 3.4, but
	  didn't provide the the same functionality to support multiple
	  artist as in 3.3, but only with the magic "Various" group which
	  was confusing. Added a checkbox so users can turn on/off multiple
	  artist. BUGS:103640

2005-04-29 13:41 +0000 [r408616]  lunakl

	* kmix/kmix.cpp, kmix/KMixApp.cpp, kmix/KMixApp.h: Backport crash
	  on exit fix.

2005-05-10 10:32 +0000 [r411876]  binner

	* kdemultimedia.lsm: update lsm for release

2005-05-10 15:39 +0000 [r412009]  carewolf

	* akode/plugins/mpc_decoder/mppdec/streaminfo.cpp,
	  akode/plugins/mpc_decoder/mppdec/mpc_dec.cpp,
	  akode/plugins/mpc_decoder/mppdec/streaminfo.h,
	  akode/plugins/mpc_decoder/mppdec/mpc_math.h,
	  akode/plugins/mpc_decoder/mppdec/mpc_dec.h: Backport of crash bug
	  in mppdec on AMD64 CCBUG: 102105

2005-05-12 20:55 +0000 [r412897]  carewolf

	* akode/plugins/xiph_decoder/vorbis_decoder.cpp: Backport of the
	  streaming ogg vorbis fix.

2005-05-16 06:48 +0000 [r414494]  mpyne

	* juk/tagtransactionmanager.cpp: Backport fix for bug 105725
	  ('undo' affects more than one action) to KDE 3.4. BUG:105725

2005-05-20 15:28 +0000 [r416084]  carewolf

	* akode/lib/player.cpp, akode/lib/streamtoframe_decoder.cpp,
	  akode/lib/player.h, akode/lib/streamtoframe_decoder.h,
	  akode/lib/audiobuffer.cpp: Backport pause looping fix, but
	  maintain old API.

2005-05-22 07:56 +0000 [r416651]  binner

	* juk/main.cpp, noatun/library/noatun/app.h, kmix/version.h:
	  Increased versions for KDE 3.4.1

2005-05-22 08:22 +0000 [r416663]  binner

	* kaboodle/kaboodle_factory.cpp: In Bugzilla a 1.7.1 version number
	  exists?

2005-05-22 11:05 +0000 [r416716]  esken

	* kmix/mixer_alsa9.cpp: Backport the "off-by-one" fix to KDE_3_4
	  branch. This might fix some strange behaviour issues and also
	  some KMix crashes (mostly experienced with emu10k1 type cards.

2005-05-22 14:28 +0000 [r416893]  larkang

	* kaudiocreator/tracksimp.cpp: Hopefully fix the slowness-problem
	  in kaudiocreator. Fixes the problem for me, but would be nice if
	  someone comfirmed that it works. CCBUG: 98477 Will commit a
	  different fix for trunk, but waiting for Ben to comment on it

