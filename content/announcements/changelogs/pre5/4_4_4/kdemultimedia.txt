------------------------------------------------------------------------
r1124773 | mpyne | 2010-05-10 15:47:42 +1200 (Mon, 10 May 2010) | 2 lines

SVN_SILENT Update my email address in JuK 4.4.4 (not a translated string)

------------------------------------------------------------------------
r1126310 | esken | 2010-05-14 04:21:30 +1200 (Fri, 14 May 2010) | 4 lines

Fix profile selection - backport to KDE4.4 branch.
Might be reason number 2 for disappearing channels.
BUGS: 228669

------------------------------------------------------------------------
r1126470 | scripty | 2010-05-14 14:03:38 +1200 (Fri, 14 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1126858 | scripty | 2010-05-15 14:01:39 +1200 (Sat, 15 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1128528 | esken | 2010-05-20 02:59:22 +1200 (Thu, 20 May 2010) | 3 lines

Fix menu bar hide problem. [backport to 4.4 branch]
CCBUGS: 191725

------------------------------------------------------------------------
