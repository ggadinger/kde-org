------------------------------------------------------------------------
r1124327 | rkcosta | 2010-05-09 09:13:46 +1200 (Sun, 09 May 2010) | 11 lines

Backport r1124326.

Escape file names in CliInterface when necessary.

The infrastructure has been added, and is used by the clizip plugin so
that it is now possible to extract and delete files which have
characters such as '[' or '*' correctly (infozip accepts some wild
characters to make our lives little bit more difficult).

BUG: 208091

------------------------------------------------------------------------
r1124370 | rkcosta | 2010-05-09 12:30:29 +1200 (Sun, 09 May 2010) | 10 lines

Backport r1124369.

Preserve paths when extracting for preview.

This prevents files with same names but different paths in the archive
from ending up being interpreted as the same by caches implemented by
some KParts, for example.

BUG: 231151

------------------------------------------------------------------------
r1124379 | rkcosta | 2010-05-09 13:38:10 +1200 (Sun, 09 May 2010) | 6 lines

Backport r1124377.

Do not preserve paths when extracting via drag'n'drop.

BUG: 208384

------------------------------------------------------------------------
r1124523 | dakon | 2010-05-09 21:15:31 +1200 (Sun, 09 May 2010) | 6 lines

fix corrupting list when trying to sign keys

If the private key selection was cancelled stale references would be left in the list of keys to sign. It was never checked if a key signing operation is already running so that list could get corrupted, too.

BUG:235913

------------------------------------------------------------------------
r1125149 | dakon | 2010-05-11 08:05:17 +1200 (Tue, 11 May 2010) | 5 lines

do not show any message if user cancels passphrase changing

BUG:235586
FIXED-IN:4.4.4

------------------------------------------------------------------------
r1125164 | dakon | 2010-05-11 08:27:29 +1200 (Tue, 11 May 2010) | 4 lines

make sure a GPGProc reacts to it's underlying KProcess signals only once

Backport of r1125160

------------------------------------------------------------------------
r1127071 | lueck | 2010-05-16 01:19:30 +1200 (Sun, 16 May 2010) | 1 line

make Button translatable
------------------------------------------------------------------------
r1127275 | lueck | 2010-05-16 18:42:17 +1200 (Sun, 16 May 2010) | 1 line

add missing i18n()
------------------------------------------------------------------------
r1127587 | scripty | 2010-05-17 14:17:29 +1200 (Mon, 17 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1128702 | rkcosta | 2010-05-20 13:42:31 +1200 (Thu, 20 May 2010) | 12 lines

Backport r1128701.

Change the way 7z's listing output is parsed.

7z first lists some information about the archive itself before showing
information about the entries in it, and the code did not take that into
account.

We now make that distinction, so that parsing entries after an entry is
added, for example, works as expected.

BUG: 238044
------------------------------------------------------------------------
r1128706 | rkcosta | 2010-05-20 13:53:49 +1200 (Thu, 20 May 2010) | 4 lines

Backport r1128705.

Const'ify readListLine whenever possible and wrap at 80 columns.

------------------------------------------------------------------------
r1129642 | rkcosta | 2010-05-23 17:43:07 +1200 (Sun, 23 May 2010) | 10 lines

Backport 1129641.

Revert 1124379.

All plugins but clizip support RootNode, and PreservePaths is needed in
order for dragging and dropping a directory to create the proper
structure.

CCBUG: 208384

------------------------------------------------------------------------
r1129768 | dakon | 2010-05-24 05:52:16 +1200 (Mon, 24 May 2010) | 1 line

remove completely useless include
------------------------------------------------------------------------
r1129769 | dakon | 2010-05-24 05:53:27 +1200 (Mon, 24 May 2010) | 4 lines

don't crash if the first time assistant tells us a key id we currently do not have loaded

BUG:238517

------------------------------------------------------------------------
r1130690 | scripty | 2010-05-26 14:10:11 +1200 (Wed, 26 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
