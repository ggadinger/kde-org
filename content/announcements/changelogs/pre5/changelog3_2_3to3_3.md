---
aliases:
- ../changelog3_2_3to3_3
hidden: true
title: KDE 3.2.3 to KDE 3.3.0 Changelog
---

<p>
This page tries to present as much as possible the additions
and corrections that occured in KDE between the 3.2.3 and 3.3.0 releases.
</p>

<a name="arts"><h3>arts</h3>

<ul>
</ul>

<a name="kdelibs"><h3>kdelibs</h3>

<ul>
	<ul>
		<li>Rewrite the name-resolution routines so that true
      asynchronous lookups are possible (multithreaded). <em>Thiago Macieira</em></li>
		<li>kconf_update has now support for updating files other than config files. <em>Waldo Bastian &lt;&#098;&#97;s&#x74;&#105;an&#00064;kde&#x2e;&#111;rg&gt;</em></li>
		<li>Crypto certificates import without user interaction (#82531) <em>Helge Deller &lt;&#00100;e&#108;ler&#x40;&#x6b;de&#00046;&#x6f;&#x72;&#x67;&gt;</em></li>
		<li>KEditToolbar: Add support for arranging actions via
drag&amp;drop. <em>Sandro Giessl &lt;sandro@giessl.com&gt;</em></li>
		<li>Move KNewStuff to kdelibs, making it usable for KDE-Edu
etc. <em>Josef Spillner &lt;sp&#105;l&#x6c;ner&#64;kd&#101;.org&gt;</em></li>
		<li>Introduce KIMIface, a generic DCOP interface to instant
messaging/chat clients, and support libraries for other KDE apps, enabling
loosely coupled integration. <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
		<li>Show actions icons in the configure shortcut dialog. <em>Olivier Goffart &lt;ogoffart @ tisaclinet.be&gt;</em></li>
		<li>KKeyDialog: add widget (based on klistviewsearchline) for
searching shortcuts <em>Cristian Tibirna &lt;tibirna @ kde.org&gt;</em></li>
		<li>User visible feedback if invocation of browser, help Center or
mailer fails </li>
	<li><h4>KImgIO</h4>
	<ul>
		<li>Add EXR image plugin, supports reading high dynamic
           range files. Resolves <a href="http://bugs.kde.org/show_bug.cgi?id=58170">bug 58170</a>. <em>Brad Hards &lt;bradh@frogmouth.net&gt;</em></li>
		<li>Add support for SGI images (a.k.a. &quot;RGB&quot; images). These do
typically have file extensions *.rgb, *.rgba, *.sgi, or *.bw. The SGI image file
format is a de-facto standard for textures, for example in 3D
modeling. <em>Melchior Franz &lt;mf&#x72;&#x61;nz&#64;&#107;d&#0101;&#x2e;o&#114;g&gt;</em></li>
		<li>Add read support for DDS images (Direct Draw Surface). This
format is mainly used for textures in DirectX. <em>Ignacio Castaño &lt;castano at ludicon dot com&gt;</em></li>
	</ul>

    </li>
    <li><h4>libkabc</h4>
    <ul>
    	<li>Write support and SSL/TLS for the LDAP resource <em>Szombathelyi György &lt;gyurco@freemail.hu&gt;</em></li>
    </ul>

    </li>
    <li><h4>KLocale</h4>
    <ul>
    	<li>Try harder to find message translations if a string is not

translated in the primary language of the user, but available in other languages
that the user has chosen in kcontrol. <em>Heiko Evermann &lt;heiko.evermann@gmx.de&gt;</em></li>

</ul>

    </li>
    <li><h4>KSpell2</h4>
    <ul>
    	<li>Introduction of the new spellchecking library fixing all of
        KSpell shortcomings. <em>Zack Rusin &lt;&#00122;a&#x63;k&#x40;k&#x64;&#0101;&#46;o&#114;g&gt;</em></li>
    </ul>

    </li>
    <li><h4>KHTML part</h4>
    <ul>
    	<li>KHTML: Merge text shifting from WebCore.
           <em>Leo Savernik &lt;l.savernik at aon dot at&gt;</em></li>
    	<li>KHTML: Type-ahead find <em>Arend van Beelen jr. &lt;arend@auton.nl&gt;</em></li>
    	<li>Copy To-&gt;IM Contact... context menu entry.  Using

kimproxy,
allow one to initiate a file transfer from Konqueror. <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>

<li>Added &quot;Open in This Window&quot; in popup frame submenu </li>
<li>New context menu when text is selected which allows lookup in
default search engine </li>
<li>Fixed and improved Access Key support: now activated by
pressing and releasing the ctrl key. When activated, the accesskey
mode displays all accesskeys available in tooltips <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
</ul>

    </li>
    </ul>

</ul>

<a name="kdeaccessibility"><h3>kdeaccessibility</h3>

<ul>
	<ul>
	<li><h4>KMagnifer</h4>
	<ul>
		<li>Make selection window resizable and moveable <em>Olaf Schmidt &lt;&#x6f;j&#115;chm&#x69;d&#116;&#64;k&#100;e&#46;&#111;r&#103;&gt;</em></li>
		<li>Show sliders if magnificated content does not fit the window <em>Olaf Schmidt &lt;&#x6f;j&#x73;&#0099;&#104;&#109;&#105;&#100;t&#x40;&#107;&#100;&#x65;&#x2e;o&#0114;g&gt;</em></li>
		<li>Magnification modes <em>Olaf Schmidt &lt;ojschmi&#100;t&#0064;&#107;d&#0101;&#046;o&#x72;g&gt;</em></li>
		<li>Allow rotation <em>Olaf Schmidt &lt;ojsc&#x68;&#x6d;&#x69;dt&#064;&#107;d&#x65;&#x2e;or&#103;&gt;</em></li>
	</ul>

    </li>
    <li><h4>KMouth</h4>
    <ul>
    	<li>Update the documentation <em>Gunnar Schmi Dt &lt;gunnar@schmi-dt.de&gt;</em></li>
    </ul>

    </li>
    </ul>

</ul>

<a name="kdeaddons"><h3>kdeaddons</h3>

<ul>
<h4>Addons</h4>
	<ul>
	<li><h4>FSView Part</h4>
	<ul>
		<li>Reduced memory requirements for large directory trees <em>Josef Weidendorfer &lt;Josef.Weidendorfer@gmx.de&gt;</em></li>
	</ul>

    </li>
    <li><h4>Sidebar Plugins</h4>
    <ul>
    	<li><strong>NEW IN KDE:</strong> Newsticker sidebar applet
          for viewing RSS feeds <em>Marcus Camen &lt;mcamen@mcamen.de&gt;</em></li>
    </ul>

    </li>
    <li><h4>Konqueror Plugins</h4>
    <ul>
    	<li><strong>NEW IN KDE:</strong>Plugin for accessing &lt;link&gt;

elements
(document relations) <em>Franck Quelain &lt;shift@free.fr&gt;</em></li>

<li><strong>NEW IN KDE:</strong>Search Bar Plugin. Provides quick
access to Find In This Page, Google and other search
engines. <em>Arend van Beelen jr. &lt;arend@auton.nl&gt;</em></li>
</ul>

    </li>
    <li><h4>Kate Plugins</h4>
    <ul>
    	<li>Kate Tab Bar extension <em>Dominik Haumann &lt;dhdev@gmx.de&gt;</em></li>
    </ul>

    </li>
    <li><h4>KFilePlugin</h4>
    <ul>
    	<li>Meta Info for Windows .lnk files (where does it point to) and

tool (lnkforward) to follow this &quot;link&quot; <em>Martin Koller &lt;m.koller@surfeu.at&gt;</em></li>

</ul>

    </li>
    <li><h4>Kuick</h4>
    <ul>
    	<li>Added option to disable plugin in context menus </li>
    </ul>

    </li>
    </ul>

<h4>Artwork</h4>
	<ul>
	<li><h4>Screen Savers</h4>
	<ul>
		<li>Enhance Euphoria and Banner screensavers to run smoothly and
look nicer. <em>Enrico Ros &lt;eros.kde@email.it&gt;</em></li>
	</ul>

    </li>
    </ul>

</ul>

<a name="kdeadmin"><h3>kdeadmin</h3>

<ul>
	<ul>
	<li><h4>KUser</h4>
	<ul>
		<li>User account administration in a LDAP database <em>Szombathelyi György &lt;gyurco@freemail.hu&gt;</em></li>
		<li>Samba account administration in a ldapsam backend <em>Szombathelyi György &lt;gyurco@freemail.hu&gt;</em></li>
		<li>MD5 Shadow password support <em>Szombathelyi György &lt;gyurco@freemail.hu&gt;</em></li>
	</ul>

    </li>
    </ul>

</ul>

<a name="kdeartwork"><h3>kdeartwork</h3>

<ul>
</ul>

<a name="kdebase"><h3>kdebase</h3>

<ul>
	<ul>
	<li><h4>KControl</h4>
	<ul>
		<li>Add a brand new control center module to globally handle KDE
visual themes. <em>Lukas Tinkl &lt;&#x6c;&#x75;ka&#x73;&#x40;&#x6b;d&#x65;&#46;o&#114;g&gt;</em></li>
		<li>Add new control center module to test and calibrate
joysticks <em>Martin Koller &lt;m.koller@surfeu.at&gt;</em></li>
	</ul>

    </li>
    <li><h4>Kicker</h4>
    <ul>
    	<li>Cleaner look for the mini-pager providing for more

contrast. <em>Sandro Giessl &lt;sandro@giessl.com&gt;</em></li>

</ul>

    </li>
    <li><h4>KDM</h4>
    <ul>
    	<li>Added &quot;Lock session&quot; checkbox to &quot;Start New Session&quot;

confirmation dialog </li>

</ul>

    </li>
    <li><h4>Konqueror</h4>
    <ul>
    	<li>Let configure shortcuts of plugins actions. <em>Olivier Goffart &lt;ogoffart @ tiscalinet.be&gt;</em></li>
    	<li>Improve the bookmark sidebar module (Context menus,

drag&amp;drop) <em>Sandro Giessl &lt;sandro@giessl.com&gt;</em></li>

<li>Ask for confirmation when loading a profile and tabs are open
(#56019) <em>Stephan Binner &lt;bi&#110;&#x6e;er&#00064;kd&#101;.&#x6f;&#114;g&gt;</em></li>
<li>Ask for confirmation when closing a webpage with modified form
content (#65758) <em>Stephan Binner &lt;b&#x69;&#110;&#110;&#x65;r&#x40;kde.o&#0114;g&gt;</em></li>
<li>Added option to hide &quot;Delete&quot; command on desktop and in file
manager </li>
<li>Confirm closing many pages with &quot;Close Other Tabs&quot;
command </li>
<li>Keybindings for switching to specific tab </li>
<li>Double click over empty tabbar space opens new tab </li>
<li>Mouse wheel over tabbar area switches tabs </li>
<li>Added option to activate previous active tab if closing the
current one </li>
<li>kfmclient: Added command &quot;newTab &#039;url&#039; [&#039;mimetype&#039;]&quot; </li>
<li>Hidden options MaximumTabLength and MinimumTabLength (defaults
to 3) </li>
<li>Count of files and folders on Folder Properties dialog
box </li>
</ul>

    </li>
    <li><h4>KDialog</h4>
    <ul>
    	<li>a dialog for showing progress bars would be useful

(#66342) <em>Stephan Binner &lt;b&#105;&#x6e;&#x6e;&#x65;&#114;&#x40;&#x6b;&#100;&#101;&#x2e;o&#x72;&#103;&gt;</em></li>

</ul>

    </li>
    <li><h4>KWin</h4>
    <ul>
    	<li>Per window settings and improved &quot;Store window settings&quot;

(#36377,#15329,#75433) <em>Lubos Lunak &lt;l.l&#00117;nak&#64;&#107;&#100;&#101;&#0046;o&#00114;g&gt;</em></li>

<li>Decoration API support for more buttons in titlebars
(#60369) <em>Sandro Giessl &lt;sandro@giessl.com&gt;</em></li>
</ul>

    </li>
    <li><h4>KDCOP</h4>
    <ul>
    	<li>More types supported for arguments (QSize, QPoint, QRect,

QColor, QPixmap) <em>Olivier Goffart &lt;ogoffart @ tiscalinet.be&gt;</em></li>

<li>Show applications icons. <em>Olivier Goffart &lt;ogoffart @ tiscalinet.be&gt;</em></li>
</ul>

    </li>
    <li><h4>Konsole</h4>
    <ul>
    	<li>Tabbar button for closing tabs <em>Kurt V. Hindenburg</em></li>
    </ul>

    </li>
    <li><h4>LDAP ioslave</h4>
    <ul>
    	<li>Writing support, SASL authentication and SSL/TLS support
         <em>Szombathelyi György &lt;gyurco@freemail.hu&gt;</em></li>
    </ul>

    </li>
    </ul>

</ul>

<a name="kdebindings"><h3>kdebindings</h3>

<ul>
	<ul>
	<li><h4>Python</h4>
	<ul>
		<li>Import and maintain a copy of the Python bindings, PyQt and
PyKDE. <em>Simon Edwards &lt;simon@simonzone.com&gt;</em></li>
	</ul>

    </li>
    </ul>

</ul>
	<ul>
	<li><h4>KJSEmbed</h4>
	<ul>
		<li>Add support for creating system tray icons from JS <em>Richard Moore &lt;&#114;&#105;&#99;h&#00064;kde.or&#0103;&gt;</em></li>
		<li>Restructure the source tree to make things more
organised <em>Richard Moore &lt;ric&#104;&#x40;&#107;&#00100;&#0101;&#46;o&#00114;&#103;&gt;</em></li>
		<li>Add support for the KPart content streaming API <em>Richard Moore &lt;&#x72;&#105;c&#x68;&#64;&#0107;de.or&#103;&gt;</em></li>
	</ul>

    </li>

</ul>

<a name="kdeedu"><h3>kdeedu</h3>

<ul>
<li><h4>KBruch</h4>
<ul>
<li>port to KConfig XT <em>Sebastian Stein &lt;kbruch@hpfsc.de&gt;</em></li>
<li>task displaying reworked <em>Sebastian Stein &lt;kbruch@hpfsc.de&gt;</em></li>
<li>make task display configurable <em>Sebastian Stein &lt;kbruch@hpfsc.de&gt;</em></li>
<li>new exercise: compare 2 ratios <em>Sebastian Stein &lt;kbruch@hpfsc.de&gt;</em></li>
<li>new exercise: convert rational number to fraction <em>Sebastian Stein &lt;kbruch@hpfsc.de&gt;</em></li>
<li>new exercise: factorize a given number into its prime factors</li>
<li>config dialog: set color and font for task display</li>
<li>What's this help and tooltips added</li>
</ul>
</li>

    </li>
    <li><h4>Kig</h4>
    <ul>
    	<li>Undo Support for Changing Color and Size of
        Objects <em>Dominique Devriese &lt;&#100;&#101;v&#0114;i&#00101;&#115;&#101;&#64;&#107;d&#101;&#046;o&#114;&#103;&gt;</em></li>
    	<li>DrGeo import filter <em>Dominique Devriese &lt;dev&#x72;ie&#115;&#0101;&#64;kd&#x65;&#46;o&#x72;&#0103;&gt;</em></li>
    	<li>Line and Point styles <em>Dominique Devriese &lt;&#x64;ev&#114;ie&#x73;e&#64;k&#100;e.&#x6f;&#114;&#0103;&gt;</em></li>
    	<li>Property tests <em>Dominique Devriese &lt;&#x64;&#101;vri&#101;&#x73;e&#x40;k&#x64;&#x65;.&#111;r&#103;&gt;</em></li>
    	<li>Better Python error reporting <em>Dominique Devriese &lt;d&#101;v&#00114;i&#101;se&#x40;k&#x64;e.&#111;&#x72;&#0103;&gt;</em></li>
    	<li>Add a system to name objects <em>Dominique Devriese &lt;d&#101;vr&#105;e&#115;&#00101;&#64;&#0107;d&#0101;&#46;&#111;&#x72;&#103;&gt;</em></li>
    	<li>Add some command line functions <em>Dominique Devriese &lt;&#100;&#101;v&#114;&#105;ese&#064;kde.&#00111;rg&gt;</em></li>
    	<li>When selecting an argument, tell the user what he&#039;s
        looking for. <em>Dominique Devriese &lt;&#0100;&#101;v&#0114;i&#x65;s&#0101;&#064;kd&#101;&#00046;o&#00114;g&gt;</em></li>
    	<li>When selecting an argument, tell the user if the
        object he wants to construct needs arguments that are not
        available. <em>Dominique Devriese &lt;d&#0101;vri&#x65;s&#101;&#0064;&#107;&#100;e&#x2e;&#111;&#0114;g&gt;</em></li>
    	<li>Various small stuff: attach labels to angles, some
        new objects like a test if two vectors are equal, some new
        transformations, intersection between some remaining objects
        etc. <em>Dominique Devriese &lt;devriese&#x40;kd&#x65;.org&gt;</em></li>
    </ul>

    </li>
    <li><h4>KHangMan</h4>
    <ul>
    	<li>Use KConfig XT instead of KConfig <em>Anne-Marie Mahfouf &lt;a&#110;nma&#00064;&#107;de.o&#x72;&#x67;&gt;</em></li>
    	<li>Add a config dialog if more settings in the Settings

dialog <em>Anne-Marie Mahfouf &lt;a&#0110;&#110;&#109;a&#64;kd&#0101;&#0046;or&#x67;&gt;</em></li>

<li>Put some data in the corresponding i18n modules and allow
KHangMan to download new languages easily using KNewStuff. Leave en in KHangMan
plus the language the user has if available in the i18n module. <em>Anne-Marie Mahfouf &lt;&#97;&#x6e;n&#109;a&#x40;k&#x64;&#x65;&#0046;o&#114;&#x67;&gt;</em></li>
</ul>

    </li>
    <li><h4>KLettres</h4>
    <ul>
    	<li>Put some data in the corresponding i18n modules and allow

KLettres to download new languages easily using KNewStuff. Leave fr (and en when
the sounds are done) as the 2 data languages in KLettres plus the language the
user has if available in the i18n module. <em>Anne-Marie Mahfouf &lt;a&#110;n&#109;a&#064;&#x6b;&#x64;e.&#0111;r&#103;&gt;</em></li>

<li>Make all sounds on ogg format as some distros don&#039;t play
mp3s <em>Anne-Marie Mahfouf &lt;a&#110;n&#109;&#97;&#64;k&#100;&#x65;&#x2e;&#111;&#x72;&#103;&gt;</em></li>
<li>Timer should be configurable either in Settings or in
kid/grown-up modes <em>Anne-Marie Mahfouf &lt;a&#110;n&#x6d;&#97;&#00064;&#107;d&#0101;&#x2e;org&gt;</em></li>
</ul>

    </li>
    <li><h4>Kalzium</h4>
    <ul>
    	<li>Rewriting most of the kalziums code <em>Carsten Niehaus &lt;c&#x6e;&#x69;ehau&#115;&#0064;&#107;de.&#111;&#00114;&#x67;&gt;</em></li>
    	<li>Make it possible to choose the unit of some

datasets. For example it is now possible to choose between Fahrenheit, Degree
Celsius and Kelvin <em>Carsten Niehaus &lt;cn&#105;ehau&#115;&#64;kd&#101;&#046;&#111;&#114;&#x67;&gt;</em></li>

<li>Offer two periodic tables: the simplified for
younger pupil and the full PSE will all 110 elements <em>Carsten Niehaus &lt;c&#110;i&#x65;h&#00097;&#117;&#115;&#00064;&#0107;&#00100;&#101;&#46;org&gt;</em></li>
<li>Rewritten plotting with more options <em>Carsten Niehaus &lt;c&#0110;ie&#x68;&#097;&#117;&#115;&#00064;k&#00100;&#00101;.&#111;r&#x67;&gt;</em></li>
<li>Update doc <em>Anne-Marie Mahfouf &lt;&#097;&#00110;nm&#97;&#0064;k&#100;&#101;&#x2e;org&gt;</em></li>
</ul>

    </li>
    <li><h4>KStars</h4>
    <ul>
    	<li>Add ability to manually step clock once per keypress <em>Jason Harris</em></li>
    	<li>Add proper motion of stars <em>Jason Harris</em></li>
    	<li>Add text labels to equator, ecliptic, and horizon

lines <em>Jason Harris</em></li>

<li>Tool to measure angular distance between two points. <em>Pablo de Vicente</em></li>
<li>Add Equinox and Solstice calculator module <em>Pablo de Vicente</em></li>
<li>Add Planet Positions calculator module <em>Pablo de Vicente</em></li>
<li>Add ecliptic to/from equatorial conversion calculator
module <em>Pablo de Vicente</em></li>
<li>Add more data to the details window: hour angle, airmass,
distance from Earth/Sun, angular size, multiple/variable stars <em>Jason Harris</em></li>
<li>Add airmass to details window. <em>Jason Harris</em></li>
<li>Use KXConfig to handle options <em>Jason Harris</em></li>
<li>Add popup-menu links to Wikipedia articles on specific
objects. <em>Jason Harris</em></li>
<li>Transient object label when mouse hovers on object (need to
make it work when clock is stopped) <em>Jason Harris</em></li>
<li>Semi-transparent or opaque background option for on-screen info
boxes <em>Jason Harris</em></li>
<li>Add Constellation Boundaries to the map <em>Jason Harris</em></li>
<li>Define Constellation lines with star names, rather than
independent points on the sky <em>Jason Harris</em></li>
<li>Display Altitude/Azimuth in status bar, along with
RA/Dec <em>Jason Harris</em></li>
<li>Make the &quot;Night Vision&quot; color scheme adjust the KStars window
colors as well <em>Jason Harris</em></li>
<li>Add true Full-screen mode <em>Heiko Evermann</em></li>
<li>Add support for CCD cameras <em>Jasem Mutlaq</em></li>
<li>Add FITSViewer Tool, which includes many image reduction and
analysis tools <em>Jasem Mutlaq</em></li>
<li>ExtDate, KStarsDateTime classes for handling dates outside of
QDate range <em>Jason Harris</em></li>
<li>Batch mode in remaining astronomial calculator
modules <em>Pablo de Vicente</em></li>
<li>KStars startup wizard with integrated download of &#039;extra&#039;
data <em>Jason Harris</em></li>
<li>Add support for &quot;Video4Linux&quot; devices and webcams <em>Jasem Mutlaq</em></li>
<li>DCOP functions for adjusting colors <em>Jason Harris</em></li>
<li>DCOP function for exporting or printing image <em>Jason Harris</em></li>
<li>Add new DCOP functions to ScriptBuilder tool <em>Jason Harris</em></li>
<li>Add Moon&#039;s illumination fraction to popup menu/details
window <em>Jason Harris</em></li>
<li>Add option to dump mode for specifying time/date <em>Jason Harris</em></li>
<li>ImageViewer: Add label with optional text describing image or
crediting its author. <em>Jason Harris</em></li>
<li>Calculator module to compute angular distance between two
arbitrary sources. <em>Pablo de Vicente</em></li>
</ul>

    </li>
    <li><h4>KmPlot</h4>
    <ul>
    	<li>Ability to draw numeric integrals with Euler&#039;s

method. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>

<li>Avoid unnecessary repainting. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Ability to select background color. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Automatic scaling. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Zoom mode. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Save version number so we can support loading old Kmplot
files. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Popup menu for plot-graphs when clicking on the right mouse
button <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Tool menu: area between plot and x-axis. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>export to BMP, PNG, and SVG format <em>Klaus-Dieter MÃ¶ller &lt;kd.moeller@t-online.de&gt;</em></li>
<li>Tool menu: maximum, minimum, y value of a given x
value. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Changing the parameter a function depends on by a
QSlider. <em>Matthias Meßmer &lt;bmlmessmer@web.de&gt;</em></li>
<li>Navigate by keyboard in trace mode. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Port the configuration dialogs to KConfig XT. <em>Matthias Meßmer &lt;bmlmessmer@web.de&gt;</em></li>
<li>Redesign of the function edit dialog: handling the extensions
by dialog widgets. <em>Matthias Meßmer &lt;bmlmessmer@web.de&gt;</em></li>
<li>Recently opened files. <em>Matthias Meßmer &lt;bmlmessmer@web.de&gt;</em></li>
<li>User defined constants: Editor and import/export from/to
KCalc. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Progress bar and cancel button for plots taking much time
(status bar, see KMail). <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Localization of the decimal seperator. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Angle modes: radian (default) and degree. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Custom color and line width for each derivatives and
anti-derivatives. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
<li>Function editor dialog: values of parameters a function depends
on can be importet from a text file. <em>Fredrik Edemar &lt;f_edemar@linux.se&gt;</em></li>
</ul>

    </li>
    <li><h4>KWordQuiz</h4>
    <ul>
    	<li><strong>NEW IN KDE:</strong> KWordQuiz is the KDE version of

the flashcard and vocabulary learning program WordQuiz. KWordQuiz is still in
early development.

          KWordQuiz supports opening, editing, and saving of vocabulary files

(format kvtml). It allows Flashcard mode, Multiple Choice and Question &amp;
Answer through a friendly Quiz mode.
<em>Peter Hedlund &lt;peter@peterandlinda.com&gt;</em></li>

<li>A context menu for the editor <em>Peter Hedlund &lt;peter@peterandlinda.com&gt;</em></li>
<li>Joining multiple files <em>Peter Hedlund &lt;peter@peterandlinda.com&gt;</em></li>
<li>Tooltips <em>Peter Hedlund &lt;peter@peterandlinda.com&gt;</em></li>
</ul>

    </li>
    <li><h4>KLatin</h4>
    <ul>
    	<li><strong>NEW IN KDE:</strong> KLatin is a program to help revise

Latin. You can be tested on vocabulary, grammar and verb and the doc includes a
revision section. <em>George Wright &lt;&#x67;&#x77;&#x72;ight&#00064;kde&#x2e;org&gt;</em></li>

<li>Tooltips and QWhatsThis help <em>George Wright &lt;&#00103;wrig&#104;&#x74;&#64;&#x6b;&#00100;&#101;&#46;org&gt;</em></li>
<li>Add doxygen comments to headers <em>George Wright &lt;&#00103;&#119;right&#064;k&#x64;&#101;.or&#x67;&gt;</em></li>
</ul>

    </li>
    <li><h4>KTurtle</h4>
    <ul>
    	<li><strong>NEW IN KDE:</strong> KTurtle is an educational

programming environment using the Logo programming language. This makes KTurtle
suitable for teaching kids the basics of math, geometry and... programming. The
commands used to program are in the style of the Logo programming language. The
unique feature of Logo is that the commands are often translated into the
speaking language of the programmer.

          KTurtle features an integrated Logo interpreter (no need to download

any other program), a powerful editor for the Logo commands with intuitive
syntax highlighting, line numbering, etc, context help for all Logo commands...
<em>Cies Breijs &lt;cies@showroommama.nl&gt;</em></li>

</ul>

    </li>
    </ul>

<a name="kdegames"><h3>kdegames</h3>

<ul>
	<li><h4>Atlantik</h4>
	<ul>
		<li>KDE User Interface standards: proper use of KStatusBar.</li>
		<li>Implemented wish <a href="http://bugs.kde.org/show_bug.cgi?id=52631">#52631</a>: during game configuration, the master can boot players to the lounge.</li>
		<li>Implemented wish <a href="http://bugs.kde.org/show_bug.cgi?id=61858">#61858</a>: property names are now displayed on the game board.</li>
		<li>Event log for analyzing the monopd XML protocol.</li>
	</ul>
	</li>

    <li><h4>KLines</h4>
    <ul>
    	<li>Made accessible by keyboard. <em>Waldo Bastian &lt;ba&#00115;tian&#0064;&#0107;d&#x65;.&#111;rg&gt;</em></li>
    </ul>

    </li>

    <li><h4>Konquest</h4>
    <ul>
    	<li>Face lift, reduced number of popup messages. <em>Waldo Bastian &lt;b&#97;&#x73;&#x74;ian&#00064;&#00107;&#100;&#101;.org&gt;</em></li>
    </ul>

    </li>

</ul>

<a name="kdegraphics"><h3>kdegraphics</h3>

<ul>
	<ul>
	<li><h4>KFilePlugins</h4>
	<ul>
		<li>exr: Display information for most common attributes in
available images. <em>Brad Hards &lt;bradh@frogmouth.net&gt;</em></li>
		<li>rgb: Display information for most common attributes of SGI
graphics. <em>Melchior Franz &lt;&#109;f&#x72;&#97;n&#122;&#64;kd&#101;&#46;org&gt;</em></li>
	</ul>

    </li>
    <li><h4>KDVI</h4>
    <ul>
    	<li>optional continuous page view, similar to acroread <em>Stefan Kebekus &lt;&#00107;&#101;b&#101;&#x6b;&#117;s&#064;&#x6b;d&#101;&#x2e;o&#114;&#x67;&gt;</em></li>
    </ul>

    </li>
    <li><h4>KPDF</h4>
    <ul>
    	<li>Added thumbnails to the page list <em>Albert Astals Cid &lt;tsdgeos@terra.es&gt;</em></li>
    </ul>

    </li>
    <li><h4>KPovModeler</h4>
    <ul>
    	<li>Added graphical editing to Mesh object <em>Leon Pennington &lt;leonscape@blueyonder.co.uk&gt;</em></li>
    	<li>New objects: New options for media, new patterns (cells, additional crackle options, julia,
    additional mandel options, and slope), added keywords no_image, no_reflection, and double_illuminate to
    graphical objects <em>Leon Pennington &lt;leonscape@blueyonder.co.uk&gt;</em></li>
    	<li>Added uv mapping and uv vectors <em>Leon Pennington &lt;leonscape@blueyonder.co.uk&gt;</em></li>
    </ul>

    </li>
    <li><h4>KolourPaint</h4>
    <ul>
    	<li><strong>NEW IN KDE:</strong> An easy-to-use paint program
          designed for everyday tasks like drawing simple diagrams/logos/icons
          and editing screenshots.

          Features include undo/redo, more than a dozen tools, selections,

transparent
image editing and zoom support (with an optional grid and thumbnail).
<em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>

<li>Add virtually unlimited Undo/Redo. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Add freehand resizing. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Add drag scrolling. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>More image effects: Balance, Emboss, Flatten, Invert (with choice of channels), Reduce Colours, Smooth Scale, Soften &amp; Sharpen. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>When loading images, dither more often for better quality. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Configurable colour depth and JPEG quality when saving (with preview). <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Single key shortcuts for all tools and tool options. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Fix several selection bugs. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Allow single click creation of text box. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Add Opaque/Transparent widget for the text tool. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Improve text quality when working with transparency. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Paste multiline clipboard contents with MMB correctly. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Improve Resize/Scale dialog usability. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Reduce flicker. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Add Export, Copy To File, Paste From File, Paste in New Window, Full Screen Mode. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Add Zoom In/Out buttons to main toolbar. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
<li>Make sure colour palette renders correctly on 8-bit screen. <em>Clarence Dang &lt;dang at kde dot org&gt;</em></li>
</ul>

    </li>
    </ul>

</ul>

<a name="kdemultimedia"><h3>kdemultimedia</h3>

<ul>
	<ul>
	<li><h4>aRts</h4>
	<ul>
		<li>JACK output plugin <em>Zack Rusin &lt;&#122;a&#99;k&#64;k&#x64;e&#46;o&#114;&#x67;&gt;, Matthias Kretz &lt;kretz@kde.org&gt;</em></li>
	</ul>

    </li>
    <li><h4>aKode</h4>
    <ul>
    	<li>akodelib: New multithreaded audio (de/en)coding

library <em>Allan Sandfeld Jensen &lt;kde@carewolf.com&gt;</em></li>

<li>akodelib: Plugins for MPEG-audio, Ogg/Vorbis, FLAC, Ogg/FLAC
and Musepack decoding <em>Allan Sandfeld Jensen &lt;kde@carewolf.com&gt;</em></li>
<li>akodelib: Support for external encoding and output
plugins <em>Allan Sandfeld Jensen &lt;kde@carewolf.com&gt;</em></li>
<li>akodelib_artsplugin: aRts-plugin that uses the akode-decoding
plugins <em>Allan Sandfeld Jensen &lt;kde@carewolf.com&gt;</em></li>
<li>akodelib_artsplugin: Support streaming aRts API, to
enable streaming playback <em>Allan Sandfeld Jensen &lt;kde@carewolf.com&gt;</em></li>
</ul>

    </li>
    <li><h4>KRec</h4>
    <ul>
    	<li>Update the documentation. <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
    </ul>

    </li>
    <li><h4>JuK</h4>
    <ul>
    	<li>Improved track announcement popup <em>Michael Pyne &lt;michael.pyne@kdemail.net&gt;</em></li>
    	<li>Add to K3B support for songs <em>Michael Pyne &lt;michael.pyne@kdemail.net&gt;</em></li>
    	<li>Improved DCOP support <em>Michael Pyne &lt;michael.pyne@kdemail.net&gt;</em></li>
    	<li>Drag and drop retagging of songs <em>Michael Pyne &lt;michael.pyne@kdemail.net&gt;</em></li>
    	<li>Quickly play songs from the Search Bar <em>Michael Pyne &lt;michael.pyne@kdemail.net&gt;</em></li>
    	<li>Album random play (#60108) <em>Michael Pyne &lt;michael.pyne@kdemail.net&gt;</em></li>
    </ul>

    </li>
    <li><h4>KFilePlugins</h4>
    <ul>
    	<li>New plugin for C64 PSID music files <em>Rolf Magnus &lt;&#114;&#x61;magn&#117;&#x73;&#64;&#107;de.org&gt;</em></li>
    </ul>

    </li>
    </ul>

</ul>

<a name="kdenetwork"><h3>kdenetwork</h3>

<ul>
	<ul>
	<li><h4>Kopete</h4>
	<ul>
		<li>Implement Jabber file transfers. <em>Till Gerken &lt;till@tantalo.net&gt;</em></li>
		<li>Add Jabber group chat support. <em>Till Gerken &lt;till@tantalo.net&gt;</em></li>
		<li>Complete Kopete&#039;s handling of external changes to IM data
                        stored in KABC - add new contacts if added in KABC and
rearrange metacontacts following the data in KABC, similarly, remove. <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
		<li>New connection API that supporting logging in as a
          different status than &quot;online&quot; <em>Matt Rogers &lt;matt.rogers@kdemail.net&gt;</em></li>
		<li>New disconnect API so we can tell when we&#039;ve been disconnected
          by the server and can then reconnect. <em>Matt Rogers &lt;matt.rogers@kdemail.net&gt;</em></li>
		<li>Latex render plugin <em>Duncan Mac-Vicar &lt;&#x64;u&#x6e;&#0099;a&#x6e;&#x40;k&#x64;&#x65;.org&gt;</em></li>
		<li>Add support for bold, italic and underlined messages to
          Yahoo <em>Matt Rogers &lt;matt.rogers@kdemail.net&gt;</em></li>
		<li>Add new mail notifications to the Yahoo! plugin <em>Matt Rogers &lt;matt.rogers@kdemail.net&gt;</em></li>
		<li>Add SSL Support in IRC <em>Jason Keirstead &lt;jason@keirstead.org&gt;</em></li>
		<li>Add the ability to associate custom KNotify event notifications
          with a metacontact (Buddy Pounce) <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
		<li>Add support for irc:// protocols in Konqueror <em>Jason Keirstead &lt;jason@keirstead.org&gt;</em></li>
		<li>Change the KopeteAwayAction to be more like Konqueror&#039;s
        Recent Documents <em>Jason Keirstead &lt;jason@keirstead.org&gt;</em></li>
		<li>Add an alias plugin <em>Jason Keirstead &lt;jason@keirstead.org&gt;</em></li>
		<li>Seperate the password handling from KopeteAccount <em>Richard Smith &lt;kde@metafoo.co.uk&gt;</em></li>
		<li>Support amaroK in Kopete&#039;s Now Listening plugin <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
		<li>Action to toggle encryption on/off in a chat. <em>Olivier Goffart &lt;ogoffart @ tiscalinet.be&gt;</em></li>
		<li>Implement KIMIface in Kopete to enable presence and messaging
            integration across the desktop. <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
		<li>Merge data acquired from Kopete&#039;s protocols to the KDE
            address book, e.g. names, email addresses and phone
            numbers. <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
		<li>Plugin to invite MSN contacts to uses gnomemeeting. <em>Olivier Goffart &lt;ogoffart @ tiscalinet.be&gt;</em></li>
		<li>&quot;Send Email...&quot; context menu entry. <em>Reuben Sutton &lt;reuben@microsucks.co.uk&gt;</em></li>
		<li>ICQ, support mimetype application/x-icq to add
            contacts <em>Stefan Gehn &lt;metz@gehn.net&gt;</em></li>
		<li>AIM, support aim: protocol to add contacts <em>Stefan Gehn &lt;metz@gehn.net&gt;</em></li>
		<li>ICQ, support for ignore-, invisible- and
            visible-list <em>Stefan Gehn &lt;metz@gehn.net&gt;</em></li>
		<li>MSN incoming File transfers trought the chat session as MSN Messenger
            6 does.<em>Olivier Goffart &lt;ogoffart @ tiscalinet.be&gt;</em></li>
	</ul>

    </li>
    <li><h4>Remote Desktop Connection (krdc)</h4>
    <ul>
    	<li>Rewrote the RDP client to use an external rdesktop process,

which includes support for RDP 5. Currently this
requires a patched rdesktop version to be installed. Future rdesktop
versions will have this support built-in. <em>Arend van Beelen jr. &lt;arend@auton.nl&gt;</em></li>

<li>Switch to enforce the local cursor. <em>Tim Jansen &lt;tim@tjansen.de&gt;</em></li>
</ul>

    <li><h4>KWiFiManager</h4>
    <ul>
    	<li>when multiple cards are in use, each instance shows information

for one card <em>Stefan Winter &lt;kde@stefan-winter.de&gt;</em></li>

<li>major code cleanup <em>Stefan Winter &lt;kde@stefan-winter.de&gt;</em></li>
<li>support for wireless scanning <em>Stefan Winter &lt;kde@stefan-winter.de&gt;</em></li>
</ul>

    </li>
    <li><h4>File Sharing</h4>
    <ul>
    	<li>Create an advanced fileshare Control Center module, based on

KSambaPlugin and KNFSPlugin <em>Jan Schaefer &lt;j_schaef@informatik.uni-kl.de&gt;</em></li>

<li>Create an advanced Konqueror properties dialog plugin, based on
KSambaPlugin and KNFSPlugin <em>Jan Schaefer &lt;j_schaef@informatik.uni-kl.de&gt;</em></li>
</ul>

    </li>
    </ul>

</ul>

<a name="kdepim"><h3>kdepim</h3></a>

<ul>
	<ul>
	<li><h4>Kontact</h4>
	<ul>
		<li>Kolab client integraton. <em>Bo Thorsen &lt;bo@sonofthor.dk&gt;</em></li>
		<li>KPilot integration into Kontact. <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;, Adriaan de Groot &lt;adridg@cs.kun.nl&gt;</em></li>
		<li>eGroupware connection. <em>Tobias Koenig &lt;t&#x6f;&#0107;&#111;&#0101;&#x40;&#x6b;&#100;&#x65;.o&#114;g&gt;</em></li>
		<li>Bug voting and feature purchasing help menu items. <em>Don Sanders &lt;s&#97;&#00110;d&#00101;rs&#64;k&#100;&#101;&#x2e;o&#00114;g&gt;</em></li>
	</ul>

    </li>
    <li><h4>KMail</h4>
    <ul>
    	<li>HTML mail composing. <em>Don Sanders &lt;s&#x61;nd&#x65;rs&#x40;&#x6b;&#100;e&#0046;org&gt;, Edwin Schepers &lt;yez@home.nl&gt;</em></li>
    	<li>List only open IMAP folders <em>Carsten Burghardt &lt;&#x62;urgh&#00097;&#x72;d&#116;&#064;&#x6b;d&#x65;.or&#103;&gt;</em></li>
    	<li>Quick search toolbar item <em>Don Sanders &lt;&#115;a&#110;&#x64;e&#114;s&#64;&#107;&#x64;e&#x2e;&#0111;r&#103;&gt;</em></li>
    	<li>Anti Spam Wizard <em>Andreas Gungl &lt;a.gungl@gmx.de&gt;</em></li>
    	<li>Anti Virus Wizard <em>Frederick Emmott &lt;fred87@users.sf.net&gt;</em></li>
    	<li>Filter Log Viewer <em>Andreas Gungl &lt;a.gungl@gmx.de&gt;</em></li>
    	<li>Per-contact crypto preferences <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;</em></li>
    	<li>Crypto plugins autoconfiguration <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;</em></li>
    	<li>Split sign and encryption keys <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;</em></li>
    	<li>Preset encryption/sign keys for OpenPGP and S/MIME in the identity <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;</em></li>
    	<li>Importing certificates from PKCS#7 certs-only attachments <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;</em></li>
    	<li>Add new (automatic) mailinglist handling functionality. <em>Zack Rusin &lt;&#x7a;ac&#0107;&#64;&#00107;&#100;&#x65;&#x2e;org&gt;</em></li>
    	<li>Add viewer plugins to allow e.g. KOrganizer to display
        invitations embedded in KMail. <em>Ingo Kloecker &lt;k&#108;&#111;eck&#x65;&#114;&#64;kde&#46;&#x6f;r&#103;&gt;</em></li>
    	<li>Renaming of Disconnected IMAP folders <em>Bo Thorsen &lt;bo@sonofthor.dk&gt;</em></li>
    	<li>New filters: &quot;is email in addressbook?&quot;, &quot;is email in one of the addressbook&#039;s categories?&quot;, &quot;has email an attachmen?&quot; <em>Martin Koebele &lt;martin@mkoebele.de&gt;, Ingo Kloecker &lt;kloecker@kde.org&gt;</em></li>
    	<li>Merge in presence display via KIMProxy <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
    	<li>Allow starting IM chats from an address&#039;s context menu <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
    	<li>IMAP ACL support (aka shared folders) <em>David Faure &lt;&#x66;&#x61;&#117;re&#64;kde&#46;or&#103;&gt;</em></li>
    	<li>Expiring old messages as a background task, optionally by moving them to another folder <em>David Faure &lt;&#102;&#0097;&#00117;re&#x40;&#x6b;&#100;e.or&#x67;&gt;</em></li>
    	<li>Compacting mbox/maildir folders as a background task <em>David Faure &lt;f&#97;&#0117;&#x72;&#101;&#0064;&#x6b;de&#x2e;&#x6f;rg&gt;</em></li>
    </ul>

    </li>
    <li><h4>Kleopatra (formerly known as KGpgCertManager)</h4>
    <ul>
    	<li>Importing/exporting certificates and secret keys <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;</em></li>
    	<li>Deleting certificates <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;</em></li>
    	<li>Async / cancel&#039;able operations <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;</em></li>
    	<li>Highlighting of revoked/expired/etc certificates in the certlist <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;</em></li>
    </ul>

    </li>
    <li><h4>KOrganizer</h4>
    <ul>
    	<li>Recurrence on todo&#039;s <em>Bram Schoenmakers &lt;bramschoenmakers@kde.nl&gt;</em></li>
    	<li>Free/Busy view <em>Cornelius Schumacher</em></li>
    	<li>Automatic upload of Free/Busy information to server <em>Cornelius Schumacher</em></li>
    	<li>Remote calendars (auto-update, change notification)
         <em>Cornelius Schumacher &lt;&#115;chum&#97;c&#00104;&#0101;r&#0064;&#0107;&#100;&#101;&#46;&#0111;rg&gt;</em></li>
    	<li>More flexible working hour configuration (minute-based, working days can be selected, overnight shifts are possible)
         <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Allow editing of more than one day in Journal (diary) view
         <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Holiday plugin uses the system&#039;s country as default
         <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Filters are also applied to todo lists, and to printing the todo list
         <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;, Bram Schoenmakers &lt;bramschoenmakers@kde.nl&gt;</em></li>
    	<li>&quot;Copy to&quot; date and &quot;Move to&quot; date popup in popup menu for todo items <em>Bram Schoenmakers &lt;bramschoenmakers@kde.nl&gt;</em></li>
    	<li>&quot;Edit&quot;-button in alarmdialog <em>Bram Schoenmakers &lt;bramschoenmakers@kde.nl&gt;</em></li>
    </ul>

    </li>
    <li><h4>KPilot</h4>
    <ul>
    	<li>Fix knotes conduit <em>Adriaan de Groot &lt;adridg@cs.kun.nl&gt;</em></li>
    	<li>KPilot integration into Kontact
         <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Configuration wizard (only three settings, handheld is auto-detected!)
         <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Allow syncing to remote files (calendar, todo, addressbook)
         <em>Joern Ahrens</em></li>
    	<li>Add a custom page to KAddressbook&#039;s contact editor to allow editing of the custom fields synced by the addressbook conduit
         <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    </ul>

    </li>
    <li><h4>KAlarm</h4>
    <ul>
    	<li>Add facility to define alarm templates <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add option to play sound file repeatedly until alarm window is closed <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add volume control for playing sound file <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add &#039;stop sound&#039; button to alarm message window when sound file is played <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add facility to execute shell commands before or after an alarm is displayed <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Middle mouse button over system tray icon displays new alarm dialog <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add option to display a reminder once only before the first alarm recurrence <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add facility to specify shell commands to execute before or after an alarm is displayed <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Display time-to-alarm in reminder message window <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>For message texts which are truncated in main window, show full text in tooltip <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Allow more flexible formats in time entry fields <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>After creating/editing alarm, prompt to re-enable alarms if currently disabled <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    </ul>

    </li>
    <li><h4>KArm</h4>
    <ul>
    	<li>Documentation <em>Mark Bucciarelli &lt;mark@hubcapconsulting.com&gt;</em></li>
    	<li>File locking <em>Mark Bucciarelli &lt;mark@hubcapconsulting.com&gt;</em></li>
    	<li>CSV Export of timecard history <em>Mark Bucciarelli &lt;mark@hubcapconsulting.com&gt;</em></li>
    </ul>

    </li>
    <li><h4>KNotes</h4>
    <ul>
    	<li>Network-enabled KNotes <em>Michael Brade &lt;b&#0114;a&#0100;&#x65;&#x40;k&#x64;e.&#111;r&#00103;&gt;</em></li>
    	<li>Implement rich text mode for the notes. <em>Michael Brade &lt;brad&#101;&#x40;&#00107;de.&#111;&#x72;&#0103;&gt;</em></li>
    </ul>

    </li>
    <li><h4>KitchenSync</h4>
    <ul>
    	<li>Finish framework. <em>Cornelius Schumacher &lt;sc&#104;&#x75;&#x6d;&#97;&#099;&#x68;e&#114;&#064;&#107;d&#x65;.&#x6f;&#0114;&#x67;&gt;</em></li>
    	<li>Syncing addressbook and calendar data between
        desktops. <em>Cornelius Schumacher &lt;&#00115;chuma&#x63;&#0104;&#x65;&#x72;&#x40;k&#100;&#0101;&#00046;o&#00114;&#00103;&gt;</em></li>
    </ul>

    </li>
    <li><h4>KNode</h4>
    <ul>
    	<li>Improved HTML rendering <em>Roberto Selbach Teixeira &lt;r&#x6f;&#98;ert&#111;&#64;kde.&#x6f;&#00114;g&gt;</em></li>
    	<li>Partial KMail look&#039;n&#039;feel <em>Roberto Selbach Teixeira &lt;&#114;&#x6f;be&#114;&#x74;o&#0064;k&#x64;e&#0046;o&#114;g&gt;</em></li>
    	<li>Show long group names as tooltips. <em>Roberto Teixeira &lt;&#114;ob&#x65;rt&#111;&#00064;&#0107;&#x64;e.o&#114;g&gt;</em></li>
    </ul>

    </li>
    <li><h4>KAddressBook</h4>
    <ul>
    	<li>Simple and advanced Custom Fields support <em>Tobias Koenig &lt;to&#107;oe&#x40;&#x6b;de&#046;&#00111;&#114;g&gt;</em></li>
    	<li>Field selection for vCard export plugin <em>Tobias Koenig &lt;&#116;okoe&#00064;kde.&#x6f;r&#x67;&gt;</em></li>
    	<li>GUI for per-contact crypto settings (see also KMail) <em>Marc Mutz &lt;marc@klaralvdalens-datakonsult.se&gt;, Steffen Hansen &lt;steffen@klaralvdalens-datakonsult.se&gt;</em></li>
    	<li>Make KAddressBook work fine with asynchronous interface of libkabc <em>Tobias Koenig &lt;&#00116;oko&#x65;&#x40;&#107;&#0100;&#101;.&#x6f;&#114;g&gt;</em></li>
    	<li>Polish up the GEO, PHOTO, LOGO and SOUND input widgets <em>Tobias Koenig &lt;&#x74;&#111;k&#00111;&#x65;&#064;&#x6b;&#100;e.o&#0114;g&gt;</em></li>
    	<li>Merge presence display in summary view <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
    	<li>Merge editor widget for multiple IM addresses <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
    	<li>Add progress bar to KAddressbook&#039;s Nokia mobile phone import/export filter <em>Helge Deller &lt;de&#0108;&#x6c;&#x65;&#114;&#x40;kde.org&gt;</em></li>
    	<li>Add custom page to allow editing of the custom fields synced by the addressbook conduit
         <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    </ul>

    </li>
    <li><h4>KonsoleKalendar</h4>
    <ul>
    	<li>Fix exporting recurring events <em>Allen Winter &lt;winterz@verizon.net&gt;</em></li>
    	<li>Handle timezones and localization <em>Allen Winter &lt;winterz@verizon.net&gt;</em></li>
    	<li>New --next command line argument for showing next event <em>Tuukka Pasanen &lt;illuusio@mailcity.com&gt;</em></li>
    	<li>New --show-next command line argument for showing next so many days events <em>Tuukka Pasanen &lt;illuusio@mailcity.com&gt;</em></li>
    	<li>Add a new compact, easy to read, export format <em>Allen Winter &lt;winterz@verizon.net&gt;</em></li>
    </ul>

    </li>
    <li><h4>libkdepim</h4>
    <ul>
    	<li>Add a renamed KIMProxy so that we can use its services and retain compatibility with 3.2.  KIMProxy code has been merged with libkdepim. <em>Will Stephenson &lt;lists@stevello.free-online.co.uk&gt;</em></li>
    </ul>

    </li>
    </ul>

</ul>

<a name="kdesdk"><h3>kdesdk</h3>

<ul>
	<li><h4>Umbrello</h4>
	<ul>
		<li>Improve icons <em>BARTKO, Zoltan &lt;bartko.zoltan@pobox.sk&gt;</em></li>
		<li>XMI standard compliance and import of foreign XMI
files <em>Oliver Kellogg &lt;okellogg@users.sourceforge.net&gt;</em></li>
		<li>clean up setModified() handling <em>Sebastian Stein &lt;seb.kde@hpfsc.de&gt;</em></li>
		<li>alignment tools added <em>Sebastian Stein &lt;seb.kde@hpfsc.de&gt;</em></li>
		<li>support compressed (*.xmi.tgz, *.xmi.tar.bz2) XMI
files <em>Sebastian Stein &lt;seb.kde@hpfsc.de&gt;</em></li>
		<li>heavily reduced memory usage and CPU load on large projects</li>
		<li>reserved keywords added for supported languages</li>
		<li>new tools for aligning several objects</li>
		<li>support added for compressed XMI files (*.xmi.tgz, *.xmi.tar.bz2)</li>
		<li>New diagram command 'Duplicate' permits the copying of diagram objects
including their features</li>
		<li>New association type: Containment (circle-plus.)</li>
		<li>Operation parameters have a 'direction' which documents whether they
are input or return values (in, out, inout.) The IDL and Ada generators use this
in their generated code.</li>
		<li>Ability to show only public methods/members in diagram.</li>
		<li>Improvements to scaled printing.</li>
		<li>Crisp new icons</li>
		<li>Improved XMI standard conformance of the file format.  In principle,
Umbrello's XMI parser is now capable of reading foreign XMI files.</li>
		<li>Improved compatibility with old umbrello files</li>
		<li>Support for repeatedly importing the same C++ file(s)</li>
		<li>Umbrello places much less demand on the X server when dealing with
large and complex diagrams</li>
		<li>Umbrello still compiles on KDE 3.1 but will not support the new
compressed-XMI file format in this case</li>
		<li>88 bugs fixed</li>
	</ul>
	</li>

    <li><h4>Cervisia</h4>
    <ul>
    	<li>&quot;Create repository&quot; (cvs init) function <em>Christian Loose &lt;christian.loose@kdemail.net&gt;</em></li>
    	<li>Retrieve cvsignore file from server <em>Christian Loose &lt;christian.loose@kdemail.net&gt;</em></li>
    	<li>Add posibility to create a diff between arbitrary revisions to

the log dialog (BR #66231) <em>Christian Loose &lt;christian.loose@kdemail.net&gt;</em></li>

<li>Make diff options used by &quot;create patch against repository&quot;
function configurable (BR #75017) <em>Christian Loose &lt;christian.loose@kdemail.net&gt;</em></li>
<li>Add &quot;Edit With&quot; entry to the RMB context menu (BR
#75825) <em>Christian Loose &lt;christian.loose@kdemail.net&gt;</em></li>
<li>Checkout a project without CVS folders (cvs export) (BR
#77894) <em>Dermot Daly</em></li>
<li>Checkout a module under a different name (alias) (BR
#80177) <em>Dermot Daly</em></li>
<li>Text in commit dialog and changelog dialog is now
spellchecked <em>Christian Loose &lt;christian.loose@kdemail.net&gt;</em></li>
</ul>

    </li>
    <li><h4>KCachegrind</h4>
    <ul>
    	<li>Storing/Switching between visualisation layouts <em>Josef Weidendorfer &lt;Josef.Weidendorfer@gmx.de&gt;</em></li>
    	<li>Possibility to show 2 event types in annotations <em>Josef Weidendorfer &lt;Josef.Weidendorfer@gmx.de&gt;</em></li>
    	<li>PNG export of graph view <em>Josef Weidendorfer &lt;Josef.Weidendorfer@gmx.de&gt;</em></li>
    	<li>Import filters for OProfile, memprof, PERL/Phyton/PHP

profilers <em>Josef Weidendorfer &lt;Josef.Weidendorfer@gmx.de&gt;</em></li>

</ul>

    </li>
    <li><h4>Kompare</h4>
    <ul>
    	<li>Add support for choosing a different encoding for reading the

files or diff output. <em>Otto Bruggeman &lt;bruggie@home.nl&gt;</em></li>

<li>Add support for more diff options. <em>Otto Bruggeman &lt;bruggie@home.nl&gt;</em></li>
<li>Added support for showing the differences in a line. <em>Otto Bruggeman &lt;bruggie@home.nl&gt;</em></li>
<li>Added support for the -x and -X options to diff in the Settings
dialog. <em>Otto Bruggeman &lt;bruggie@home.nl&gt;</em></li>
<li>Make the divider draggable <em>Jeff Snyder &lt;jeff-kdeweb@caffeinated.me.uk&gt;</em></li>
</ul>

    </li>
    <li><h4>KBabel</h4>
    <ul>
    	<li>GUI for projects <em>Stanislav Visnovsky &lt;&#118;&#x69;&#115;n&#111;vs&#107;y&#064;k&#100;e.&#0111;rg&gt;</em></li>
    	<li>Use of KConfigXT <em>Stanislav Visnovsky &lt;vis&#00110;&#x6f;vsky&#64;kd&#x65;&#x2e;&#x6f;&#x72;&#x67;&gt;</em></li>
    </ul>

    </li>
    </ul>

</ul>

<a name="kdetoys"><h3>kdetoys</h3>

<ul>
	<ul>
	<li><h4>KWeather</h4>
	<ul>
		<li>Add Day/Night support to KWeather. <em>Ian Reinhart Geiser &lt;&#x67;&#x65;i&#115;eri&#x40;k&#100;e&#x2e;&#111;&#x72;g&gt;</em></li>
		<li>Better font support. <em>Ian Reinhart Geiser &lt;&#00103;eis&#x65;ri&#00064;&#107;&#100;&#00101;&#00046;&#x6f;&#0114;&#103;&gt;</em></li>
		<li>Better layout support. <em>Ian Reinhart Geiser &lt;&#x67;&#0101;i&#00115;e&#x72;&#x69;&#64;&#x6b;&#00100;&#101;.&#x6f;&#x72;g&gt;</em></li>
	</ul>

    </li>
    <li><h4>KWorldClock</h4>
    <ul>
    	<li>Provide a reasonable manual <em>Brad Hards &lt;bradh@frogmouth.net&gt;</em></li>
    </ul>

    </li>
    </ul>

</ul>

<a name="kdeutils"><h3>kdeutils</h3>

<ul>
	<ul>
	<li><h4>Kgpg</h4>
	<ul>
		<li>Do not show expired keys in the signing dialog <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
		<li>Make it possible to change colors assigned to trust
values <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
		<li>Add option to hide invalid keys (revoked/expired) <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
		<li>Improve multi-file operations (better feedback,
non-blocking) <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
		<li>Added search filter in key dialogs <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
		<li>Added status bar for improved feedback <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
		<li>Improve autodetection of dropped files (signatures,
keys) <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
		<li>Change icon to reflect KGpg&#039;s state (busy/ready) <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
	</ul>

    </li>
    <li><h4>KCalc</h4>
    <ul>
    	<li>More different modes: logic, statistics,

trig-functions <em>Klaus Niederkrueger &lt;kniederk@MI.Uni-Koeln.DE&gt;</em></li>

<li>Pressing Ctrl displays the accels for all buttons. <em>Klaus Niederkrueger &lt;kniederk@MI.Uni-Koeln.DE&gt;</em></li>
<li>Display content can be copied with standard actions
cut/copy/paste <em>Klaus Niederkrueger &lt;kniederk@MI.Uni-Koeln.DE&gt;</em></li>
</ul>

    </li>
    <li><h4>Ark</h4>
    <ul>
    	<li>Search Bar <em>Henrique Pinto &lt;henrique.pinto@kdemail.net&gt;</em></li>
    	<li>New Konqueror Integration Plugin <em>Georg Robbers &lt;Georg.Robbers@urz.uni-hd.de&gt;</em></li>
    	<li>KParts-based integrated viewer <em>Henrique Pinto &lt;henrique.pinto@kdemail.net&gt;</em></li>
    	<li>Improved Extraction Dialog <em>Henrique Pinto &lt;henrique.pinto@kdemail.net&gt;</em></li>
    </ul>

    </li>
    </ul>

</ul>

<a name="kdevelop"><h3>kdevelop</h3>

<ul>
	<ul>
		<li>Simple toolbar classbrowser <em>Alexander Dymo &lt;cloudtemple@mksat.net&gt;</em></li>
		<li>New documentation plugin <em>Alexander Dymo &lt;cloudtemple@mksat.net&gt;</em></li>
		<li>KDevAssistant application - a standalone API
documentation viewer <em>Alexander Dymo &lt;cloudtemple@mksat.net&gt;</em></li>
		<li>Framework for various project documentation
plugins <em>Alexander Dymo &lt;cloudtemple@mksat.net&gt;</em></li>
		<li>Doxygen plugin updates <em>Amilcar do Carmo Lucas &lt;amilcar@ida.ing.tu-bs.de&gt;</em></li>
		<li>KDevLicense interface <em>Sascha Cunz &lt;scunz@ng-projekt.de&gt;</em></li>
		<li>Copy/Paste in editor context menu <em>Jens Dagerbo &lt;jens.dagerbo@swipnet.se&gt;</em></li>
		<li>New FileList plugin <em>Jens Dagerbo &lt;jens.dagerbo@swipnet.se&gt;</em></li>
		<li>Make some KMDI features configurable <em>Jens Dagerbo &lt;jens.dagerbo@swipnet.se&gt;</em></li>
		<li>New Code Snippet plugin <em>Robert Gruber</em></li>
		<li>Doxygen preview and autocomment <em>Jonas Jacobi &lt;jonas.jacobi@web.de&gt;</em></li>
	</ul>
</ul>

<a name="kdewebdev"><h3>kdewebdev</h3>

<ul>
	<ul>
		<li><strong>NEW IN KDE: </strong>KImageMapEditor
integration <em>Andras Mantia &lt;a&#00109;a&#110;&#116;&#105;a&#64;kd&#0101;.&#00111;rg&gt;, Jan Schaefer &lt;janschaefer@users.sourceforge.net&gt;</em></li>
		<li><strong>NEW IN KDE: </strong>Integrate KLinkStatus into
kdewebdev <em>Andras Mantia &lt;a&#109;anti&#0097;&#x40;kde.o&#114;&#x67;&gt;, Paulo Moura Guedes &lt;pmg@netcabo.pt&gt;</em></li>
	<li><h4>Quanta Plus</h4>
	<ul>
		<li>Use KMDI for the MDI <em>Andras Mantia &lt;a&#109;a&#0110;&#116;&#105;a&#64;&#x6b;de.o&#x72;g&gt;</em></li>
		<li>Basic support for development teams in projects <em>Andras Mantia &lt;&#00097;&#109;an&#0116;&#x69;&#97;&#0064;&#00107;de&#046;o&#x72;&#x67;&gt;</em></li>
		<li>Directory Templates <em>Andras Mantia &lt;&#97;man&#116;&#x69;&#x61;&#x40;k&#x64;e.&#111;&#x72;&#00103;&gt;</em></li>
		<li>Complete template features <em>Andras Mantia &lt;a&#x6d;ant&#105;&#97;&#64;kde.&#0111;r&#x67;&gt;</em></li>
		<li>Supplemental project file data <em>Eric Laffoon &lt;&#x73;&#x65;qu&#00105;&#116;&#x75;r&#64;k&#x64;e&#0046;o&#114;&#x67;&gt;</em></li>
		<li>Multiple upload profiles <em>Andras Mantia &lt;&#97;m&#0097;&#x6e;&#116;&#0105;a&#x40;k&#100;e&#46;&#0111;r&#x67;&gt;</em></li>
		<li>Basic CVS project integration (Commit/Update) <em>Andras Mantia &lt;a&#109;&#97;nti&#97;&#64;&#00107;&#x64;&#x65;&#x2e;&#x6f;rg&gt;</em></li>
		<li>Support for downloading resources from a central
server <em>Andras Mantia &lt;ama&#110;&#00116;ia&#064;k&#x64;&#0101;.o&#x72;g&gt;</em></li>
		<li>Event actions for file and project operations <em>Andras Mantia &lt;&#97;m&#x61;&#x6e;&#x74;ia&#64;k&#x64;e.&#00111;r&#00103;&gt;</em></li>
		<li>Interface personalities for tasks and project team
members <em>Andras Mantia &lt;a&#109;a&#110;tia&#00064;kde.&#111;rg&gt;</em></li>
		<li>PHP debugger support <em>Linus McCabe &lt;Linus@mccabe.nu&gt;, Thiago Silva &lt;thiago.silva@kdemail.net&gt;</em></li>
		<li>Improve the file trees <em>Jens Herden &lt;jens@kdewebdev.org&gt;</em></li>
		<li>Basic support for any editor implementing the KTextEditor
interface <em>Andras Mantia &lt;&#x61;&#x6d;an&#x74;&#105;a&#64;&#107;&#x64;&#00101;&#46;o&#x72;g&gt;</em></li>
		<li>Replace accented characters on the fly <em>Andras Mantia &lt;ama&#00110;ti&#0097;&#x40;kde.&#x6f;&#x72;g&gt;</em></li>
	</ul>

    </li>
    <li><h4>Kommander</h4>
    <ul>
    	<li>Enable list widget functionality <em>Michal Rudolf &lt;mrudolf@kdewebdev.org&gt;</em></li>
    	<li>Enable tree detail widget functionality <em>Michal Rudolf &lt;mrudolf@kdewebdev.org&gt;</em></li>
    	<li>Improve the editor <em>Michal Rudolf &lt;mrudolf@kdewebdev.org&gt;</em></li>
    	<li>Improve the text editor <em>Michal Rudolf &lt;mrudolf@kdewebdev.org&gt;</em></li>
    	<li>Add DCOP functions assistant <em>Michal Rudolf &lt;mrudolf@kdewebdev.org&gt;</em></li>
    	<li>Handle multiple dialogs packaged in single archive <em>Michal Rudolf &lt;mrudolf@kdewebdev.org&gt;</em></li>
    	<li>Add localization functions <em>Michal Rudolf &lt;mrudolf@kdewebdev.org&gt;</em></li>
    	<li>Enable free slots <em>Michal Rudolf &lt;mrudolf@kdewebdev.org&gt;</em></li>
    	<li>Make widgets DCOP accessable <em>Michal Rudolf &lt;mrudolf@kdewebdev.org&gt;</em></li>
    	<li>Add the ability to plug in widgets <em>Marc Britton &lt;consume@optushome.com.au&gt;</em></li>
    	<li>Add rich text blogging widget <em>Marc Britton &lt;consume@optushome.com.au&gt;</em></li>
    	<li>New @readSetting and @writeSetting specials for saving

text <em>Marc Britton &lt;consume@optushome.com.au&gt;</em></li>

<li>Population text - populates widgets with signals and
slots <em>Marc Britton &lt;consume@optushome.com.au&gt;</em></li>
</ul>

    </li>
    <li><h4>KFileReplace</h4>
    <ul>
    	<li>New startup/new project dialog <em>Andras Mantia &lt;&#x61;man&#x74;&#105;&#00097;&#x40;kd&#00101;&#046;o&#114;g&gt;</em></li>
    </ul>

    </li>
    <li><h4>KLinkStatus</h4>
    <ul>
    	<li>Filter to display links (broken, good, timeout, etc) <em>Paulo Moura Guedes &lt;moura@kdewebdev.org&gt;</em></li>
    	<li>Pause and resume search. <em>Paulo Moura Guedes &lt;moura@kdewebdev.org&gt;</em></li>
    	<li>Validate URLs with reference (#) <em>Paulo Moura Guedes &lt;moura@kdewebdev.org&gt;</em></li>
    	<li>Edit link referrers in Quanta <em>Paulo Moura Guedes &lt;moura@kdewebdev.org&gt;</em></li>
    	<li>Add GUI for settings. <em>Paulo Moura Guedes &lt;moura@kdewebdev.org&gt;</em></li>
    </ul>

    </li>
    </ul>

</ul>