------------------------------------------------------------------------
r1031784 | scripty | 2009-10-06 03:00:04 +0000 (Tue, 06 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1032326 | sengels | 2009-10-07 14:48:26 +0000 (Wed, 07 Oct 2009) | 1 line

use warning only with gnuc
------------------------------------------------------------------------
r1032981 | scripty | 2009-10-09 03:13:07 +0000 (Fri, 09 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1042067 | mpyne | 2009-10-29 03:13:33 +0000 (Thu, 29 Oct 2009) | 6 lines

Postpone creation of Player DBus object until after the player has been properly setup.
This hopefully also fixes bug 211755 (please reopen if it doesn't :)
This fix will be in KDE 4.3.3.

BUG:211755

------------------------------------------------------------------------
r1042068 | mpyne | 2009-10-29 03:15:52 +0000 (Thu, 29 Oct 2009) | 5 lines

Postpone CoverManager shutdown until after the playlists have been destroyed, backported
to KDE 4.3.3.

BUG:192371

------------------------------------------------------------------------
r1042537 | mpyne | 2009-10-30 01:50:41 +0000 (Fri, 30 Oct 2009) | 8 lines

Backport fix for bug 204391 to KDE 4.3.3 or 4.3.4 (we'll see who wins the race!)
This patch from Kåre Särs should fix the issue of JuK skipping every other track on
Windows (and possibly elsewhere, depending on how useful/lousy your Phonon backend is).

If it doesn't fix the bug, just reopen.

BUG:204391

------------------------------------------------------------------------
