------------------------------------------------------------------------
r1132786 | trueg | 2010-06-01 00:09:57 +1200 (Tue, 01 Jun 2010) | 1 line

Backport: do not reset available vars before using them
------------------------------------------------------------------------
r1134404 | jlayt | 2010-06-04 21:28:15 +1200 (Fri, 04 Jun 2010) | 2 lines

New printing features didn't make 4.7, and 4.8 doesn't look likely either

------------------------------------------------------------------------
r1134467 | nlecureuil | 2010-06-04 23:58:27 +1200 (Fri, 04 Jun 2010) | 5 lines

Fix listing of files if it starts with ":"
BUG: 228577
FIXED-IN: 4.4.5


------------------------------------------------------------------------
r1134476 | ahartmetz | 2010-06-05 00:25:50 +1200 (Sat, 05 Jun 2010) | 3 lines

Backport r1134265 from trunk.
CCBUG: 240585

------------------------------------------------------------------------
r1134652 | orlovich | 2010-06-05 08:02:51 +1200 (Sat, 05 Jun 2010) | 8 lines

Merged revision:r1134650 | orlovich | 2010-06-04 15:54:43 -0400 (Fri, 04 Jun 2010) | 7 lines

Don't let our nested KHTMLPart to be destroyed by widget destruction.
This may cause it to be destroyed before us, causing a Cache::clear 
leak check while we're hanging on to a CachedImage. We will clean the 
part properly ourselves in our destructor anyway.

Fixes some exit crashes w/ark.
------------------------------------------------------------------------
r1134707 | mpyne | 2010-06-05 14:30:30 +1200 (Sat, 05 Jun 2010) | 3 lines

Backport duplicate icon search path elimination to KDE SC 4.4.5. This should be a slight
speedup when having to search for an icon to load. (Backport of r1131792 and 1131793).

------------------------------------------------------------------------
r1135154 | jlayt | 2010-06-07 03:33:14 +1200 (Mon, 07 Jun 2010) | 7 lines

Backport RTL fix for KDatePicker/KDateTable

Backport form trunk of revision 1135145

BUG: 213637


------------------------------------------------------------------------
r1136325 | sengels | 2010-06-10 02:07:27 +1200 (Thu, 10 Jun 2010) | 1 line

backport r1136323
------------------------------------------------------------------------
r1136335 | sengels | 2010-06-10 03:00:08 +1200 (Thu, 10 Jun 2010) | 1 line

more changes - last must not be 0
------------------------------------------------------------------------
r1136498 | rkcosta | 2010-06-10 15:30:26 +1200 (Thu, 10 Jun 2010) | 4 lines

Backport r1136494.

Add missing quote for the lang(ru, uk) > q selector.

------------------------------------------------------------------------
r1137667 | dafre | 2010-06-14 10:53:43 +1200 (Mon, 14 Jun 2010) | 4 lines

Backport r1137666:
Fix a bug where the parser did not filter out characters that needed to be escaped. I also added a blacklist hash to ease the process for future characters


------------------------------------------------------------------------
r1138391 | cfeck | 2010-06-16 10:02:45 +1200 (Wed, 16 Jun 2010) | 4 lines

Next attempt at KPluginSelector lockup (backport r1137013)

CCBUG: 213068

------------------------------------------------------------------------
r1140118 | pino | 2010-06-20 09:24:20 +1200 (Sun, 20 Jun 2010) | 2 lines

revert r1136325 and r1136335, as they broke kcompletion and its unit tests

------------------------------------------------------------------------
r1140403 | ilic | 2010-06-21 03:41:11 +1200 (Mon, 21 Jun 2010) | 1 line

Fixed glitch in fallback logic, caused wrong behavior in some cases.
------------------------------------------------------------------------
r1140492 | pino | 2010-06-21 09:21:55 +1200 (Mon, 21 Jun 2010) | 2 lines

update the definition of application/x-nzb

------------------------------------------------------------------------
r1142445 | cfeck | 2010-06-25 14:00:59 +1200 (Fri, 25 Jun 2010) | 5 lines

More protection against broken XCF files (backport r1142443)

CCBUG: 242518
FIXED-IN: 4.4.5

------------------------------------------------------------------------
r1142806 | mueller | 2010-06-26 06:41:08 +1200 (Sat, 26 Jun 2010) | 2 lines

KDE 4.4.5

------------------------------------------------------------------------
