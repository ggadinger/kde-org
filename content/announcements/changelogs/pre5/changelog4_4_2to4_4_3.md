---
aliases:
- ../changelog4_4_2to4_4_3
hidden: true
title: KDE 4.4.3 Changelog
---

<h2>Changes in KDE 4.4.3</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_4_3/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix wrongly aligned long texts in KPageDialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=234838">234838</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1116669&amp;view=rev">1116669</a>. </li>
        <li class="bugfix ">Fix slow wheel scrolling in KCategorizedView (Dolphin/KPluginSelector). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=233163">233163</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1118521&amp;view=rev">1118521</a>. </li>
        <li class="bugfix ">Moving tabs in konqueror would focus the location bar (and prevent moving further using the shortcuts). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=159295">159295</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1110402&amp;view=rev">1110402</a>. </li>
      </ul>
      </div>
      <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix special handling of renaming to lower/upper case on FAT filesystems. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162358">162358</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1109205&amp;view=rev">1109205</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Properly destroy plugins when object removed from document, should
        fix the 'ghost sound' problem on YouTube. See SVN commit <a href="http://websvn.kde.org/?rev=1118443&amp;view=rev">1118443</a>. </li>
        <li class="bugfix ">Properly attempt part creation when no src set if we have datatype;
        needed for a part of new YouTube skin's probe sequence. See SVN commit <a href="http://websvn.kde.org/?rev=1118443&amp;view=rev">1118443</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_4_3/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kmenuedit">KMenuEdit</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Folders can now be moved. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=227836">227836</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1108974&amp;view=rev">1108974</a>. </li>
      </ul>
      </div>
      <h4><a name="konsole">Konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Prevent crash if the session management's Konsole file is unusable. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203621">203621</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1115610&amp;view=rev">1115610</a>. </li>
        <li class="bugfix ">Correct ANSI's 'delete char' to delete the last character in a line. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=217669">217669</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1119960&amp;view=rev">1119960</a>. </li>
        <li class="bugfix ">Prevent crash when saving session data; don't use a reference to a freed object. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=232584">232584</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1114971&amp;view=rev">1114971</a>. </li>
        <li class="bugfix ">Prevent crash if no "Copy To" is active and user selects "Copy To None." Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=234330">234330</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1114967&amp;view=rev">1114967</a>. </li>
      </ul>
      </div>
      <h4><a name="wallpapers">wallpapers</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Provide support for archive for wallpapers. See SVN commit <a href="http://websvn.kde.org/?rev=1109055&amp;view=rev">1109055</a>. </li>
      </ul>
      </div>
      <h4><a name="nspluginviewer">nspluginviewer</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Avoid some pointless wakeups. See SVN commit <a href="http://websvn.kde.org/?rev=1118452&amp;view=rev">1118452</a>. </li>
        <li class="bugfix ">Properly create the widget in top-level mode. See SVN commit <a href="http://websvn.kde.org/?rev=1118452&amp;view=rev">1118452</a>. </li>
        <li class="bugfix ">Improve plugin scripting (npruntime support, unidirectional only), to make new YouTube skin work. See SVN commit <a href="http://websvn.kde.org/?rev=1118452&amp;view=rev">1118452</a>. </li>
      </ul>
      </div>      
    </div>
        <h3 id="kdeplasma-addons"><a name="kdeplasma-addons">kdeplasma-addons</a><span class="allsvnchanges"> [ <a href="4_4_3/kdeplasma-addons.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="picture frame applet">picture frame applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make various Pictures of the day work again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=226230">226230</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1112110&amp;view=rev">1112110</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_4_3/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix audio files playing silently when no volume level has been specified. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=232353">232353</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1112326&amp;view=rev">1112326</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_4_3/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/okteta" name="okteta">Okteta</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash after a reload while editing a value. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=232775">232775</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1109567&amp;view=rev">1109567</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Do not crash when saving file in editor was cancelled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=233259">233259</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1110994&amp;view=rev">1110994</a>. </li>
      </ul>
      </div>
    </div>
        <h3 id="kdesdk"><a name="kdesdk">kdesdk</a><span class="allsvnchanges"> [ <a href="4_4_3/kdesdk.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kapptemplate">KAppTemplate</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make Qt4 GUI app be built by KDevelop. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=232696">232696</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1112090&amp;view=rev">1112090</a>. </li>
      </ul>
      </div>
        </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_4_3/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kolourpaint">kolourpaint</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when zoom operation is aborted. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=234300">234300</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1114559&amp;view=rev">1114559</a>. </li>
      </ul>
      </div>
      <h4><a href="http://okular.kde.org/" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix mouse autohide sometimes not working in presentation mode. See SVN commit <a href="http://websvn.kde.org/?rev=1108275&amp;view=rev">1108275</a>. </li>
        <li class="bugfix ">Fix crash when reading some FictionBook files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=217135">217135</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1109640&amp;view=rev">1109640</a>. </li>
        <li class="bugfix ">Fix crash when reading some ODT files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=233944">233944</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1113768&amp;view=rev">1113768</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_4_3/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="audiocd kioslave">audiocd kioslave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash when entering the Information tab on a CD that was not correctly recognized. See SVN commit <a href="http://websvn.kde.org/?rev=1118482&amp;view=rev">1118482</a>. </li>
        <li class="bugfix ">Improve CD detection. See SVN commit <a href="http://websvn.kde.org/?rev=1118482&amp;view=rev">1118482</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="extragear"><a name="extragear">extragear</a><span class="allsvnchanges"> [ <a href="4_4_3/extragear.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="konq-plugins">konq-plugins</a></h4>
      <div class="product" style="padding-left: 20px;">
      </div>
    </div>