---
aliases:
- /announcements/plasma-5.16.3-5.16.4-changelog
hidden: true
plasma: true
title: Plasma 5.16.4 Complete Changelog
type: fulllog
version: 5.16.4
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Notifier: don't accumulate notifications. <a href='https://commits.kde.org/discover/3efc98f24168286eef03bc8e7a9998f9f2c638f4'>Commit.</a>
- Notifier: Use QPointer instead of a raw pointer for notifications. <a href='https://commits.kde.org/discover/abfad62a7f975aabfffe73c40760fc51a998f626'>Commit.</a>

### <a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a>

- Guard against plasmashell being properly dead and not showing the SNI. <a href='https://commits.kde.org/drkonqi/3eeb0ce5bc1ee61dc9c9ff4376cad5302db4e2de'>Commit.</a> See bug <a href='https://bugs.kde.org/383863'>#383863</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22553'>D22553</a>

### <a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a>

- Kioclient: Don't convert `:x:y` to `?line=x&column=y` for URLs starting with remote schemes. <a href='https://commits.kde.org/kde-cli-tools/42ef318a9d1c454f96b60181d8231a59233720ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408632'>#408632</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22525'>D22525</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Fixed dictionary runner not finding any definitions. <a href='https://commits.kde.org/kdeplasma-addons/cda996dfb011d50bcc1dcc68add42bdf24f175fc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376905'>#376905</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22814'>D22814</a>
- Hide the applet when clicking the view and it's expanded. <a href='https://commits.kde.org/kdeplasma-addons/692dc35493126665446d038019c42a2307a10beb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395589'>#395589</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [kcmkwin/kwindesktop] Make SpinBox editable. <a href='https://commits.kde.org/kwin/93f210e857ef7a64d4052451330c990d445d4eb7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/410368'>#410368</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22819'>D22819</a>
- Fix Qt warnings when rect is invalid, e.g. QRect(0,2111 3840x0). <a href='https://commits.kde.org/kwin/d72e2bfc557c6a865efb417a657c3134b55fab9d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9014'>D9014</a>
- Don't crash when highlighted tabbox client is closed. <a href='https://commits.kde.org/kwin/d948d247fe4371462f2fe6b96b25fd8026abafb5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406784'>#406784</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20916'>D20916</a>
- Fix case-sensitivity typo in libinput configuration function. <a href='https://commits.kde.org/kwin/3b0f704e3b90b723a8516ce3fbff9bd18e2b11cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408943'>#408943</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22388'>D22388</a>
- Fix creation of kdeglobals if /etc/xdg/kdeglobals present. <a href='https://commits.kde.org/kwin/c3c030d8b5953e4f2181e45fdc399ea786ac9352'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22238'>D22238</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Do not skip code launching application in application dashboard. <a href='https://commits.kde.org/plasma-desktop/fba194e562730e4b038a231330b9d45c331f8535'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408748'>#408748</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22306'>D22306</a>
- Properly initialize oldStart and oldEnd in PreviewImageProvider::requestImage. <a href='https://commits.kde.org/plasma-desktop/b0f1d6620d78235e5b85defab880d68129f7a4e9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409518'>#409518</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22707'>D22707</a>
- Fix compilation with Qt 5.13 (missing include QTime). <a href='https://commits.kde.org/plasma-desktop/7c151b8d850f7270ccc3ffb9a6b3bcd9860609a3'>Commit.</a>
- [LNF KCM] make it possible to close the preview. <a href='https://commits.kde.org/plasma-desktop/d88f6c0c89e2e373e78867a5ee09b092239c72de'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22543'>D22543</a>
- [Fonts KCM] Alter DPI only on explicit user interaction. <a href='https://commits.kde.org/plasma-desktop/e9a38fbd4d6af2b3b5a5ab10417ab895ed13c12b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405572'>#405572</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22397'>D22397</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Airplane mode improvements. <a href='https://commits.kde.org/plasma-nm/7dd740aa963057c255fbbe83366504bbe48a240e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399993'>#399993</a>. Fixes bug <a href='https://bugs.kde.org/400535'>#400535</a>. Fixes bug <a href='https://bugs.kde.org/405447'>#405447</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22680'>D22680</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [Notifications] Keep new delegate hidden until room has been cleared. <a href='https://commits.kde.org/plasma-workspace/f5515049b37f97f22c828c519e63f1aac163b313'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22760'>D22760</a>