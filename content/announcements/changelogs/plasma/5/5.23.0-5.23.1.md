---
title: Plasma 5.23.1 complete changelog
version: 5.23.1
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: Don't reuse GError instances. [Commit.](http://commits.kde.org/discover/0f2f62b8412892df23787439c1d1cd4a5b6eeb83) Fixes bug [#426565](https://bugs.kde.org/426565)
+ Flatpak: Set the real value of the remotes. [Commit.](http://commits.kde.org/discover/2836f6373acbfbca666f08bbe725e01a8b0a7ca2) Fixes bug [#443455](https://bugs.kde.org/443455)
+ SourcesPage: Address how we show the checked state. [Commit.](http://commits.kde.org/discover/e9f3a33feb24093f5f40e565a16b20e81334b0ee) Fixes bug [#406295](https://bugs.kde.org/406295)
+ Suggest correct words in the search field. [Commit.](http://commits.kde.org/discover/2a659ba2328b22dead5aca93c971ee3506cacdc0) Fixes bug [#443945](https://bugs.kde.org/443945)
+ SourcesPage: Use ItemIsUserCheckable to check if a source is checkable. [Commit.](http://commits.kde.org/discover/1407b82ce0c1871d1a5418056f26eabc9d1a9e4a) 
+ Flatpak: Do not crash when a source is disabled. [Commit.](http://commits.kde.org/discover/5f7350e84caf1499bb920f7eed32c3ce7973ba01) 
+ Update kf5 version requirement to 5.86. [Commit.](http://commits.kde.org/discover/cfb54919b70cb59d6f00b02880bfc347d571a980) 
+ Remove pointless tooltip from Labelbackground. [Commit.](http://commits.kde.org/discover/7a0631a7a101dc0691b3c8c37edc7ec7ad688e28) 
{{< /details >}}

{{< details title="kactivitymanagerd" href="https://commits.kde.org/kactivitymanagerd" >}}
+ Fix build by lowering KDE_COMPILERSETTINGS_LEVEL. [Commit.](http://commits.kde.org/kactivitymanagerd/393a89ce2c0fa97c2b4def3ba7a7d4f40bf0a8c6) 
{{< /details >}}

{{< details title="kde-cli-tools" href="https://commits.kde.org/kde-cli-tools" >}}
+ Update kf5 version requirement to 5.86. [Commit.](http://commits.kde.org/kde-cli-tools/5ecdfb1a45633f43aab1f35a6b04277bc3c6816f) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Fix algorithm for common sizes in Generator::cloneScreens. [Commit.](http://commits.kde.org/kscreen/eac5fe142efa67e1ba3d491ffa7a735135c1abc3) Fixes bug [#442822](https://bugs.kde.org/442822)
+ Update kf5 version requirement to 5.86. [Commit.](http://commits.kde.org/kscreen/7070ab79516786d82312eb60e005c0499e148308) 
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Fix sleep and hibernate actions. [Commit.](http://commits.kde.org/kscreenlocker/7458926a43370cb1f7ba83c6fa87884f04d1ba6a) 
+ Update kf5 version requirement to 5.86. [Commit.](http://commits.kde.org/kscreenlocker/a5fb54f0c2166c2203f44a2c439ed00164b1f376) 
{{< /details >}}

{{< details title="kwayland-integration" href="https://commits.kde.org/kwayland-integration" >}}
+ Fix: Kicker's sub-menus are covered up by Plasma panels. [Commit.](http://commits.kde.org/kwayland-integration/3cea874d8ed333bc0d79f665765a52b70d457757) Fixes bug [#378694](https://bugs.kde.org/378694)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Platforms/drm: fix disabled outputs with legacy drivers. [Commit.](http://commits.kde.org/kwin/85c4ebfa612d50386d74af14f1a103d1109a24c5) 
+ Platforms/drm: reverse output creation order. [Commit.](http://commits.kde.org/kwin/1cce55f594ad3796e1cb0fe4c07ef6f630f1db29) 
+ X11: Properly unredirect windows if compositing is not possible. [Commit.](http://commits.kde.org/kwin/140834e75a86153e50c7551802085272cab6bff1) Fixes bug [#443953](https://bugs.kde.org/443953)
+ Restore old behavior of Workspace::clientArea(clientOpt, Toplevel). [Commit.](http://commits.kde.org/kwin/2958881264caf8d3bd83a34411e9586f8fcb7211) Fixes bug [#443787](https://bugs.kde.org/443787)
+ Do not ask for decoration settings without guarding. [Commit.](http://commits.kde.org/kwin/a3ffcfa88019e8095f8eb78e0180326d60de33f8) 
+ Scripting: Guard against nullptr m_client in WindowThumbnailItem. [Commit.](http://commits.kde.org/kwin/42126a2596dad53b7e5c62856bb346187a0ad901) Fixes bug [#443765](https://bugs.kde.org/443765)
+ Properly update parent item's bounding rect when moving item. [Commit.](http://commits.kde.org/kwin/5df7f620c5ac03fb772e700c8138fb0f59e13093) Fixes bug [#443756](https://bugs.kde.org/443756)
+ Fix AbstractClient::adjustedSize() for wayland windows with no committed buffer. [Commit.](http://commits.kde.org/kwin/65b878f6dfdb9ab73aee50a31ce05ed4cef97880) Fixes bug [#443705](https://bugs.kde.org/443705)
+ Set KDE_COMPILERSETTINGS_LEVEL. [Commit.](http://commits.kde.org/kwin/f6938af096613e672cc215cb6432f7becfaa6371) 
+ Pipewire: Fix downloading software-rotated textures. [Commit.](http://commits.kde.org/kwin/26aab43f91a67128d9b15e48644d190fc3262e57) 
+ [wayland] fix crash on startup with lv3:ralt_alt XKB option. [Commit.](http://commits.kde.org/kwin/ef6a9c47c5a77b45bdd77628279a4bfe85c97978) Fixes bug [#440027](https://bugs.kde.org/440027)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Fix "clear emoji history" action. [Commit.](http://commits.kde.org/plasma-desktop/839f6bc0cf42620ef5f22224b1761ee303203413) Fixes bug [#443974](https://bugs.kde.org/443974)
+ Kickoff: Replace Kicker DragHelper with Qt Quick Drag. [Commit.](http://commits.kde.org/plasma-desktop/a6e732f35f4fcbfeeb0cbf41609e62c040326ab7) Fixes bug [#443708](https://bugs.kde.org/443708)
+ Make tooltip interactive when there is a player but no window is present. [Commit.](http://commits.kde.org/plasma-desktop/796eb9e09b25cac45d50a3a38a6cbbc2e756d64c) Fixes bug [#443425](https://bugs.kde.org/443425)
+ Taskmanager: Disconnect from backend.showAllPlaces on destruction. [Commit.](http://commits.kde.org/plasma-desktop/7caec0de8fe9ca64d015388d957e782c68fc8126) Fixes bug [#443820](https://bugs.kde.org/443820)
+ Add systemsettings runner to hardcoded list of allowed runners for kicker. [Commit.](http://commits.kde.org/plasma-desktop/ef588c632b260a907d9b74d9e7ae39745d1472a8) Fixes bug [#443776](https://bugs.kde.org/443776)
+ Folder View: Don't cancel multi-selection with right-click. [Commit.](http://commits.kde.org/plasma-desktop/93c82a56c091f3355e26514ae093ac31dfc37a8c) Fixes bug [#443743](https://bugs.kde.org/443743)
+ [applets/kicker] Explicitly set visible property when opacity is 0. [Commit.](http://commits.kde.org/plasma-desktop/3bd34bae87a295a8dba3915df5a450b04a98d1c3) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Openconnect widget: Default to "anyconnect" when protocol is not set. [Commit.](http://commits.kde.org/plasma-nm/f4ae0cec44c151415c337cd38b44393df0b63b07) 
+ Openconnect: Make FSID passphrase + empty private key combination work. [Commit.](http://commits.kde.org/plasma-nm/c9f4c07157c269fcf9bd41981bc534d2f8b5e43b) Fixes bug [#443770](https://bugs.kde.org/443770)
+ Only include mobileproviderstest with ModemManager. [Commit.](http://commits.kde.org/plasma-nm/534e4ade96993146fa6e2015a3813edef15502b7) 
{{< /details >}}

{{< details title="Plasma Phone Components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Panel: Fix quicksetting delegate width not getting enforced on label. [Commit.](http://commits.kde.org/plasma-phone-components/2a9699b0f8d14ded9bd2e0fdc80f3f04234af9e1) 
+ Update kf5 version requirement to 5.86. [Commit.](http://commits.kde.org/plasma-phone-components/c195b51a99c28f9c6b86acf95b05dc2b3154d183) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [freespacenotifier] impr: no notification for ro filesystems. [Commit.](http://commits.kde.org/plasma-workspace/707b22ff16419a70692725be299f56c361d37ba3) 
+ Kcms/colors: Don't apply header accent colors to non-Header color schemes. [Commit.](http://commits.kde.org/plasma-workspace/ef1b01c680ca1961757e4509f890d89def1cfc66) Fixes bug [#443786](https://bugs.kde.org/443786)
+ [kcms/colors] Notify global settings change after saving. [Commit.](http://commits.kde.org/plasma-workspace/1a3699a8bcd3fe27e5ce3ba123624c36060aa213) 
+ Krdb: Fix removal of Xft.dpi from Xresources. [Commit.](http://commits.kde.org/plasma-workspace/85399471b2aad37e4524bde20b70fe2cb61354ba) Fixes bug [#350039](https://bugs.kde.org/350039). See bug [#376406](https://bugs.kde.org/376406)
+ Sddm-theme: fix missing password field on "Other" page. [Commit.](http://commits.kde.org/plasma-workspace/d5a3e749a30613294f41386180aaf31dfb1a9bee) Fixes bug [#443737](https://bugs.kde.org/443737)
+ Update kf5 version requirement to 5.86. [Commit.](http://commits.kde.org/plasma-workspace/4f76938b6b0f9e10f9c8354f63e3770e3d9bf1b8) 
+ Pipewire: Report DRM_FORMAT_MOD_INVALID when no modifiers are offered. [Commit.](http://commits.kde.org/plasma-workspace/e9b466c47376d4ecac0d8e9711eeb6aa7ea9437b) 
+ [digital-clock] Do not assign undefined when agenda is not visible. [Commit.](http://commits.kde.org/plasma-workspace/e122bafcd27824c1d87dcb6f56168dc469510753) Fixes bug [#443380](https://bugs.kde.org/443380)
{{< /details >}}

