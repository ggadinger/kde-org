---
aliases:
- /announcements/plasma-5.20.0-5.20.1-changelog
hidden: true
plasma: true
title: Plasma 5.20.1 complete changelog
type: fulllog
version: 5.20.1
---

<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Show "Enable bluetooth" only when Manager is operational. <a href='https://commits.kde.org/bluedevil/c115bdd69788940769f8aec416ab2cf7be38475c'>Commit.</a> </li>
<li>Fix device icon size on device page. <a href='https://commits.kde.org/bluedevil/97129ef619de66505d281be35fa6f6661357b114'>Commit.</a> </li>
<li>[kcm] Set sane default size. <a href='https://commits.kde.org/bluedevil/fdc622af5f49b6ccfc83641c9ce8e8d77a99e82a'>Commit.</a> </li>
</ul>


<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Remove the shadow in widgetDestroyed(). <a href='https://commits.kde.org/breeze/e370403c44aa6f1c3ad558d879673a6bead49c75'>Commit.</a> </li>
<li>Regenerate wallpaper sizes. <a href='https://commits.kde.org/breeze/c57d8fdf307347f5c3421c2ef6ba7e67a6bf4690'>Commit.</a> </li>
<li>Generate_wallpaper_sizes.py: Properly compress wallpapers. <a href='https://commits.kde.org/breeze/3a53b37468d9041e4b614571a7fb445dd2b53994'>Commit.</a> </li>
<li>Fix for occasional background glitches behind transparent Menus, especially when hovering over menu elements. <a href='https://commits.kde.org/breeze/855d4497a373af3f09c994f0730cd625db7069cc'>Commit.</a> </li>
</ul>


<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Fix a dangling reference to the list of database files. <a href='https://commits.kde.org/kactivitymanagerd/590778d6f105e11f4521d5bed65ac0e30b086132'>Commit.</a> See bug <a href='https://bugs.kde.org/427650'>#427650</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Fix typo in small icons window switcher. <a href='https://commits.kde.org/kdeplasma-addons/b30115beb4f1068bab359a9680dbf35c3f03ac11'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427745'>#427745</a></li>
<li>Use higher match priority and copy suggested text. <a href='https://commits.kde.org/kdeplasma-addons/b3b26e75ba3cd546117371391d8b36be484a700c'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Divide network speeds by 2. <a href='https://commits.kde.org/ksysguard/33694eafa0178ecb1e33d7d26bb15dc97defbf3c'>Commit.</a> </li>
</ul>


<h3><a name='kwayland-server' href='https://commits.kde.org/kwayland-server'>KWayland-server</a> </h3>
<ul id='ulkwayland-server' style='display: block'>
<li>Send wl_pointer.frame when emulating pointer events out of touch ones. <a href='https://commits.kde.org/kwayland-server/3642ecf3db63a10ff77deb9de353614b331f0066'>Commit.</a> </li>
<li>Properly handle destruction of XdgOutputV1Interface. <a href='https://commits.kde.org/kwayland-server/7a8ac182d4ddcfbafe7badc89778ff5b6c375777'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426293'>#426293</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Introduce persistent global share context. <a href='https://commits.kde.org/kwin/445d1496e4a57d81dc86928e4f8b2385a73e6386'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415798'>#415798</a></li>
<li>Fix a potential SIGSEGV. <a href='https://commits.kde.org/kwin/062e0441a56a09d49f8478a109690ca063fb1417'>Commit.</a> </li>
<li>Qpa: Create a pbuffer for internal windows. <a href='https://commits.kde.org/kwin/3dcc4ebd37858e5913e12f93c7e05e3e8496bd5b'>Commit.</a> </li>
<li>Core: Use less confusing name for Platform::supportsQpaContext(). <a href='https://commits.kde.org/kwin/259dedfc99aaf076befa68bc024b0cec6cc1356b'>Commit.</a> </li>
<li>Qpa: Merge OpenGL platform context classes. <a href='https://commits.kde.org/kwin/7d327d0b8f5d0d3f0929cf9a638a5af1ffc0baa1'>Commit.</a> </li>
<li>Scene: Reduce the call cost of Platform::supportsQpaContext(). <a href='https://commits.kde.org/kwin/77a6d2c6fa0e31679742a6d139bc9bc68d1d8f0e'>Commit.</a> </li>
<li>Fix pipewire stream double free. <a href='https://commits.kde.org/kwin/ae6e7feef093f43753b46fbf8f54ed0282fd16d2'>Commit.</a> </li>
<li>Detect softpipe and llvmpipe on Mesa 20.2+. <a href='https://commits.kde.org/kwin/6addcab364d972d0f990f50e8125390ee290ecbc'>Commit.</a> </li>
<li>Wayland: Introduce logicalToNativeMatrix() helper. <a href='https://commits.kde.org/kwin/e9b751d2c177e9d63453d3abd67b0fdc1abec44d'>Commit.</a> </li>
<li>Platforms/drm: Fix software flip output transforms. <a href='https://commits.kde.org/kwin/642be48bd283606b515594bfe8ee5b8f60c395e4'>Commit.</a> </li>
<li>Platforms/drm: Compute correct cursor transform matrix. <a href='https://commits.kde.org/kwin/1fd9ae618aaf8b9fcd5b644fa3039ec782d6355f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427605'>#427605</a>. See bug <a href='https://bugs.kde.org/427060'>#427060</a></li>
<li>Wayland: Fix Qt clients not being maximized initially. <a href='https://commits.kde.org/kwin/76a33c90fb3a8ebdb9a8049953d79f093927f250'>Commit.</a> </li>
<li>Xwayland: Avoid creating a tree query on crash. <a href='https://commits.kde.org/kwin/0109bdbba9bfef93134e4d2b8e4d4ead2dde84b4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427688'>#427688</a></li>
<li>Wayland: Block geometry updates while placing popups. <a href='https://commits.kde.org/kwin/83ed25031c799d678add8a0661709e006e642fca'>Commit.</a> </li>
<li>Fix KWIN_EFFECT_FACTORY macros. <a href='https://commits.kde.org/kwin/e35a5a7e1d55233276455ff2b488628fa2add7ca'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>[cursors] Add symlinks for corner resize. <a href='https://commits.kde.org/oxygen/46431203c3309b90c31dc63a579d735bdae0365a'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Fix sizing of tooltip with media controls without thumbnail. <a href='https://commits.kde.org/plasma-desktop/cd5195e8e088c888173bac33650de4aa5008deab'>Commit.</a> </li>
<li>[kcms/autostart] Set sane default size. <a href='https://commits.kde.org/plasma-desktop/f13a460b806840651934e2a24aa1d3933954eb2b'>Commit.</a> </li>
<li>[kcms/user] Fix leaking model. <a href='https://commits.kde.org/plasma-desktop/081a30017ab3706ff2d7dfcfbe0086ac38c255a9'>Commit.</a> </li>
</ul>


<h3><a name='plasma-disks' href='https://commits.kde.org/plasma-disks'>plasma-disks</a> </h3>
<ul id='ulplasma-disks' style='display: block'>
<li>Fix device type check for freebsd. <a href='https://commits.kde.org/plasma-disks/313bd99b9d62086f20a25609f1d287a5afa5e84d'>Commit.</a> </li>
<li>Replace open-fstat dance with lstat. <a href='https://commits.kde.org/plasma-disks/97ec1c6e3ceb432763cf2c1e7212a507598a2942'>Commit.</a> </li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Kdirselectdialog: when creating a new dir that already exists, select it. <a href='https://commits.kde.org/plasma-integration/6e28f9394095be9823781ad087ec10df89d83b4a'>Commit.</a> </li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Bump Controls 2.14 in DeviceCombobox.qml. <a href='https://commits.kde.org/plasma-pa/1d0b0f32a8169858e03b7e9288cbdd8dada8e14d'>Commit.</a> </li>
<li>Correctly look up data in the model. <a href='https://commits.kde.org/plasma-pa/463544c76af4f4574653da79a8072a9cb6a4e28b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427687'>#427687</a></li>
<li>Simplify DeviceComboBox. <a href='https://commits.kde.org/plasma-pa/347f598ec75c0f4605f54a0eb104f37b110bdf00'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408700'>#408700</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[applets/devicenotifier] Don't offer to unmount non-removable devices. <a href='https://commits.kde.org/plasma-workspace/59ce9a3e9f162992e5158c3a5eaebf2ef3352eae'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427176'>#427176</a></li>
<li>[applets/systemtray] Fix icon size for 24px panels. <a href='https://commits.kde.org/plasma-workspace/8e3de778c666adf3b9f6b94ed5ddc7369ebad32c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427690'>#427690</a></li>
<li>Make Prison a required dependency. <a href='https://commits.kde.org/plasma-workspace/38ced5810c26e5de4d6dd0058e9733ff5f10dfa7'>Commit.</a> </li>
<li>[notifications] Fix margins of close button timeout indicator. <a href='https://commits.kde.org/plasma-workspace/6d2ef8e25612d0c125190983bfed90bec106ec00'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425911'>#425911</a></li>
<li>[Notifications] Also check transient parent for whether it's a dialog. <a href='https://commits.kde.org/plasma-workspace/684bc80657e4ccaf52aed6426e8d80d1d475742d'>Commit.</a> See bug <a href='https://bugs.kde.org/426187'>#426187</a></li>
<li>Revert "krdb: Call xrdb with -nocpp" to fix gitk runtime errors. <a href='https://commits.kde.org/plasma-workspace/ae43cd883313e2309b3f90416b69e2e008b54a25'>Commit.</a> </li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Ignore player's from KDE Connect when suspending. <a href='https://commits.kde.org/powerdevil/4ac78e7118238414d3f2d603c21975413eea8bb0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427209'>#427209</a></li>
</ul>


</main>
