---
aliases:
- ../../fulllog_releases-22.12.1
title: KDE Gear 22.12.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Ensure Akonadi IncidenceChanger doesn't try to show dialogs. [Commit.](http://commits.kde.org/akonadi-calendar/9baa8ca070a73658bec0c5a9a1854e4ec73abd8a)
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Include sys/stat.h to fix undefined mode_t on Musl systems. [Commit.](http://commits.kde.org/ark/e806c02dec9d42a0816a3f57b8c1aaa5673ea635)
{{< /details >}}
{{< details id="baloo-widgets" title="baloo-widgets" link="https://commits.kde.org/baloo-widgets" >}}
+ Make tests pass on gitlab. [Commit.](http://commits.kde.org/baloo-widgets/1b724989330d174fc62ceb195a375d1a6f100cb5)
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Revert "DolphinTrash: port away from deprecated KIO API". [Commit.](http://commits.kde.org/dolphin/72fb5462931440a88ccf669d18cefdee97b685bf)
+ Revert "Port away from deprecated KIO API". [Commit.](http://commits.kde.org/dolphin/f0a6120eb95f44eb97bb801869b5b19812e6d517)
+ Revert "Fix build with older KF versions". [Commit.](http://commits.kde.org/dolphin/9059742323460bf69729c7b2387228f7e025dced)
+ Revert "portalize drag urls". [Commit.](http://commits.kde.org/dolphin/c8aed8ac81d9f7f3dc93a7570037041228a98bf4) See bug [#457529](https://bugs.kde.org/457529). See bug [#462928](https://bugs.kde.org/462928)
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Work around bug in QThreadPool::tryStart(std::function). [Commit.](http://commits.kde.org/filelight/83d15a5a71bf189627c28dd2eb2e2a9f41863648) Fixes bug [#449688](https://bugs.kde.org/449688)
+ Change the semantics of duplication. [Commit.](http://commits.kde.org/filelight/dc1bfb1943ff14f0f5daa072b3e198dbcaa42e92) Fixes bug [#463442](https://bugs.kde.org/463442)
+ Not all files are folders. [Commit.](http://commits.kde.org/filelight/b5b77ffc7e784ff2fed662fadba060e10451b569) Fixes bug [#463304](https://bugs.kde.org/463304)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Set icon for open with action. [Commit.](http://commits.kde.org/gwenview/f6847a3bec650bcb664f94ca589fbd071a6888d7)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Tile pkpass event ticket background images vertically. [Commit.](http://commits.kde.org/itinerary/5232b080136aa51a1629ea76a33d8c87fb01847c)
+ Only accept pan and zoom gestures on the location picker map. [Commit.](http://commits.kde.org/itinerary/2696f30d2da9f2ff9a461fe3a1f9180c136b91a9)
+ Remove readonly from another constant property. [Commit.](http://commits.kde.org/itinerary/cc1ae1509fa1ba3bd2b998164ead54dfe0894e0f)
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Fix regression: set focus to time widget in error in Alarm Edit dialog. [Commit.](http://commits.kde.org/kalarm/200daf868e43a725e445a35e619610210431a731)
+ Update change log. [Commit.](http://commits.kde.org/kalarm/5911c7e6528caf351b84b86a23253229399273d4)
+ Bug 463310: Allow AM/PM control in Edit Alarm dialogue to be tabbed into. [Commit.](http://commits.kde.org/kalarm/dd73bd6c3b6ee77f3c40ca057bf52ebada39a038)
+ Fix handling of quoted path if configured for the terminal for command alarms. [Commit.](http://commits.kde.org/kalarm/feef605887d6f3fe474dae9f3e185b4006b2d181)
+ Update change log. [Commit.](http://commits.kde.org/kalarm/148e683abeec89a016c9bdbda29cd52c92b69ae4)
+ Fix handling of quoted path if configured for the terminal for command alarms. [Commit.](http://commits.kde.org/kalarm/a9201952c2e061e09710cf566eedfa7629d46824)
+ Evaluate trigger times for working time alarms in KAlarm's default time zone. [Commit.](http://commits.kde.org/kalarm/81e15901561f4f6960627a994dbf8e0e46356bfc)
+ Update date picker display when a resource becomes disabled. [Commit.](http://commits.kde.org/kalarm/a593c13cf1a4cc4e992164e1f41e8f2dbd432d26)
+ Implement automatic version suffix increment for each KDE Gear release. [Commit.](http://commits.kde.org/kalarm/03cb5402c833e495f5c46d88a2b6295df803b091)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Externaltools: Fix crash when saving. [Commit.](http://commits.kde.org/kate/1bdee120c828a1163d3b92b3b95fa92df52f06e1)
+ Open Folder: set QFileDialog::ReadOnly. [Commit.](http://commits.kde.org/kate/ee9fba653653804842fc0d10b77f380026d2d8b5) Fixes bug [#462688](https://bugs.kde.org/462688)
+ Fix crash in compiler explorer widget. [Commit.](http://commits.kde.org/kate/e25672296ee3b93ec1523fc619d080502633d29b)
+ Guard MainWindow::activeView with null checks. [Commit.](http://commits.kde.org/kate/188df5505dd2060a5060f0adf91245303d7a335c)
+ Lsp: Downgrade window/logMessage to MessageType::Log. [Commit.](http://commits.kde.org/kate/d8e96f06c2d251a5d92c66cf71d3b9d989814ba5) Fixes bug [#462476](https://bugs.kde.org/462476)
+ Fix session restore for KWrite. [Commit.](http://commits.kde.org/kate/a6c64ff86e9815b3f719880442dd9242f60380e6) Fixes bug [#463139](https://bugs.kde.org/463139)
+ Fix size issue with config widet. [Commit.](http://commits.kde.org/kate/d9a621f34108a1098c6a87c17917ebe4735b3e4d)
+ Documents: Hide 'Open Widgets' when no widgets open. [Commit.](http://commits.kde.org/kate/01d2e4b59c4ce3d5d32fd81ea215b7ee1350280b)
{{< /details >}}
{{< details id="kdenetwork-filesharing" title="kdenetwork-filesharing" link="https://commits.kde.org/kdenetwork-filesharing" >}}
+ Fix desktop file name for kinfocenter. [Commit.](http://commits.kde.org/kdenetwork-filesharing/cdd05509851ccbaff5e6ece05ff3c53c7ea17f12)
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ New camera proxy for Akaso. [Commit.](http://commits.kde.org/kdenlive/95a5eb04060b1716fd82c33854d9df58ef7eeafe)
+ Fix replacing image clip discards its duration. [Commit.](http://commits.kde.org/kdenlive/a58689dbb4b58906525ae5d06451db714f08d1f5) Fixes bug [#463382](https://bugs.kde.org/463382)
+ Subtitles: when using a background with alpha, draw one rect around all lines to avoid overlay issues. [Commit.](http://commits.kde.org/kdenlive/3f38004e7d88eb4bc0b9dba8ed3998bf2b3d730a)
+ Fix multiple issues with copy/paste/move rotoscoping keyframes. [Commit.](http://commits.kde.org/kdenlive/4cc1af75d77d12b21b9bdb43baba8ddb0e9fdfdd)
+ Don't build designer plugins by default - only useful for developers. [Commit.](http://commits.kde.org/kdenlive/978ebcf51c0a779f3711d7c6b732b866211d2b6c)
+ Fix color balance filter not disabled when picking a new color. [Commit.](http://commits.kde.org/kdenlive/8fd8ec751387765c862fab36adeb600c05b896f4)
+ Fix possible freeze on aborting edit-friendly transcoding request. [Commit.](http://commits.kde.org/kdenlive/d0b871f0fe16f2f66fb5c0a9d2fd30ed1c92de9b)
+ Fix remove space on tracks with a mix. [Commit.](http://commits.kde.org/kdenlive/08fc0897823f95a7597c1afa91febbff2da17cce)
+ Fix editing multiple markers sets all comments to comment of first. [Commit.](http://commits.kde.org/kdenlive/8b77b7da5284b29f30c4646ec49d650e19b8def4)
+ Fix designer plugin crash. [Commit.](http://commits.kde.org/kdenlive/78c216161f01fae3ebda62bcb27446ba386d1bbe)
+ Fix guides move in spacer/trimming operations and tests. [Commit.](http://commits.kde.org/kdenlive/75d1bb8062901c4ad25523c227325674636b5e59)
+ Small improvement to audio on pause/play. [Commit.](http://commits.kde.org/kdenlive/cfa13f830c787946ed9c936eb22615ebf5d1a94b)
+ Fix typo. [Commit.](http://commits.kde.org/kdenlive/73e63f60a4b0cd52c11b2363c04d95ee8124467c)
+ Don't interrupt timeline playback when refreshing the clip monitor or changing preview resolution. [Commit.](http://commits.kde.org/kdenlive/256e4d6fbe76d20a1749287ab3846a11f2f4fea6)
+ Don't show timeline preview crash message if job was stopped by a timeline operation. [Commit.](http://commits.kde.org/kdenlive/f6b699194bd9c627b4b328f3563a6afd52e3c792)
+ Fix close button in status messages not working. [Commit.](http://commits.kde.org/kdenlive/ca2393aae5b5e99c77d71015e6ff8d67c700b62b)
+ Preview chunks should be sorted by integer. [Commit.](http://commits.kde.org/kdenlive/66db84ba5e97a71236bc1cb2747206e14a3ce0a6)
+ Fix timeline preview incorrectly stopping when moving a clip outside preview zone. [Commit.](http://commits.kde.org/kdenlive/789239336823c39846f4f7f5453d5f0d6a02806f)
+ QMetaObject::invokeMethod should be used with Qt::DirectConnection when expecting a return argument. [Commit.](http://commits.kde.org/kdenlive/468aa1213c3db3724a20b32817966d0b969664e2)
+ Disable parallel processing on 32bit systems. [Commit.](http://commits.kde.org/kdenlive/e4291ba5f0b21d606706eab17c1692f0708ffbbe)
+ Fix pressing Esc during timeline drag corrupts timeline. [Commit.](http://commits.kde.org/kdenlive/ad81f39d99d7d8ed1bd27a80e8693eb69f6e53aa)
+ Fix guides incorrectly moved when unlocked. [Commit.](http://commits.kde.org/kdenlive/125d1b3e4950878d60010b7e9b4902e7a91509b6)
+ Update mouse position in timeline toolbar on zoom and scroll timeline. [Commit.](http://commits.kde.org/kdenlive/51ea8c037ab7d99cc45b128c263d3614a72e1d42)
+ Fix crash dropping an effect with a scene (rotoscoping, transform,...) on the project monitor. [Commit.](http://commits.kde.org/kdenlive/8baeda138be2622ef6edba6fe743580367af0943)
+ Fix zoom sometimes behaving incorrectly on very low zoom levels. [Commit.](http://commits.kde.org/kdenlive/b74dc5e9b2fa3fb91980b264d2a230d8232e19c2)
+ Fix zoom on mouse not working as expected when zooming after last clip. [Commit.](http://commits.kde.org/kdenlive/549da07cd701e4a32df90f1e16546e66bc22cd59)
+ Restrict guides to integer position on paint to avoid drawing artifacts. [Commit.](http://commits.kde.org/kdenlive/4b36bc293c903a44468a94dea6951a2b63da6782)
+ Fix resize zone conflicting with move on low zoom levels. [Commit.](http://commits.kde.org/kdenlive/e35b9017826551057fe7041e36e3652e1dd17447)
+ Fix title clip line break sometimes pushing text outside item rect. [Commit.](http://commits.kde.org/kdenlive/a6be02a90e8764431d5aedfa28547e6d464d4ae7)
+ Fix rendering when using an MLT properties file with a space in it. [Commit.](http://commits.kde.org/kdenlive/05fa0568d1511ab65dae958a20c342204937baca) Fixes bug [#462650](https://bugs.kde.org/462650)
+ Fix monitor overlay sometimes incorrectly placed. [Commit.](http://commits.kde.org/kdenlive/1cceea6f274ea007dfb21cc8cd23293a6543595c)
+ Ensure on monitor marker color is updated even if 2 markers have the same text. [Commit.](http://commits.kde.org/kdenlive/431b643275afc50a845ce0206b907c56479f32b2)
+ Cleanup monitor zone resize. [Commit.](http://commits.kde.org/kdenlive/cf9b53629815f99ecfca2020ff20703a91dc7cf7)
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Repeat pkpass event ticket background image vertically. [Commit.](http://commits.kde.org/kdepim-addons/31705ff455a47780e96a3291db8e6914c02af3ac)
{{< /details >}}
{{< details id="kgpg" title="kgpg" link="https://commits.kde.org/kgpg" >}}
+ CMake: search for gpgme with pkg-config first. [Commit.](http://commits.kde.org/kgpg/697e9fd839ecfe34bd174431350c9c67fbc711b6)
+ Close write channel after sending new password when generating new key. [Commit.](http://commits.kde.org/kgpg/9fc74c0d1d3db89998d9e995dfad2500d6e857de) Fixes bug [#457738](https://bugs.kde.org/457738)
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ [activities] Mark worker as read-only. [Commit.](http://commits.kde.org/kio-extras/538222427f4d0a0e5502d9d946170d4f98f1fab9) See bug [#444363](https://bugs.kde.org/444363)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Handle more NH booking confirmation variants. [Commit.](http://commits.kde.org/kitinerary/1e320b05c685404553da18870245dda26a3262aa)
+ Extract class from MAV barcodes. [Commit.](http://commits.kde.org/kitinerary/dbfa07c47c28f2b0cad28a3e07b8ebfe85b433b0)
+ Improve Eventim pkpass seat data extraction. [Commit.](http://commits.kde.org/kitinerary/86d46a6f78c862bb79f158392f4a0e1ec616ea99)
+ Unpack location arrays in Event objects. [Commit.](http://commits.kde.org/kitinerary/1275d9e058f569c5c4d34899efb110fdb1d1d703)
+ Add extractor script for Eventim "TicketDirect" PDFs. [Commit.](http://commits.kde.org/kitinerary/33997a97edf3cd758212f7f3c7d41f6f3f6958a0)
+ Include a seat object in the default event reservation template. [Commit.](http://commits.kde.org/kitinerary/cbc4f8039ef8d605902d4ba26885841f261e0264)
+ Post-process Seat objects. [Commit.](http://commits.kde.org/kitinerary/a1723c2cde1093eb2feecfe17e4398180d269e77)
+ Support international Trenitialia tickets. [Commit.](http://commits.kde.org/kitinerary/c9d3a3ada9d376abbf5d83c2da0eec5014e17873)
+ Support French language variants of Trenitalia tickets. [Commit.](http://commits.kde.org/kitinerary/448691d4c4bfd691c646d478e1488722bf1c99c2)
+ More complete implementation of uPER length determinant decoding. [Commit.](http://commits.kde.org/kitinerary/6bcf03d7e51576007a3531fb89ebb1710f65e3a2)
+ Protect against out-of-bounds reads in BitVector. [Commit.](http://commits.kde.org/kitinerary/454d007280b7128acfb7de79b6f2b1f6c08b9511)
{{< /details >}}
{{< details id="kmag" title="kmag" link="https://commits.kde.org/kmag" >}}
+ Fix grabbing for multi-screen configurations. [Commit.](http://commits.kde.org/kmag/ed3f9b4c485600b2676b64225d32367ff8570ad1)
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Fix bug 460747: Kontact Crashes Upon Start. [Commit.](http://commits.kde.org/kmail/9d37b837eca35bd7976709a9d25d3700c70e321e) Fixes bug [#460747](https://bugs.kde.org/460747)
+ Fix Kontact configuration, overview, modules not displayed for selection (eg mail, notes, calender ...). [Commit.](http://commits.kde.org/kmail/265ea600c5011f535e95999338b8c9dbcc4db790) Fixes bug [#463390](https://bugs.kde.org/463390)
+ Allow to load local file (fix loading image). [Commit.](http://commits.kde.org/kmail/85cfb2f06f079216ea5d496ea9efad400e924d75)
+ Remove info about removed feature (ShowUserAgent). [Commit.](http://commits.kde.org/kmail/fc0b487d3f3ac9c9ca0650aa635795b5478de85b) See bug [#448348](https://bugs.kde.org/448348)
+ BUG: 459399 Fix Random text in kmail's message list configuration settings. [Commit.](http://commits.kde.org/kmail/6618f8f80ccadff908d11db7506b3af8d15ad032) Fixes bug [#459399](https://bugs.kde.org/459399)
+ Fix bug 460289:  Opening the detailed progress window does nothing. [Commit.](http://commits.kde.org/kmail/437295a534eca2907e749f18d8d895da50d343b9) Fixes bug [#460289](https://bugs.kde.org/460289)
{{< /details >}}
{{< details id="knights" title="knights" link="https://commits.kde.org/knights" >}}
+ Update src/proto/ficsdialog.cpp. [Commit.](http://commits.kde.org/knights/dad5045128d4ab2c0e0ce8e57f516666915e5859)
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Save changes to Start page. [Commit.](http://commits.kde.org/konqueror/6d69057aac7ee8808c055bf4518a1d7da7ed47e6)
+ Avoid possible crash while looking for fields to auto fill. [Commit.](http://commits.kde.org/konqueror/d4f78e4a6eea2c8e579a221b106f642bb1d419fc)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Fix applying default encoding in profile editor. [Commit.](http://commits.kde.org/konsole/e620e0b73f1f5ccab38c991ad13b7db41a066410)
+ Fix applying default encoding from KCodecAction. [Commit.](http://commits.kde.org/konsole/991627bf5bb532cdb51145e34397e8ad5022d29a)
+ Change word mode defaults for text render. [Commit.](http://commits.kde.org/konsole/001ef6658b1eb11edccdd86273f9a16f2fe629de)
+ Fix saving new profiles. [Commit.](http://commits.kde.org/konsole/9566ed97d0d2e85f27790ab8f1d89ac0c9ad4373) Fixes bug [#460383](https://bugs.kde.org/460383)
+ Recover comment lost in refactoring. [Commit.](http://commits.kde.org/konsole/b9577014c799108ab0d2bac7c28d45f972e7846f)
+ Fractional scaling and Antialias don't mix well. [Commit.](http://commits.kde.org/konsole/35c5bc11094e56aece691ef574fe93456beab96d) Fixes bug [#462914](https://bugs.kde.org/462914)
+ Fixes for EastAsianWidth wide chars on RTL context. [Commit.](http://commits.kde.org/konsole/a1934c046cab6df6b8e1af6fe7a1ca74a0f95e7c)
+ Fix regression with CJK wide chars at cursor. [Commit.](http://commits.kde.org/konsole/6e6832788189a07fd6aa20f383bb7a88bd24bf90) Fixes bug [#463031](https://bugs.kde.org/463031)
+ Allow Shift+Arrows+Alt/Ctrl for applications. [Commit.](http://commits.kde.org/konsole/07cbfb8904a6102107ac9e4ec3b368cfff8053da) Fixes bug [#405073](https://bugs.kde.org/405073)
+ Propagate Shift+PgUp/PgDown in alternate screen. [Commit.](http://commits.kde.org/konsole/c5a1830f7631804cdf8235e779c9e1c1360546de) Fixes bug [#461150](https://bugs.kde.org/461150)
+ Cosmetic fixes for keytabs. [Commit.](http://commits.kde.org/konsole/f93621ee29c2d29f440423e764577ec2c897776b)
+ Fix macos.keytab. [Commit.](http://commits.kde.org/konsole/6475577af854db580ea522b70474b228cf9c7494)
+ Support Ctrl and Alt in mouse tracking mode. [Commit.](http://commits.kde.org/konsole/a072d060c6abf94f0fcaf95531c8260b814f4314) Fixes bug [#442306](https://bugs.kde.org/442306)
+ Url filter: start regex with word boundary. [Commit.](http://commits.kde.org/konsole/a48e4deab23de1283f89aea49bf8037412daede1) Fixes bug [#462511](https://bugs.kde.org/462511). Fixes bug [#455166](https://bugs.kde.org/455166)
+ Fix macos.keytab. [Commit.](http://commits.kde.org/konsole/0c00c6c9543077bb3bf9de51c834b6b33c33f99a)
+ Set half page scrolling when the user selects this in the profile edit dialog. [Commit.](http://commits.kde.org/konsole/9d3e599ecb097175673218b696da9f274c86a434) Fixes bug [#462971](https://bugs.kde.org/462971)
+ Fix black pixel at (0,0). [Commit.](http://commits.kde.org/konsole/e73068d117d9bba76d6b45e96f85c4113e79bc6a) Fixes bug [#462421](https://bugs.kde.org/462421)
+ Correct the action name for Shift-Ctrl-PageDown. [Commit.](http://commits.kde.org/konsole/0bfcc0776fbfa212c8c6106b6ca581a82e8e1f9a)
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Consider pen width when computing polyline bounding boxes. [Commit.](http://commits.kde.org/kosmindoormap/76ce995a460aa3bfa6f48e8e50cdd62c493c1ba8)
+ Pass view to scene item boundingRect() methods. [Commit.](http://commits.kde.org/kosmindoormap/b9bc58d1f1ed0bbf2abf85345e560f481732d33f)
{{< /details >}}
{{< details id="kpat" title="kpat" link="https://commits.kde.org/kpat" >}}
+ Fix crash when dealing last card from the deck. [Commit.](http://commits.kde.org/kpat/c4bb5993cb34a80867a5ddfdfa8aabad249f63f2) Fixes bug [#424263](https://bugs.kde.org/424263)
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Partial support for new GBFS v2.3 vehicle and propulsion types. [Commit.](http://commits.kde.org/kpublictransport/eeef3f440e0b4272f21213c047fc11da9568d376)
+ Fix GBFS floating vehicle selection by distance. [Commit.](http://commits.kde.org/kpublictransport/754b7e9b4c67ea5acb474e3a94b553a76eb5f01e)
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ Vnc: Fix handling of hires and horizontal scroll. [Commit.](http://commits.kde.org/krdc/3a6647134b887720b78225eafa6d5fb2f2a1d734)
{{< /details >}}
{{< details id="libkdcraw" title="libkdcraw" link="https://commits.kde.org/libkdcraw" >}}
+ Fix build with libraw 0.21. [Commit.](http://commits.kde.org/libkdcraw/fc4b2fb44de7f6d26c27c635f60ee5cef7a1ad14)
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Disable load from local file. [Commit.](http://commits.kde.org/messagelib/45e8c1dabd9a7744f0d9395e65061f384009c4ab)
+ Fix load image. [Commit.](http://commits.kde.org/messagelib/947933a609b61c43b13e58667a8882bf093b637f)
+ Const'ify variable. [Commit.](http://commits.kde.org/messagelib/5855afd29d230193ce869a67020ef6009d87a4a9)
+ Avoid to duplicate email identity. [Commit.](http://commits.kde.org/messagelib/18893efcb84f3436a156531acae72511ccfb7cb1)
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Enable Support for Dark Theme for Titlebar in Windows. [Commit.](http://commits.kde.org/okular/a3386f16054e833ddf108a032c027476833c6fc0)
{{< /details >}}
{{< details id="skanlite" title="skanlite" link="https://commits.kde.org/skanlite" >}}
+ Use lambdas for disabling and enabling 'Reselect Device' button. [Commit.](http://commits.kde.org/skanlite/0aab98f92ce1f05cd7f9e3d39bb170f6c3e5e3cd) Fixes bug [#462408](https://bugs.kde.org/462408)
+ Disable button for changing scanner while scan is in progress to prevent sane from crashing. [Commit.](http://commits.kde.org/skanlite/aba8765d35638a1ef1251a96b9234567ae92266b)
{{< /details >}}
{{< details id="yakuake" title="yakuake" link="https://commits.kde.org/yakuake" >}}
+ Re-add missing header when building without KF5Wayland. [Commit.](http://commits.kde.org/yakuake/39b9909bd9eb98728a6f07a220af374e7eb974f7)
{{< /details >}}
{{< details id="zanshin" title="zanshin" link="https://commits.kde.org/zanshin" >}}
+ Disable running task widget on Wayland. [Commit.](http://commits.kde.org/zanshin/e7fda0693debce5da29eeb544871a11ea0081be1)
{{< /details >}}
