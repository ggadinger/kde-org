---
aliases:
- ../../fulllog_releases-22.11.90
title: KDE Gear 22.11.90 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="colord-kde" title="colord-kde" link="https://commits.kde.org/colord-kde" >}}
+ Set CMake project version. [Commit.](http://commits.kde.org/colord-kde/ca57c56ffd84e73bfc7c8a84500bff7e21896be7)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Re-arange playlist overflow menu. [Commit.](http://commits.kde.org/elisa/fca3a9b392d0b41bc4a2cbb9574c35d054767a0f)
+ Add missing rating action to playlist overflow menu. [Commit.](http://commits.kde.org/elisa/99def659d07f9fa2e3db02f3368890aa9960cbb6)
+ Improve delegate drop shadow appearance. [Commit.](http://commits.kde.org/elisa/9809d109c89b03512a3fa9307d7a933c04f1d8fe)
+ Fix crash in metadata extraction. [Commit.](http://commits.kde.org/elisa/16671865b1aafeabc349607182719f0b58fed6e1)Fixes bug [#461750](https://bugs.kde.org/461750)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Actually start the deletion/trash job. [Commit.](http://commits.kde.org/gwenview/eef66c1c23695a61cc2276389b24c248f9050026)Fixes bug [#461761](https://bugs.kde.org/461761)
+ Shorten "Adjust Brightness/Contrast/Gamma" menu item text. [Commit.](http://commits.kde.org/gwenview/cedd458974caa53a2b3ede4f7cd078e54d7ef228)
+ Restore compatibility with KF5 < 5.100. [Commit.](http://commits.kde.org/gwenview/0de73c4dfaa3df1de2ec17272de4d7644f4af2c7)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Correctly merge data from date and time input fields. [Commit.](http://commits.kde.org/itinerary/abefc65801a5246dab10eb134e1268bf71357006)Fixes bug [#461963](https://bugs.kde.org/461963)
+ Report success on importing also when a single health cert was imported. [Commit.](http://commits.kde.org/itinerary/e8b2d8b01fca52a773dc892d72cbf24b63199275)
+ Exclude unused QtMultiMedia plugins from the APK. [Commit.](http://commits.kde.org/itinerary/6c0e25b0afbc6411981e05594f9088a5f25af1df)
+ Use StyledText and set linkColor to text color. [Commit.](http://commits.kde.org/itinerary/f1e82e3f665ef5cf4b1e3e245e33390d2c362b00)
+ EventPage: Wrap event location. [Commit.](http://commits.kde.org/itinerary/03b0265e601ca07ef0b949d02b74937ec0183a02)
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Formatting. [Commit.](http://commits.kde.org/kalarm/807871d9d465c3708e20dfa3735c7449224ff8cb)
+ Bug 461713: Add unit tests for daily alarms before time shift. [Commit.](http://commits.kde.org/kalarm/0af201382b6a54af2987c850d728d989055fdd64)
+ Bug 461713: Fix handling of times which repeat during DST -> standard time shift. [Commit.](http://commits.kde.org/kalarm/3d26729783c3da588a3bed9107d62c31b4100a38)
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Don't open link twice. [Commit.](http://commits.kde.org/kalendar/7c0a65e4b75fe42c18fa8fab069f16f91a354f46)Fixes bug [#455791](https://bugs.kde.org/455791)
+ Use pixel perfect size for icons. [Commit.](http://commits.kde.org/kalendar/a8c73c61e7bb8318719ddaf2d0e22d591c4e0883)Fixes bug [#462175](https://bugs.kde.org/462175)
{{< /details >}}
{{< details id="kalgebra" title="kalgebra" link="https://commits.kde.org/kalgebra" >}}
+ Fix appstream issues. [Commit.](http://commits.kde.org/kalgebra/bd7eb1b2367299d38445a0c5597a3c741e846146)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix focus toolview actions. [Commit.](http://commits.kde.org/kate/dc5d2ceea09776549cb2e82f43c5f19c20e92aea)
+ Lsp: specify codeActionKind valueSet. [Commit.](http://commits.kde.org/kate/cdbb75729d1473ee676bd63e3e89605c18198d9b)
+ Fix code action shortcut. [Commit.](http://commits.kde.org/kate/07d79251bf3b60b0c4750b34a8f126434d5526d6)
+ WelcomeView: Silence warning. [Commit.](http://commits.kde.org/kate/ccb8f2436b3c8b6995effc6d402ded550a4e1a40)
+ Ensure to better pass focus back to right widget. [Commit.](http://commits.kde.org/kate/6433e714350a8fec6411e3ac75f8088cafc3c70c)
+ [WelcomeView] Fix layout stretching. [Commit.](http://commits.kde.org/kate/78a4b979c3b7e104ee73d578390261bc649af9a5)
+ [WelcomeView] Make UI more responsive for windows of large size. [Commit.](http://commits.kde.org/kate/de0eca37f2e35d637b7d8d2e5c7edc76308e69ee)
+ Avoid all session restore/save work for KWrite. [Commit.](http://commits.kde.org/kate/4011ec9e7a1f770cb0a7271abdb2a8e5c905c47a)
+ Build-plugin: Add building/ready indicator icons to the output tab. [Commit.](http://commits.kde.org/kate/f44b1320b68da4698ccb41a09d5b5d946a0d8cee)
+ Fix typo in option name. [Commit.](http://commits.kde.org/kate/aeb8e09d8934836de153bac72370e4b32e97ddc5)
+ TabMimeData: Set URLs on mime data. [Commit.](http://commits.kde.org/kate/7b7bd947278af994b091dfb08bf25030ecdd0297)
+ KateTabBar: Add document icon to drag pixmap. [Commit.](http://commits.kde.org/kate/9b450e14ef8b508145a7056e406c3f247430b360)
+ Fix lsp completion not triggered on user invocation. [Commit.](http://commits.kde.org/kate/e2d1f1b7cc076f1342f9f0eb743b4ccecca20e59)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ [plugins/clipboard] Check for null mimedata. [Commit.](http://commits.kde.org/kdeconnect-kde/e5c117eb68a15b66e77b889dcd1b5267d48ac7fa)Fixes bug [#461875](https://bugs.kde.org/461875)
+ [handler] Fix desktop file name. [Commit.](http://commits.kde.org/kdeconnect-kde/04d9157718c3b5d54465f932b397b8ad5a0f2289)Fixes bug [#461764](https://bugs.kde.org/461764)
+ [handler] Extract translatable strings from ui file. [Commit.](http://commits.kde.org/kdeconnect-kde/61e0108c662cc0f855f6cb565a48bf5417d0ee7f)
+ [handler] Set translation domain. [Commit.](http://commits.kde.org/kdeconnect-kde/abe4a57ebe3fac9aa1decf7a16081f923968e2e5)
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix undo broken when trying to delete all tracks (don't allow it and fix the underlying bug). [Commit.](http://commits.kde.org/kdenlive/6d0b7533b28e356a9a62875ee0e56b63cb0bc9c7)Fixes bug [#462228](https://bugs.kde.org/462228)
+ Fix import keyframes broken. [Commit.](http://commits.kde.org/kdenlive/8647249503bcebf68be7d752e809a61e148795c4)See bug [#456492](https://bugs.kde.org/456492)
+ Fix project duration incorrectly changed when changing speed of last clip in project. [Commit.](http://commits.kde.org/kdenlive/395ebda459da53a655a2708fc024bd5189b9cff7)
+ Correctly stop archiving job on pressing abort. [Commit.](http://commits.kde.org/kdenlive/272290c743cd0134591808cc68a9d6ce4ffba4b3)
+ Animate track lock icon when trying to perform an operation on a locked track instead of silent failure. [Commit.](http://commits.kde.org/kdenlive/23f8c5c8a5059cd3bcd9ef3b962ba7586fba5fdf)
+ Fix luma incorrectly flagged as missing on Windows. [Commit.](http://commits.kde.org/kdenlive/ce465115def5143b629c079d987c7b8ac4b6b4a4)Fixes bug [#461849](https://bugs.kde.org/461849)
+ [Renderwidget] Fix "Edit Metadata" opens wrong page. [Commit.](http://commits.kde.org/kdenlive/a88c178dccfa8dc6a42276587ceb1bbe583c93ff)
+ We do not necessarily need mediainfo to get the source timecode. [Commit.](http://commits.kde.org/kdenlive/487dc0645c503cf2af200456215149ebd3c546a6)
+ Fix crash when trying to launch second Glaxnimate instance with IPC. [Commit.](http://commits.kde.org/kdenlive/8021e16e8bc98e25f0a0f87d12b4d22b5fe91825)
+ Improve perfocmance of online resource search. [Commit.](http://commits.kde.org/kdenlive/e6a1cc154295b5d6cc5533ad761872ac296dcef4)
+ Better error message in some cases of preview render failure. [Commit.](http://commits.kde.org/kdenlive/db528d58fbea80e4f46558fafcf427cf8e18c2c2)
+ Fix incorrect loading of subtitle with two dots. [Commit.](http://commits.kde.org/kdenlive/45042d1a6e101b6148e43865ad28922b1e0a9ac1)Fixes bug [#461995](https://bugs.kde.org/461995)
+ Fix color picker on wayland. [Commit.](http://commits.kde.org/kdenlive/b93e0b21539c29d724dfb9346bacfde75c71e2b7)
+ Port KMessageBox to twoActions where not violating string freeze. [Commit.](http://commits.kde.org/kdenlive/25edf681abf839125d17b6d78c5a5b3fbd2efd8a)
+ Transcoding: use pcm instead of alac codec (fix possible audio artifacts). [Commit.](http://commits.kde.org/kdenlive/fcb24a4cf57e230efaa6ab328a64df48090b7389)
+ Various fixes for spacer operation with subtitle track. [Commit.](http://commits.kde.org/kdenlive/81d3e6610c9998f56dbbba80a37807fa7bd5b6c2)
+ Fix image proxies not correctly applied after recovering proxy, don't attempt to proxy small images. [Commit.](http://commits.kde.org/kdenlive/323ec2c2040543a230b4ffc95d6915751f580c50)
+ Improve recovery for project files with missing proxy and source. [Commit.](http://commits.kde.org/kdenlive/bed54470e309c3238938738873869530ae3f0b60)
+ Clip properties: also show tooltip for image proxies. [Commit.](http://commits.kde.org/kdenlive/b3bc1403b6d2f7a6a6f2be29d9717cf2f8453547)
+ Fix designer crash with Kdenlive widgets. [Commit.](http://commits.kde.org/kdenlive/6d6cd00715e8f0f65b6aedbf83535d3194d8c665)
+ Disable parallel rendering for now (currently crashes because of an MLT regression). [Commit.](http://commits.kde.org/kdenlive/5b5fdfbc10d764c382f2335f214593f0f3d46baf)
+ Fix crash undoing timeremap change after unselecting the clip. [Commit.](http://commits.kde.org/kdenlive/1eac6b03ec0d86f6a74fa10a127174c74c2f7cd5)
+ Fix recovering luma file paths when opening an Appimage project or from another computer. [Commit.](http://commits.kde.org/kdenlive/42bea5dfa9b4f19eb1ffe03e8a66159359e9e7a5)
+ Fix MaxOS compilation. [Commit.](http://commits.kde.org/kdenlive/892c88c5373755ca90cfb9a7956ae7eb8efb3a10)
+ Ensure monitor zone out cannot go beyond clip out. [Commit.](http://commits.kde.org/kdenlive/3ff4f881eb7f9f76e3ddbfeda7d01d81f2ffe6ad)
+ Switch to a proper JSon format to store guide categories instead of hacky line feed separated format. [Commit.](http://commits.kde.org/kdenlive/85cf5974849770bc9b5a14174fc225cab346360f)
+ Don't attempt deleting the clip on aborting a thumbnail job. [Commit.](http://commits.kde.org/kdenlive/da8d2d8b574fc3ed1446b4e4b63e0f432f0830e5)
+ Auto-call taskManager.taskDone() when run() ends. [Commit.](http://commits.kde.org/kdenlive/fced1a0bda4cccfc16dc77234f836d7b4e16f2e9)
+ Fix remove space. [Commit.](http://commits.kde.org/kdenlive/5de71c28e78688dd2de11707c5728a4fa65252f6)
+ Fix relocating files with proxies and image sequences. [Commit.](http://commits.kde.org/kdenlive/deacdde4c1944de1a9a559ea7374a8fde6e5be2e)
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix bug 461771 Again, Languagetool for a french installation ,by default checks with english grammar and english dictionary. [Commit.](http://commits.kde.org/kdepim-addons/7cd5fee151f96d6b719b5d13d0292352b5c67081)Fixes bug [#461771](https://bugs.kde.org/461771)
+ Install plugins in pim<version>. [Commit.](http://commits.kde.org/kdepim-addons/25a202f46b6834511b8b7da6bf40348df8b5bf0c)
{{< /details >}}
{{< details id="kdev-python" title="kdev-python" link="https://commits.kde.org/kdev-python" >}}
+ Fix possible crash in AstTransformer. [Commit.](http://commits.kde.org/kdev-python/c9cfb00a291e7cd61208d1d321df43ff7775961a)
{{< /details >}}
{{< details id="khelpcenter" title="khelpcenter" link="https://commits.kde.org/khelpcenter" >}}
+ Set app translation domain before first i18n call. [Commit.](http://commits.kde.org/khelpcenter/f266b75818dc555a8af32aac979e91d6937e9740)
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ [activities] Fix showing associated files. [Commit.](http://commits.kde.org/kio-extras/270f2487c824713304f8b227fb14fa7e0c5c369c)Fixes bug [#453110](https://bugs.kde.org/453110)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Improve HTML to text conversion. [Commit.](http://commits.kde.org/kitinerary/fcc5767f9bcbf06946b574d1f965f02b1258b009)
+ More HTML whitespace robustness fixes. [Commit.](http://commits.kde.org/kitinerary/93603367ef3c631afbb20fd58e7f32d462dd2adc)
+ Increase robustness against different HTML to text conversions. [Commit.](http://commits.kde.org/kitinerary/98d72a3a6f544e23124360f9e7d5b0a419441dab)
+ Trim phone numbers in post-processing as well. [Commit.](http://commits.kde.org/kitinerary/b997cc41697f20b5c6ee4df17fef770869e7725e)
+ Add Bateliers Arcachon extractor script. [Commit.](http://commits.kde.org/kitinerary/cff357a7d946c50b24ae8cf039a3bef1d275e185)
+ Make PDF title accessible to extractor scripts and filters. [Commit.](http://commits.kde.org/kitinerary/ac9e7f944fbfa7efdad18af63d46b74863bb07a9)
+ Create image nodes for PDF raster image masks as well. [Commit.](http://commits.kde.org/kitinerary/b10c0aa319b0bc2b3f84bd6f26a6c14613640020)
+ Implement PDF image mask loading. [Commit.](http://commits.kde.org/kitinerary/1364b9d23c36e9b250a757dbdca4a3ca7d9cca6f)
+ Look up image masks for loading. [Commit.](http://commits.kde.org/kitinerary/b1b1777a780e19cd312830e754cca7b6c76a3589)
+ Deduplicate images only per page in PDF documents. [Commit.](http://commits.kde.org/kitinerary/cbd3f8dd38d61801ce8df8190f610c1cd6d0b57c)
+ Add a hashable opaque reference type for addressing PDF images. [Commit.](http://commits.kde.org/kitinerary/cdbb9797e51abd67ce60f37ef015d39e08404f62)
+ Attempt to fix the unity build on binary factory. [Commit.](http://commits.kde.org/kitinerary/e5a339aaedea7d2966a9ab8ca8d2608e5ad4400b)
+ Extract CD ticket code from 1154UT vendor blocks. [Commit.](http://commits.kde.org/kitinerary/d4c89c1e7978a0c080bf06567b02478c732d844e)
{{< /details >}}
{{< details id="kmailtransport" title="kmailtransport" link="https://commits.kde.org/kmailtransport" >}}
+ Install plugins in pim5 it's more logical. [Commit.](http://commits.kde.org/kmailtransport/d540f826f9b8da626f536a889c2253d6ef3e0db8)
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Improve detection of recursive calls to Konqueror. [Commit.](http://commits.kde.org/konqueror/f27a9eb1831d98ef294b55076dcdd1affacec067)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Update foreground pgroup info before accessing it. [Commit.](http://commits.kde.org/konsole/65c7187f8c16c7eeb68652633418b8ef1a0c94a0)Fixes bug [#454122](https://bugs.kde.org/454122). Fixes bug [#458157](https://bugs.kde.org/458157)
+ Make MainWindow::wasWindowGeometrySaved() more efficient. [Commit.](http://commits.kde.org/konsole/0f92b70767df70a7acfa67732aeacb0b7b6f460b)
+ Fix geometry restoration check. [Commit.](http://commits.kde.org/konsole/cc9073abdec8864a015bc20d2b94f4d55f456553)Fixes bug [#460428](https://bugs.kde.org/460428)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Fix search plugin. [Commit.](http://commits.kde.org/korganizer/55bf72fd622f2ef215050664f95a647474bcffae)
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Detect another staircase tagging variant. [Commit.](http://commits.kde.org/kosmindoormap/7b0d0fad8e74f785d1a57e65a07a836ad0f8a369)
+ Fix hit detection for labels with a fixed (maximum) text width. [Commit.](http://commits.kde.org/kosmindoormap/41a85714463ae7aa5631596ca5d77ebf35299e14)
+ Add icon for stairwell rooms. [Commit.](http://commits.kde.org/kosmindoormap/a7ddc45ff966c70709023c335d61dbee0da5ff0b)
+ Improve contrast of buildings in the Breeze light style. [Commit.](http://commits.kde.org/kosmindoormap/f19035dd555f245ed8180acee109c7a5bd0ec1bd)
+ Handle another way of tagging staircases for changing floors. [Commit.](http://commits.kde.org/kosmindoormap/81584fc9659145993761e1f18ea1b72502f1c0bf)
+ When falling back to any translation, check it's actually a translation. [Commit.](http://commits.kde.org/kosmindoormap/981b0bad3c012a742dd3aa9bcc6b683ca345784e)
+ Show room numbers if no room name is available. [Commit.](http://commits.kde.org/kosmindoormap/da518c6ae428336a955c3e457bf2d9d16a18bf74)
+ Don't install standalone app metadata and desktop file on Android. [Commit.](http://commits.kde.org/kosmindoormap/2c52e64870874ca1a3e64b9f5417f43ad52ef17f)
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ DB also has Railjet coach layouts in Germany. [Commit.](http://commits.kde.org/kpublictransport/94baa2ec5f4c6d6f949e95e7e9e10dc60b4ad09f)
{{< /details >}}
{{< details id="libkgapi" title="libkgapi" link="https://commits.kde.org/libkgapi" >}}
+ Don't cache promises for AccountManager::findAccount. [Commit.](http://commits.kde.org/libkgapi/d677a08c21fd99e7e8be0a0899f797f9237207e4)Fixes bug [#406839](https://bugs.kde.org/406839). Fixes bug [#409122](https://bugs.kde.org/409122). Fixes bug [#421664](https://bugs.kde.org/421664). Fixes bug [#456923](https://bugs.kde.org/456923)
+ AccountManager: Don't keep finished promises in the cache. [Commit.](http://commits.kde.org/libkgapi/b5a581d98d9b57363c44bd98eeab7243fbf13a22)
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Store lineedit place holder => when we create a new tab we have correct. [Commit.](http://commits.kde.org/messagelib/f62897fa32f207121ba71b4632f4844a91f188b8)
+ Make sure that all lineedit has placeholder. [Commit.](http://commits.kde.org/messagelib/a471bd1655de2d40a7dfd23debb703f4a09034d4)
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Minor optimization. [Commit.](http://commits.kde.org/pimcommon/f915125a5d37365ecfa654e0a11d09b101050b3a)
+ Add autocorrection specific debug category. [Commit.](http://commits.kde.org/pimcommon/df0f222148498755903a2381160e549fa9def06e)
+ Fix crash. [Commit.](http://commits.kde.org/pimcommon/b703266dd9db5b11fe71618445a2e1308d74c02d)
+ Fix pri file. [Commit.](http://commits.kde.org/pimcommon/f71677a8540abdd980892461b74e946b494d75f9)
+ Add more autotest. [Commit.](http://commits.kde.org/pimcommon/45f204a3e545c52f80a6a35f50175f0d13fe54ee)
+ Clean up. [Commit.](http://commits.kde.org/pimcommon/e55b565b3b5c63a65989898021e8102fba31fb5d)
+ Use AutoCorrectionUtils::wordsFromSentence. [Commit.](http://commits.kde.org/pimcommon/416415e6e5e0c4fb6a96b46762aff9a94fcea90b)
+ Const'ify methods. [Commit.](http://commits.kde.org/pimcommon/add877be46fbd993be3f4e59baf1cd92aa7b117e)
+ Debug--. [Commit.](http://commits.kde.org/pimcommon/2fa5b716ba5707ca91debf1df86e6167b5b49a7a)
+ Add more autotests. [Commit.](http://commits.kde.org/pimcommon/2690beccc0afe9f4b365a9755c0270d36936e61b)
+ Remove unused method. [Commit.](http://commits.kde.org/pimcommon/08b309773fa59a99a1af447adc03ec734193e2a6)
+ Make sure to not add empty string. [Commit.](http://commits.kde.org/pimcommon/ab7b905d6267c196d1d6556bcd9c8afec337bb5b)
+ Add more autotests. [Commit.](http://commits.kde.org/pimcommon/4342adbf7b6bf117aac2a3acd0d444b54e3bb97f)
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Lib/cppparser/parser.cpp : Apply patch of comment #1 by Xuxu. Thanks. [Commit.](http://commits.kde.org/umbrello/035c942f83e0d8d39b31c531f237e8ccc0895656)Fixes bug [#456427](https://bugs.kde.org/456427)
+ CMakeLists.txt : Increase UMBRELLO_VERSION_MINOR to 37. [Commit.](http://commits.kde.org/umbrello/0cd0efa45971b5d79647c4dd776f1531c239636f)
{{< /details >}}
