---
qtversion: 5.15.2
date: 2023-01-14
layout: framework
libCount: 83
---


### Breeze Icons

* Revert "Add SimpleScreenRecorder icon" (bug 412490)
* Add some links for symbolic icons
* Don't generate BINARY_ICONS_RESOURCE by default with Qt >= 6

### Extra CMake Modules

* FindXCB: remove Xprint and XEvIE
* Mark translation fetching as deprecated

### KDE Doxygen Tools

* add -u to ensure all variables are defined when used
* update dependencies and add a script to automate this

### KAuth

* Fix polkit package name in CMake config (bug 463402)

### KConfig

* fix safety issue of multiple threads use KConfig in the same process

### KConfigWidgets

* [kcommandbar] Save last used items in state config
* [kcodecaction] Deprecate KEncodingProper-based API

### KContacts

* Fix bug 395683 Entry for security-aware messengers (bug 395683)

### KCoreAddons

* Config.cmake.in: workaround Inotify static issue (bug 460656)
* Better Pluralize 'n minutes ago' (bug 462261)
* Fix portal urls cache on X11 (bug 460314)

### KDeclarative

* calendarevents: add new alternate date ready signal and deprecate the old one (bug 463196)
* Support modifierOnlyAllowed in KeySequenceItem

### KFileMetaData

* odfextractor: Add support for Open Document Graphics

### KGlobalAccel

* Don't install kglobalacceld5 service file for KF6
* Deprecate activateGlobalShortcutContext and shortcut contexts in general

### KDE GUI Addons

* waylandclipboard: Be a bit smarter about when to use the QGuiApplication::clipboard (bug 462979)
* KeySequenceRecorder: Allow modifier-only shortcuts
* KColorSchemeWatcherWin: Fix checking for dark mode
* Correct descriptions for CMake options WITH_WAYLAND and WITH_X11
* waylandclipboard: Update QClipboard when gaining focus (bug 454379)

### KHolidays #

* holidays/plan2/holiday_us_en-us - fix Junetenth date
* Fix German "Buß- und Bettag" holiday is wrong (SN,BY) (bug 461835)
* Add holidays for Malaysia (bug 462867)
* cmake: Use ECMQmlModule for declarative plugin
* qcalendarsystem condition 'months<0' is always true

### KImageFormats

* raw: tweak seek implementation
* heif: fix error handling
* heif: rewrite plugin to use only libheif C API

### KInit

* Fix Windows build

### KIO

* [kprocessrunner] Don't specify which desktop the app should be launched on (bug 462996)
* Fix crash when dealing with user notification dialog
* [WidgetsAskUserActionHandler] Add fallbacks for dialog parent windows (bug 463124)
* [jobuidelegate] Set window when creating delegate
* kdiroperator: fix full file path not being stripped (bug 459900)
* Fix integer overflow for large files in AccessManager (bug 452972)
* Pre-select "Delete" in Delete Confirmation Dialog (bug 462845)
* Make link against KWindowSystem private
* [KOpenWithDialog] Save search history in state config
* file_unix: Fix check for hidden NTFS files
* Use new nomenclature with socket name; use ".socket" suffix
* [previewjob] Deprecate overlayIconSize and overlayIconAlpha
* Replace KIO::SlaveConfig with KIO::WorkerConfig
* [kpropertiesdialog] Remove UI to set DBus startup type
* when inside a sandbox use different openwith behavior
* Don't install service desktop files for KCMs in KF6
* Don't install KonqPopupMenu service type file for KF6

### Kirigami

* InlineMessage: Downstream and clarify padding expressions that are based on background's border width
* Calculate CategorizedSettings's width by using word width instead of length (bug 462698)
* ActionMenuItem: Shorten one binding expression, remove useless check
* Fix getting tablet mode on application startup (bug 462187)
* Show tooltip in avatar if available
* ActionTextField: Specify focus reason for shortcut activation
* Add text to clear action in search field
* ColumnView: Resolve assert
* ActionTextField: Avoid showing empty tooltip for actions without text
* InlineMessage: Make content text selectable, just like their Widgets counterpart
* PageRow: Fix potential popping of visible pages with popHiddenPages
* NavigationTabButton: Support display property
* DefaultCardBackground: Make basic drop shadow follow the radius
* NavigationTabBar: Move button width calculation out of NavigationTabButton
* fix the listitemdraghandle example so it's not a broken mess
* Dialog: Fix assigning undefined to QObject property, which is a type error in QML
* Use StandardKey.Find instead of "CTRL+F"
* ActionTextField: Fix property access error on non-Kirigami actions
* PasssiveNotificationManager: Use Item instead of Control so that we can use a touchscreen
* ActionTextField: Remove useless binding
* Don't accept hover events at the bottom of ApplicationItem
* PageRow: Rename popNotVisiblePages... to popHiddenPages, and fix null
* FormLayout: remove excessive spacing above unlabeled separators (bug 462300)
* Allow closing drawers on non-touch devices (bug 454119)
* Re-implement PassiveNotification functionality
* Add appropriate tooltip and accessibility property to GlobalDrawer's menu button
* Fix double namespace Kirigami.Kirigami.*
* InlineMessage: Make 1 expression explicit and boolean-valued
* InlineMessage: Don't bind your logic to the value of .visible property
* OverlaySheet: Use same heading size as Dialog
* icon: proceed itemChange in QQuickItem (bug 462630)
* Icon: repolish on DPR changes
* Icon: Use correct devicePixelRatio

### KNewStuff

* core: Don't make URL installation a false question
* KNSCore::Question: Include the entry in the question (bug 460136)

### KNotification

* doc: Tell which is the default value of status and category

### KNotifyConfig

* Fix signal after edit TTS_select

### KParts

* ReadOnlyPart: don't emit urlChanged() from destructor

### KService

* Allow accessing custom properties of KServiceActions
* Replace QVariant::Type with QMetaType::Type
* Deprecate KService::dbusStartupType

### KTextEditor

* Fix crash on undo after reload (bug 462793)
* Update the cursor and anchor to sync with the visual but not scroll for selectAll
* Implement invokeAction and commit preedit when click outside the preedit
* Add missing KWindowSystem dependency
* Fix 1 space indent not detected
* Added New Script Sort Uniq
* avoid use of markedAsFoldingStart
* start to compute indentation based folding on demand
* Accept event when clearing multicursors
* Add Dart and Go to katemoderc

### KUnitConversion

* Adapt to HRK being replaced by EUR

### KWallet Framework

* Don't install kwalletd5 service file for KF6

### KWayland

* Make sure that HAVE_MEMFD is defined to 0 if not found

### KWidgetsAddons

* Make Full Screen action use consistent text and icon (bug 240854)

### KWindowSystem

* Merge two KWINDOWSYSTEM_HAVE_X11 sections into one
* Fix KStartupInfo::appStarted()
* Deprecate KStartupInfo::startupId()
* Deprecate KStartupInfo::silenceStartup()
* KWindowSystemPluginWrapper doesn't need a virtual dtor
* Make KX11Extras::icon behave like KWindowSytem::icon (bug 462739)

### KXMLGUI

* Decrease minimum height of inactive list widget (bug 461580)

### ModemManagerQt

* Lookup country code during initialization

### NetworkManagerQt

* Explicitly look for Qt::DBus in the CMake config file

### Plasma Framework

* Dialog: Only center panel popup if it still would cover two thirds of its panel widget (bug 462930)
* containmentinterface: add function to open context menu
* ExpandableListItem: load custom content when item is expanded
* PC3/ActionTextField: Specify focus reason for shortcut activation
* Use StandardKey.Find instead of "CTRL+F"
* ActionTextField: add property type for focusSequence
* ActionTextField: show native shortcut text in tooltip
* ActionTextField: fix StandardKey not working
* Dialog: Use one std::optional instead of two boolean flags
* Mark transient applets in config (bug 417114)

### Prison

* Add support for ZXing 2.0

### QQC2StyleBridge

* CheckBox/RadioButton/Switch: update implicit sizing code
* CheckBox/RadioButton/Switch: center align indicators when no content
* CheckBox/RadioButton/Switch: align indicators with 1st text line
* Button/ToolButton: allow menu arrows to be added by Accessible.role

### Sonnet

* Add Esperanto trigram data file

### Syntax Highlighting

* Resolve conflict on .ex file extension
* Markdown: remove dynamic rules that are not needed
* Julia: merge hundreds of rules into a single regex ; fix adjoint operator ; includes ##Comments
* avoid temporary constructions of Definition in AbstractHighlighter::highlightLine
* GCode: numbers are optional with parameters (bug 462969)
* cmake: Use ECMQmlModule for qtquick plugin
* update version and references
* Add MapCSS highlighting
* Add testcase to markdown file for Nim code blocks
* Markdown: Use nim highlighting in nim blocks
* Add testcase for "\\"
* Nim: Small syntax fixes
* Add new keywords: `get` and `set`

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
