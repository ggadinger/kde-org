---
date: 2023-05-11
appCount: 120
image: true
layout: gear
---
+ kdenlive: Fix corrupted project files on opening ([Commit](http://commits.kde.org/kdenlive/2eecb82e109949dbeb8137b4b5f83ac30cd65438), fixes bug [#469217](https://bugs.kde.org/469217))
+ skanpage:  Scan Export window's OCR language list is now scrollable ([Commit](http://commits.kde.org/skanpage/23564a7666cfec8c6ba9e149e89229c3b616b82c), fixes bug [#468522](https://bugs.kde.org/468522))
+ spectacle: Quitting Spectacle with Escape no longer affects windows below it ([Commit](http://commits.kde.org/spectacle/4c18dc46d84d60217653c70060b6bd7fa352e091), fixes bug [#428478](https://bugs.kde.org/428478))
