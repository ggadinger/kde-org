---
description: KDE Gear ⚙️ 22.12 brings improvements and new features to dozens of apps
authors:
  - SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
  - SPDX-FileCopyrightText: 2022 Carl Schwan <carl@carlschwan.eu>
  - SPDX-FileCopyrightText: 2022 Aron Kovacs <aronkvh@gmail.com>
  - SPDX-FileCopyrightText: 2022 Nate Graham <nate@kde.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2022-12-08
hero_image: hero.png
images:
  - /announcements/gear/22.12.0/Hero.webp
layout: gear
scssFiles:
- /scss/gear-22.12.scss
outro_img:
  link: many_apps.webp
  alt: Screenshots of many applications
title: "KDE Gear \n22.12 is Here!"
highlights:
  title: Highlights
  items:
  - header: Dolphin
    text: New Selection Mode
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Apps/22.12/dolphin_selection2.png
      mp4: https://cdn.kde.org/promo/Announcements/Apps/22.12/dolphin_selection2.mp4
      webm: https://cdn.kde.org/promo/Announcements/Apps/22.12/dolphin_selection2.webm
    hl_class: konsole
  - header: KDE Connect
    text: Inline Message Replies
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Apps/22.12/kdeconnect_message02.png
      mp4: https://cdn.kde.org/promo/Announcements/Apps/22.12/kdeconnect_message02.mp4
      webm: https://cdn.kde.org/promo/Announcements/Apps/22.12/kdeconnect_message02.webm
    hl_class: kdenlive
draft: false
---

KDE Gear ⚙️ is back with exciting new features, performance boosts, and bugfixes for all your favorite KDE apps!

In this release: Kate extends a warm welcome, Dolphin offers you more choices, and a lot of apps serve up hamburgers galore!

{{< announcements/donorbox >}}

Read on for details...

## What's New

{{< highlight-grid >}}

### [Dolphin](https://apps.kde.org/dolphin/)

Dolphin is KDE's powerful file browser. It has supported connecting to and browsing Samba shares for many versions, but now it is also able to [manage permissions remotely](https://bugs.kde.org/show_bug.cgi?id=40892).

Another new feature is *Selection Mode*. Hit the <kbd>spacebar</kbd> (or tap the hamburger menu and check the *Select files and folders checkbox*) and a light green bar will appear at the top of the file view (see video). You can now click or tap files and folders and quickly and easily select the ones you want to work with.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.12/dolphin_selection2.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.12/dolphin_selection2.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.12/dolphin_selection2.png" fig_class="mx-auto max-width-800" >}}

Another toolbar will appear at the bottom of the view, giving you options of what you can do with the selected files. For example, if you select only images, it will offer to open them in Gwenview. The moment you select a file Gwenview cannot handle, the options will change to fit the new set of selected files.

### [Gwenview](https://apps.kde.org/gwenview/)

Speaking of Gwenview, KDE's feature-rich image and video viewer becomes even... er... feature-richer, as Gwenview now also lets you adjust the brightness, contrast, and gamma of your pictures as you preview them.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.12/gwenview_colors.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.12/gwenview_colors.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.12/gwenview_colors.png" fig_class="mx-auto max-width-800" >}}

Another welcome feature for people who use Free Software graphical editing tools is that [Gwenview can now open GIMP's *.xcf* files](https://invent.kde.org/graphics/gwenview/-/merge_requests/156).

### [Kate](https://kate-editor.org/) and [KWrite](https://apps.kde.org/kwrite/)

Kate and KWrite, KDE's text editors, add a welcome window when launched without any files open. The new window lets you create a new file, open an existing file from a list of recent files or anywhere else on the system, and consult the documentation.

![Kate's new welcome screen](kate_welcome.webp)


But probably the most useful new feature of them all is the [Keyboard Macro tool](https://kate-editor.org/post/2022/2022-08-24-kate-new-features-august-2022/). Activate it in *Settings* > *Configure Kate...* > *Plugins*, and then at the bottom of the *Tools* menu, you'll find tools to record, save, and play back macros. You can record a long sequence of key presses you need to type often and then hit <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>K</kbd> and Kate will type the sequence for you. You can name and save sequences you find particularly useful and use them again and again during different sessions.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.12/kate-keyboard-macros-select-commands_h264.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.12/kate-keyboard-macros-select-commands_h264.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.12/kate-keyboard-macros-select-commands_h264.png" fig_class="mx-auto max-width-800" >}}

In other news, you will find that Kate and KWrite have also adopted KHamburgerMenu for those who prefer to hide their menubars. Because these are large and complex apps, the menubar is still shown by default, like it is in Okular and Kdenlive.

### [Kdenlive](https://kdenlive.org)

Kdenlive improves its guide/marker system with custom categories and search filters. Kdenlive also improves its integration with other video applications, and now you can send Kdenlive  timelines as backgrounds to the [Glaxnimate](https://glaxnimate.mattbas.org/) vector animation utility.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.12/kdenlive_hamburger.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.12/kdenlive_hamburger.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.12/kdenlive_hamburger.png" fig_class="mx-auto max-width-800" >}}

And like many other KDE apps, Kdenlive has also adopted KHamburgerMenu, although by default the regular menu is what you will see the first time you launch the app. If you turn off the normal menubar to gain some vertical real estate, all the menu options will be tucked away under the hamburger menu button in the toolbar.

### [KDE Connect](https://kdeconnect.kde.org/)

KDE Connect links up your phone to your desktop. It allows you to share files and the clipboard, use your phone as a mouse or a remote control, and answer messages from your desktop.

In version 22.12, when you want to reply to a text message using the KDE Connect widget, the text field is now inline rather than in a separate dialog window, making it more convenient to answer when working on your computer.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.12/kdeconnect_message02.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.12/kdeconnect_message02.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.12/kdeconnect_message02.png" fig_class="mx-auto max-width-800" >}}

### [Kalendar](https://apps.kde.org/kalendar/)

Kalendar is a new calendaring app from KDE. In the latest update, devs have introduced a new "basic" mode for views. These are ideal for use on low-performance or battery-powered devices, as this mode is not based on swipe gestures like the regular views, but rather presents a more static layout that is easier on the hardware.

Also new is that [Kalendar now uses pop-up windows for displaying events](https://claudiocambra.com/2022/09/15/kde-pim-in-july-and-august), making it easier and more convenient to view and manage your schedule.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.12/kalendar_popup.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.12/kalendar_popup.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.12/kalendar_popup.png" fig_class="mx-auto max-width-800" >}}

Developers have also been busy fixing bugs and improving the performance of Kalendar, so you can expect a smoother and more reliable experience when using the app.

## [Elisa](https://elisa.kde.org/)

Elisa is a friendly music player with a cool, modern look. With version 22.12 Elisa is even friendlier, as it now [shows a message explaining what didn't work](https://invent.kde.org/multimedia/elisa/-/merge_requests/376) in case you dragged-and-dropped a non-audio file onto its playlist.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.12/elisa.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.12/elisa.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.12/elisa.png" fig_class="mx-auto max-width-800" >}}

You can also put Elisa into a true full-Screen mode, and the *Artist* view displays a grid of the artist's albums, rather than a sea of nondescript identical icons.

## And all this too

* [Kitinerary](https://apps.kde.org/itinerary/) is KDE's travel assistant. Apart from supporting giving you information about trains, planes and buses, [the new version now also supports boats and ferries](https://volkerkrause.eu/2022/09/30/kde-itinerary-august-september-2022.html)
* [Kmail](https://kontact.kde.org/components/kmail/) is an advanced email client. The latest version comes with [improvements that make it easier to use encryption for email messages](https://blog.sandroknauss.de/overhaul-encyption-support-in-kontact/).
* If your keyboard has a "Calculator" button, [pressing it will now open KCalc](https://bugs.kde.org/show_bug.cgi?id=353596), [KDE's full-featured calculator app](https://apps.kde.org/kcalc/).
* [Spectacle](https://apps.kde.org/spectacle/) is a screenshot application that is simple by default, but powerful when needed that supports all sorts of screen-capturing modes, annotations, editing, and more. The new version of Spectacle now [remembers the last-chosen rectangular region area by default](https://invent.kde.org/graphics/spectacle/-/merge_requests/156), even across app launches.
* You use [Ark](https://apps.kde.org/ark/) to easily create, open, explore and extract files from compressed archives. In version 22.12, [Ark adds ARJ](https://bugs.kde.org/show_bug.cgi?id=104404) to its roster of supported compression formats. Ark has also [adopted KHamburgerMenu](https://bugs.kde.org/show_bug.cgi?id=444664) for a cleaner, simpler look.

---

{{< announcements/gear_major_outro >}}
