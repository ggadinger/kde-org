---
title: KDE platvorm 4.9 – parem ühilduvus, lihtsam arendus
date: "2012-12-05"
hidden: true
---

<p>
KDE annab uhkelt teada KDE platvormi 4.9 väljalaskmisest. KDE platvorm on Plasma töötsoonide ja KDE rakenduste alus. KDE meeskond töötab praegu KDE arendusplatvormi järgmise põlvkonna kallal, mis kannab nimetust "raamistik 5". Ehkki raamistik 5 on valdavalt tagasiulatuvalt ühilduv, on selle baasiks siiski Qt5. Sellega suureneb jaotatavus, mis võimaldab kasutada valikuliselt kindlaid platvormi osi ja vähendada tunduvalt kõikvõimalikke sõltuvusi. Seepärast on KDE platvorm praegu üsna külmutatud olekus: enamik selle kallal nähtud vaevast kulus vigade ja jõudluse parandamisele.
</p>
<p>
Edenenud on QGraphicsView lahutamine Plasmast, millega sillutatakse teed puhtale QML-põhisele Plasmale, samuti on paranenud Qt Quicki toetus KDE teekides ja Plasma töötsoonides.
</p>
<p>
KDE teekides on tehtud süvatasemel üht-teist võrguühenduse vallas. Kõik KDE rakendused suudavad nüüd palju kiiremini ligi pääseda võrgus välja jagatud ressurssidele. NFS-i, Samba ja SSHFS ühendused ning KIO ei loe enam sisse kõiki kataloogi elemente, mis muudab nad palju kiiremaks. Ebavajalike serveripöörduste vähendamisega on kiirust lisatud ka HTTP-protokollile. See on eriti täheldatav loomu poolest võrgus töötavates rakendustes, näiteks Korganizer, mis töötab nende paranduste tagajärjel ligemale 20% kiiremini. Võrguressursside puhul on täiustatud ka parooli salvestamist.
</p>
<p>
Nepomukis ja Sopranos parandati hulk kriitilise tähtsusega vigu, mis muudab töölauaotsingu kiiremaks ja usaldusväärsemaks nii KDE platvormile 4.9 tuginevates rakendustes kui ka töötsoonides. Just jõudluse parandamine ja suurem stabiilsus olidki nende projektide puhul käesolevaks väljalaskeks valmistudes kõige olulisemad eesmärgid.
</p>

<h4>KDE arendusplatvormi paigaldamine</h4>

<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral ja protsessoritel (näiteks ARM ja x86), operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta. <a href="http://plasma-active.org">Plasma Active</a> kujutab endast kasutajakogemust paljudele seadmetele, näiteks tahvelarvutid ja muud mobiilsed seadmed.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/4.9.0/">download.kde.org</a>, samuti
<a href="/download">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.9.0 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4/4.9.0">4.9 infoleheküljel</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Täieliku 4.9.0 lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/4.9.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.9.0 kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/4/4.9.0#binary">4.9.0 infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.4 või 4.8.0. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes.
</p>




<h2>Täna ilmusid veel:</h2>

<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Plasma töötsoonid 4.9 – põhikomponentide täiustused</a></h2>

<p>
Plasma töötsoonide peamistest uuendustest tasub esile tõsta olulisi täiustusi failihalduris Dolphin, X'i terminali emulaatoris Konsole, tegevustes ja aknahalduris Kwin. Neist kõneleb lähemalt <a href="../plasma">'Plasma töötsoonide teadaanne'.</a>
</p>

<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Uued ja täiustatud KDE rakendused 4.9</a></h2>
<p>
Täna ilmunud uute ja täiustatud KDE rakenduste seast väärivad märkimist Okular, Kopete, KDE PIM, õpirakendused ja mängud. Neist kõneleb lähemalt <a href="../applications">'KDE rakenduste teadaanne'</a>
</p>
