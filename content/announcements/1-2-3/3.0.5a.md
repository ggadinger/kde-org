---
aliases:
- ../announce-3.0.5a
custom_about: true
custom_contact: true
date: '2002-12-21'
description: KDE 3.0.5a is a security and maintenance update over KDE 3.0.5.
title: KDE 3.0.5a Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Security, Enhanced Service Release for Third
   Generation of Leading Open Source Desktop
</h3>

<p align="justify">
  <strong>
    The KDE Project Ships Enhanced Security Release for the
    Leading Linux/UNIX Desktop
  </strong>
</p>

<p align="justify">
  The <a href="/">KDE
  Project</a> today announced the immediate availability of KDE 3.0.5a.
  KDE 3 is the third generation of KDE's <em>free</em>, powerful desktop
  for Linux and other UNIXes.  KDE 3.0.5a is available in
  <a href="http://i18n.kde.org/teams/index.php">51</a> languages and ships
  with the core KDE libraries, the base desktop environment
  and hundreds of applications and other desktop enhancements
  from the other KDE base packages (PIM, administration, network,
  edutainment, development, utilities, multimedia, games, artwork, and
  others).
</p>

<p align="justify">
  KDE 3.0.5a provides a number of important security improvements, as
  described in the
  <a href="/info/security/advisory-20021220-1.txt">security
  advisory</a>, and some stability enhancements.  The KDE Project strongly
  recommends that all KDE users upgrade to this release as soon as practicable.
</p>

<p align="justify">
  KDE, including all its libraries and its applications, is
  available <em><strong>for free</strong></em> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from the KDE
  <a href="http://download.kde.org/stable/3.0.5a/">http</a> or
  <a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
</p>

<p align="justify">
  For more information about the KDE 3 series, please read the
  <a href="/announcements/announce-3.0">KDE 3.0
  announcement</a> and the <a href="/info/1-2-3/3.0.5a">KDE
  3.0.5a information page</a>.
</p>

<a id="binary"></a>
<h4>
  Installing KDE 3.0.5a Binary Packages
</h4>
<p align="justify">
  <u>Binary Packages</u>.
  Due to the Holidays few binary packages are available at this juncture.
  Additional binary packages, as well as updates to the packages now
  available, may become available via the KDE servers or vendors' websites
  over the coming days and weeks.  Some vendors are expected to incorporate
  the security improvements into new builds of KDE 3.0.5.  Please check
  the <a href="/info/1-2-3/3.0.5a">KDE 3.0.5a information
  page</a> and your vendor's website periodically for available packages.
</p>
<p align="justify">
  At the moment binary packages are available through the
  KDE servers (or elsewhere, where indicated) for the following distributions
  and versions (please be sure to
  read any applicable <code>README</code> first):
</p>
<ul>

<!--  DEBIAN -->
<li><a href="http://www.debian.org">Debian</a>:
    <ul type="disc">
      <li>
         Debian stable (woody):
         <a href="http://download.kde.org/stable/3.0.5a/Debian/woody/">Intel i386</a>
      </li>
    </ul>
</li>

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>:
  <ul type="disc">
    <li>
      8.0: <a href="http://download.kde.org/stable/3.0.5a/RedHat/8.0/i386/">Intel i386</a>,
           <a href="http://download.kde.org/stable/3.0.5a/RedHat/8.0/noarch/">Language packages</a>
    </li>
    <li>
      7.3: <a href="http://download.kde.org/stable/3.0.5a/RedHat/7.3/i386/">Intel i386</a>,
           <a href="http://download.kde.org/stable/3.0.5a/RedHat/7.3/noarch/">Language packages</a>
    </li>
  </ul>
</li>

<!--   SGI IRIX -->
<li>
  <a href="http://www.sgi.com/">SGI IRIX</a>
  (<a href="http://download.kde.org/stable/3.0.5a/contrib/IRIX/README_DD5_IRIX">README</a>):
  <ul type="disc">
    <li>6.5:
      <a href="http://download.kde.org/stable/3.0.5a/contrib/IRIX/">IRIX</a>
    </li>
  </ul>
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution):
   <ul type="disc">
     <li>
       8.1: <a href="http://download.kde.org/stable/3.0.5a/contrib/Slackware/8.1/">Intel i386</a>
     </li>
   </ul>
</li>

<!-- SOLARIS -->
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      9.0: <a href="http://download.kde.org/stable/3.0.5a/contrib/Solaris/">SPARC64</a>
    </li>
    <li>
      8.0: <a href="http://download.kde.org/stable/3.0.5a/contrib/Solaris/">Intel x86 / SPARC64</a>
    </li>
    <li>
      7.0: <a href="http://download.kde.org/stable/3.0.5a/contrib/Solaris/">SPARC64</a>
    </li>
  </ul>
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
   (<a href="http://download.kde.org/stable/3.0.5a/SuSE/README">README</a>):
  <ul type="disc">
    <li>
      8.1:
      <a href="http://download.kde.org/stable/3.0.5a/SuSE/ix86/8.1/">Intel
      i386</a>
    </li>
   <li>
     8.0:
     <a href="http://download.kde.org/stable/3.0.5a/SuSE/ix86/8.0/">Intel
     i386</a>
    </li>
    <li>
      7.3:
      <a href="http://download.kde.org/stable/3.0.5a/SuSE/ix86/7.3/">Intel
      i386</a> and
      <a href="http://download.kde.org/stable/3.0.5a/SuSE/ppc/7.3/">IBM
      PowerPC</a>
    </li>
  </ul>
</li>

<!-- TURBO LINUX -->
<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
  (<a href="http://download.kde.org/stable/3.0.5a/Turbo/Readme.txt">README</a>):
    <ul type="disc">
      <li>
        <a href="http://download.kde.org/stable/3.0.5a/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       8.0: 
       <a href="http://download.kde.org/stable/3.0.5a/Turbo/8/i586/">Intel i586</a>
      </li>
      <li>
       7.0:
       <a href="http://download.kde.org/stable/3.0.5a/Turbo/7/i586/">Intel i586</a>
      </li>
    </ul>
</li>
   
</ul>


<p align="justify">
  Please note that the KDE Project makes binary packages available from the
  KDE web site as a convenience to KDE users.  The KDE Project is not
  responsible for these packages as they are provided by third
  parties - typically, but not always, the distributor of the relevant
  distribution - using tools, compilers, library versions and quality
  assurance procedures over which KDE exercises no control.  In addition,
  please note that some vendors provide binary packages solely through
  their servers.  If you cannot find a binary package for your OS, or you
  are displeased with the quality of binary packages available for your OS,
  please read the <a href="http://www.kde.org/packagepolicy.html">KDE
  Binary Package Policy</a> and/or contact your OS vendor.
</p>

<h4>
  Compiling KDE 3.0.5a
</h4>
<p align="justify">
  <a id="source_code-library_requirements"></a><u>Library
  Requirements / Options</u>.
  KDE 3.0.5a requires
  <a href="http://www.trolltech.com/developer/download/qt-x11.html">Qt
  3.0.3 for X11</a>.
  In addition, KDE can take advantage of a number of other
  <a href="../3.0/#source_code-library_requirements">optional
  libraries and applications</a> to improve your desktop experience and
  capabilities.
</p>

<p align="justify">
  <u>Compiler Requirements</u>.
  KDE is designed to be cross-platform and hence to compile with a large
  variety of Linux/UNIX compilers.  However, KDE is advancing very rapidly
  and the ability of native compilers on various UNIX systems to compile
  KDE depends on users of those systems
  <a href="http://bugs.kde.org/">reporting</a> compile problems to
  the responsible developers.
</p>

<p align="justify">
  With respect to the most popular KDE compiler,
  <a href="http://gcc.gnu.org/">gcc/egcs</a>, please note that some
  components of KDE will not compile properly with gcc versions earlier
  than gcc-2.95, such as egcs-1.1.2 or gcc-2.7.2, or with unpatched
  versions of <a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc</a> 3.0.x.
  However, KDE should compile properly with gcc 3.1 and 3.2.
</p>

<p align="justify">
  <a id="source_code"></a><u>Source Code / SRPMs</u>.
  The complete source code for KDE 3.0.5a is available for
  <a href="http://download.kde.org/stable/3.0.5a/src/">download</a>.
  These source packages have been digitally signed with
  <a href="http://www.gnupg.org/">GnuPG</a> using the KDE Archives PGP Key
  (available from the
  <a href="http://www.kde.org/signature.html">KDE Signature page</a>
  or public key servers), and their respective MD5 sums are listed on the
  <a href="/info/1-2-3/3.0.5a">KDE 3.0.5a Info Page</a>.
</p>

<p align="justify">
  <u>Further Information</u>.  For further
  instructions on compiling and installing KDE 3.0.5a, please consult
  the <a href="http://developer.kde.org/build/index.html">installation
  instructions</a> and, if you should encounter compilation difficulties, the
  <a href="http://developer.kde.org/build/compilationfaq.html">KDE
  Compilation FAQ</a>.
</p>

<h4>
  KDE Sponsorship
</h4>

<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/gallery/index.html">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a> provide significant support for KDE promotion, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>

<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Trademarks Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  Trolltech and Qt are trademarks of Trolltech AS.

  UNIX is a registered trademark of The Open Group.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>
<hr />

<table id ="press" border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
Andreas Pour<br />
KDE League, Inc.<br />

[pour@kde.org](mailto:pour@kde.org)<br>
(1) 917 312 3122

  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(33) 4 3250 1445

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
      (49) 2421 502758
  </td>
</tr>
<tr Valign="top">
  <td >
    Europe (English):
  </td>
  <td>
   Jono Bacon <br>
    
  [jono@kde.org](mailto:jono@kde.org) <br>
  </td>
</tr>
<tr Valign="top">
  <td >
      Southeast Asia (English and Indonesian):
  </td>
  <td>
   Ariya Hidayat <br>
    
  [ariya@kde.org](mailto:ariya@kde.org) <br>
  (62) 815 8703177

  </td>
</tr>
</table>
