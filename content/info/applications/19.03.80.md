---
title: "KDE Applications 19.03.80 Info Page"
announcement: /announcements/announce-applications-19.03-beta
layout: applications
signer: Albert Astals Cid
signing_fingerprint: 8692A42FB1A8B666C51053919D17D97FD8224750
binary_package: false
---
