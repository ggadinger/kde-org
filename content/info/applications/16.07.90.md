---
title: "KDE Applications 16.07.90 Info Page"
announcement: /announcements/announce-applications-16.08-rc
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
