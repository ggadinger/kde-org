<li>KRunner version KDE SC 4.4.0 has a race condition that can allow
users at a local console to gain unintended access to a locked machine
and the logged-in account.
Read the <a href="/info/security/advisory-20100217-1.txt">detailed advisory</a>
for more information.
</li>
