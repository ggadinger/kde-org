KDE Project Security Advisory
=============================

Title:           kcron: Invalid temporary file handling
Risk Rating:     Low
CVE:             CVE-2022-24986
Versions:        kcron <= 21.12.2
Author:          Albert Astals Cid <aacid@kde.org>
Date:            16 February 2022

Overview
========

KCron is a module for the System Settings application.
The module identifies itself with the "Task Scheduler" user visible name.
It allows users to edit crontabs (both user-specific and system-wide) in a
GUI interface.

The code of the module doesn't use temporary files correctly neither when
reading the existing crontab nor when saving the new one.

Impact
======

Your private tasks may be exposed to other users of the system.
The system tasks may be replaced by other users of the system that don't
have rights to edit them.

Workaround
==========

Do not use use the Task Scheduler module of System Settings if there are
non-trusted users in your system

Solution
========

Update kcron 21.12.3 or apply the following patches
  https://commits.kde.org/kcron/ef4266e3d5ea741c4d4f442a2cb12a317d7502a1 [1]

[1] Not related to this issue these patches are also needed so that one applies
  https://commits.kde.org/kcron/2c04c9f665283e8480a65f4ac0accfe6a8e0539a

Credits
=======

Thanks to SUSE Linux security team for reporting the issue.
Thanks to Albert Astals Cid fixing the issue.
Thanks to Laurent Montel for helping with the reviews.
