<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/extragear-plasma-3.96.0.tar.bz2">extragear-plasma-3.96.0</a></td><td align="right">3.5MB</td><td><tt>c7cc1078a77cccf6aa8bae8e01a18f1e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdeaccessibility-3.96.0.tar.bz2">kdeaccessibility-3.96.0</a></td><td align="right">7.4MB</td><td><tt>ed0ac63fbe96a07ee0ab6749e6730fe4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdeadmin-3.96.0.tar.bz2">kdeadmin-3.96.0</a></td><td align="right">1.4MB</td><td><tt>d9f2d6a71b7fa35a4db3baf6fcc3db52</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdeartwork-3.96.0.tar.bz2">kdeartwork-3.96.0</a></td><td align="right">44MB</td><td><tt>a5d63e5f1116c4211f5fef6ff0ca2074</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdebase-3.96.0.tar.bz2">kdebase-3.96.0</a></td><td align="right">4.0MB</td><td><tt>71e7172754c2739e79e09e3301080ffe</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdebase-runtime-3.96.0.tar.bz2">kdebase-runtime-3.96.0</a></td><td align="right">53MB</td><td><tt>51f9ca43f8a6f6632f774f588208268c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdebase-workspace-3.96.0.tar.bz2">kdebase-workspace-3.96.0</a></td><td align="right">9.8MB</td><td><tt>ecbc8d8a9892725634ef0132fefd16f1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdebindings-3.96.0.tar.bz2">kdebindings-3.96.0</a></td><td align="right">4.1MB</td><td><tt>cb8081edb4b054fa5c84749a8fdf366a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdeedu-3.96.0.tar.bz2">kdeedu-3.96.0</a></td><td align="right">40MB</td><td><tt>056e68ba45772616e68acc40627fe2f7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdegames-3.96.0.tar.bz2">kdegames-3.96.0</a></td><td align="right">27MB</td><td><tt>d44e594732a1713a62e9c307d47319b8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdegraphics-3.96.0.tar.bz2">kdegraphics-3.96.0</a></td><td align="right">2.3MB</td><td><tt>093d45c959abe35cfcb3023278235a2d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdelibs-3.96.0.tar.bz2">kdelibs-3.96.0</a></td><td align="right">8.6MB</td><td><tt>d73ec2c5473c99a2a0e2c685335e0180</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdemultimedia-3.96.0.tar.bz2">kdemultimedia-3.96.0</a></td><td align="right">1.1MB</td><td><tt>daf943edaefddf147682c311b2bc610b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdenetwork-3.96.0.tar.bz2">kdenetwork-3.96.0</a></td><td align="right">6.2MB</td><td><tt>6500ca827402b5921603a3a5daa1c1d9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdepim-3.96.0.tar.bz2">kdepim-3.96.0</a></td><td align="right">13MB</td><td><tt>06748d9904dfa9fe90a34091742deb49</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdepimlibs-3.96.0.tar.bz2">kdepimlibs-3.96.0</a></td><td align="right">1.7MB</td><td><tt>c7ca2bb95cf318a97b9504cdd5988621</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdesdk-3.96.0.tar.bz2">kdesdk-3.96.0</a></td><td align="right">4.2MB</td><td><tt>7a2f020665c777dfb3af90cf4157b91e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdetoys-3.96.0.tar.bz2">kdetoys-3.96.0</a></td><td align="right">2.2MB</td><td><tt>fadddab4a80115b02b3dc9eea89becf3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.96/src/kdeutils-3.96.0.tar.bz2">kdeutils-3.96.0</a></td><td align="right">2.3MB</td><td><tt>f0c73b5f8119ee78f9741af1b3bd10d2</tt></td></tr>
</table>
